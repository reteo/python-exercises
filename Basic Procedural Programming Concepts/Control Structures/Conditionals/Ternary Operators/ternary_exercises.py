"""ternary_exercises: Exercises covering ternary operators."""
import random


def done():
    print("\n---\n")


def main():

    # These seem to be a glorious collection to use with exercises.

    word_list = [
        "innocent",
        "civic",
        "addicted",
        "satisfying",
        "brown",
        "kayak",
        "quixotic",
        "hissing",
        "trite",
        "noon",
        "trucks",
        "weigh",
        "radar",
        "art",
        "bell",
        "cheese",
    ]
    rand_word_list = [random.choice(word_list) for i in range(0, random.randint(1, 10))]
    number_list = [random.randint(-20, 20) for i in range(0, random.randint(3, 10))]
    combined_list = [str(x) for x in number_list]
    combined_list.extend(word_list)
    combined_list.extend(["!", "?", "'", "-", "=", "~", "/", ".", ",", " "])
    word_number_list = [
        str(random.choice(combined_list)) for i in range(1, random.randint(4, 10))
    ]
    combo_string = word_number_list[0] + word_number_list[1]
    e_rand_word_list = [
        random.choice(word_list) for i in range(0, random.randint(0, 3))
    ]
    e_number_list = [random.randint(-20, 20) for i in range(0, random.randint(0, 3))]
    posnum = random.randint(1, 10)
    longposnum = random.randint(1, 100)
    vlongposnum = random.randint(1, 200)
    negnum = random.randint(-10, -1)
    longnegnum = random.randint(-100, -1)
    vlongnegnum = random.randint(-200, -1)
    fullnum = random.randint(-10, 10)
    longfullnum = random.randint(-100, 100)
    vlongfullnum = random.randint(-200, 200)
    word = random.choice(word_list)
    randcase_word = random.choice([word.lower(), word.upper(), word.capitalize()])
    letter = random.choice("abcdefghijklmnopqrstuvwxyz")
    print("Random word list:", rand_word_list)
    print("Number list:", number_list)
    print("Combined list:", combined_list)
    print("Word and number list:", word_number_list)
    print("Combination String:", combo_string)
    print("Random word list (can be empty):", e_rand_word_list)
    print("Number list: (can be empty)", e_number_list)
    print("Positive Number:", posnum)
    print("Potentially-Large Positive Number:", longposnum)
    print("Potentially-Very-Large Positive Number:", vlongposnum)
    print("Negative Number:", negnum)
    print("Potentially-Large Negative Number:", longnegnum)
    print("Potentially-Very-Large Negative Number:", vlongnegnum)
    print("Number:", fullnum)
    print("Potentially-Large Number:", longfullnum)
    print("Potentially-Very-Large Number:", vlongfullnum)
    print("Random Word:", word)
    print("Random Word in random case:", randcase_word)
    print("Random Letter:", letter)

    done()

    print(
        "1. Write a program that uses a ternary operator to determine if a number is odd or even."
    )

    print(f"Number: {fullnum}")

    print("Even" if fullnum % 2 == 0 else "Odd")

    done()

    print(
        "2. Write a program that uses a ternary operator to determine the maximum of two numbers."
    )

    number1 = random.randint(1, 100)
    number2 = random.randint(1, 100)
    print(f"Numbers: {number1} {number2}")

    print(number1 if number1 > number2 else number2)

    done()

    print(
        "3. Write a program that uses a ternary operator to determine the minimum of two numbers."
    )

    number1 = random.randint(1, 100)
    number2 = random.randint(1, 100)
    print(f"Numbers: {number1} {number2}")

    print(number1 if number1 < number2 else number2)

    done()

    print(
        "4. Write a program that uses a ternary operator to determine if a number is positive, negative, or zero."
    )

    print(f"Number: {fullnum}")

    print("Zero" if fullnum == 0 else "Positive" if fullnum > 0 else "Negative")

    done()

    print(
        "5. Write a program that uses a ternary operator to determine if a string is empty or not."
    )

    word_local = random.choice([word, ""])
    print(f"String: {word_local}")

    print("Empty" if word_local == "" else "Not Empty")

    done()

    print(
        "6. Write a program that uses a ternary operator to determine if a list is empty or not."
    )

    print(f"List: {e_rand_word_list}")

    print("Empty" if len(e_rand_word_list) == 0 else "Not Empty")

    done()

    print(
        "7. Write a program that uses a ternary operator to determine the length of a string."
    )

    print(f"String: {word}")

    print(
        "Less than 6 characters"
        if len(word) < 6
        else "More than 6 characters"
        if len(word) > 6
        else "6 characters"
    )

    done()

    print(
        "8. Write a program that uses a ternary operator to determine the length of a list."
    )

    print(f"List: {rand_word_list}")

    print(
        "Less than 4 elements"
        if len(rand_word_list) < 4
        else "More than 4 elements"
        if len(rand_word_list) > 4
        else "4 elements"
    )

    done()

    print(
        "9. Write a program that uses a ternary operator to determine if a number is divisible by 5 or not."
    )

    print(f"Number: {vlongfullnum}")

    print("Divisible by 5" if vlongfullnum % 5 == 0 else "Not divisible by 5")

    done()

    print(
        "10. Write a program that uses a ternary operator to determine if a number is divisible by 10 or not."
    )

    print(f"Number: {vlongfullnum}")

    print("Divisible by 10" if vlongfullnum % 10 == 0 else "Not divisible by 10")

    done()

    print(
        "11. Write a program that uses a ternary operator to determine if a number is divisible by 15 or not."
    )

    print(f"Number: {vlongfullnum}")

    print("Divisible by 15" if vlongfullnum % 5 == 0 else "Not divisible by 15")

    done()

    print(
        "12. Write a program that uses a ternary operator to determine if a number is prime or not."
    )
    print(f"Number: {vlongfullnum}")

    print(
        "Prime"
        if (vlongfullnum < -1 or vlongfullnum > 1)
        and all(vlongfullnum % i for i in range(2, int(abs(vlongfullnum))))
        else "Not prime"
    )

    done()

    print(
        "13. Write a program that uses a ternary operator to determine if a character is a vowel or not."
    )

    print(f"Character: {letter}")

    print("Vowel" if letter in "aeiou" else "Not a Vowel")

    done()

    print(
        "14. Write a program that uses a ternary operator to determine if a character is a consonant or not."
    )

    print(f"Character: {letter}")

    print("Consonant" if letter not in "aeiou" else "Not a consonant")

    done()

    print(
        "15. Write a program that uses a ternary operator to determine if a character is a digit or not."
    )

    character = combo_string[0]
    print(f"Character: {character}")

    print("Digit" if character.isdigit() else "Not a digit")

    done()

    print(
        "16. Write a program that uses a ternary operator to determine if a character is a letter or not."
    )

    print(f"Character: {character}")

    print("Letter" if character.isalpha() else "Not a letter")

    done()

    print(
        "17. Write a program that uses a ternary operator to determine if a character is lowercase or not."
    )

    character = randcase_word[0]
    print(f"Character: {character}")

    print("Lowercase" if character.islower() else "Not lowercase")

    done()

    print(
        "18. Write a program that uses a ternary operator to determine if a character is uppercase or not."
    )

    print(f"Character: {character}")

    print("Uppercase" if character.isupper() else "Not Uppercase")

    done()

    print(
        "19. Write a program that uses a ternary operator to determine if a string contains a specific substring."
    )

    while True:
        substring_word = random.choice(rand_word_list)
        substring_slice_low = random.randint(0, (int(len(substring_word) / 2)))

        substring = substring_word[substring_slice_low : substring_slice_low + 2]
        if substring != "":
            break

    print(f"String: {word}")
    print(f"Substring: {substring}")

    print("Substring found" if substring in word else "Substring not found")

    done()

    print(
        "20. Write a program that uses a ternary operator to determine if a list contains a specific element."
    )

    element = random.choice(word_list)

    print(f"List: {rand_word_list}")
    print(f"Element: {element}")

    print("Element found" if element in rand_word_list else "Element not found")

    done()

    print(
        "21. Write a program that uses a ternary operator to determine the index of a specific element in a list."
    )

    print(f"List: {word_list}")
    print(f"List: {element}")
    index = -1 if element not in rand_word_list else rand_word_list.index(element)

    print(f"Index: {index}" if index != -1 else "Index not found.")

    done()

    print(
        "22. Write a program that uses a ternary operator to determine if a number is within a specific range."
    )

    low_number = random.randint(-10, 5)
    high_number = random.randint((low_number + 1), 10)
    print(f"Number: {fullnum}")
    print(f"Range: {low_number} to {high_number}")

    print(
        f"{fullnum} is between {low_number} and {high_number}"
        if fullnum > low_number and fullnum < high_number
        else f"{fullnum} is not between {low_number} and {high_number}"
    )

    done()

    print(
        "23. Write a program that uses a ternary operator to determine if a string starts with a specific substring."
    )

    print(f"String: {word}")
    print(f"Substring: {substring}")

    print(
        f"{word} begins with {substring}"
        if word.startswith(substring)
        else f"{word} does not begin with {substring}"
    )

    done()

    print(
        "24. Write a program that uses a ternary operator to determine if a string ends with a specific substring."
    )

    print(f"String: {word}")
    print(f"Substring: {substring}")

    print(
        f"{word} ends with {substring}"
        if word.endswith(substring)
        else f"{word} does not end with {substring}"
    )

    done()

    print(
        "25. Write a program that uses a ternary operator to determine the number of occurrences of a specific substring in a string."
    )
    
    print(f"String: {word}")
    print(f"Substring: {substring}")

    print(f"\"{substring}\" does not occur in \"{word}\"" if word.count(substring) == 0 else f"\"{substring}\" occurs in \"{word}\" {word.count(substring)} " + ("time." if word.count(substring) == 1 else "times."))

main()
