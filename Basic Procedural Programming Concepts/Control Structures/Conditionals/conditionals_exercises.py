"""conditionals_exercises: Exercises conditionals"""

from math import sqrt
import random

def get_input(variable_type: type, request: str, error_message: str):
    while True:
        try:
            result = input(request)
            final_result = variable_type(result)
            return final_result
        except Exception:
            print(error_message)


def done():
    print("\n---\n")


def is_prime(number: int):
    if number > 1 or number < -1:
        for i in range(2, int(number / 2) + 1):
            if number % i == 0:
                return False
    else:
        return False
    return True

def is_palindrome(value):
    test_value_forward = str(value)
    test_value_backward = str(value)[::-1]

    if test_value_forward == test_value_backward:
        return True
    else:
        return False


def is_square(value):
    if value > 0:
        if sqrt(value) == float(int(sqrt(value))):
            return True

    return False


def main():
    # First, we want to initialize some variables we'll be using
    # throughout the exercises.
    
    word_list = [
        "innocent",
        "civic",
        "addicted",
        "satisfying",
        "brown",
        "kayak",
        "quixotic",
        "hissing",
        "trite",
        "noon",
        "trucks",
        "weigh",
        "radar",
        "art",
        "bell",
        "cheese",
    ]
    number_list = [random.randint(-20, 20) for i in range(0, random.randint(3, 10))]
    combined_list = [str(x) for x in number_list]
    combined_list.extend(word_list)
    combined_list.extend(["!", "?", "'", "-", "=", "~", "/", ".", ",", " " ])
    word_number_list = [
        str(random.choice(combined_list))
        for i in range(1, random.randint(4, 10))
    ]
    combo_string = word_number_list[0] + word_number_list[1]
    e_rand_word_list = [
        random.choice(word_list) for i in range(0, random.randint(0, 3))
    ]
    e_number_list = [random.randint(-20, 20) for i in range(0, random.randint(0, 3))]
    posnum = random.randint(1, 10)
    longposnum = random.randint(1, 100)
    vlongposnum = random.randint(1, 200)
    negnum = random.randint(-10, -1)
    longnegnum = random.randint(-100, -1)
    vlongnegnum = random.randint(-200, -1)
    fullnum = random.randint(-10, 10)
    longfullnum = random.randint(-100, 100)
    vlongfullnum = random.randint(-200, 200)
    word = random.choice(word_list)
    randcase_word = random.choice([word.lower(), word.upper(), word.capitalize()])
    letter = random.choice("abcdefghijklmnopqrstuvwxyz")

    print("Random word list:", word_list)
    print("Number list:", number_list)
    print("Combined list:", combined_list)
    print("Word and number list:", word_number_list)
    print("Combination String:", combo_string)
    print("Random word list (can be empty):", e_rand_word_list)
    print("Number list: (can be empty)", e_number_list)
    print("Positive Number:", posnum)
    print("Potentially-Large Positive Number:", longposnum)
    print("Potentially-Very-Large Positive Number:", vlongposnum)
    print("Negative Number:", negnum)
    print("Potentially-Large Negative Number:", longnegnum)
    print("Potentially-Very-Large Negative Number:", vlongnegnum)
    print("Number:", fullnum)
    print("Potentially-Large Number:", longfullnum)
    print("Potentially-Very-Large Number:", vlongfullnum)
    print("Random Word:", word)
    print("Random Word in random case:", word)
    print("Random Letter:", letter)

    done()

    # 1. Write a program that checks if a number is odd or even and
    # prints the result. If the number is odd, print "Odd", if the
    # number is even, print "Even"

    if posnum % 2 == 0:
        print(f"{posnum}: Even.")
    else:
        print(f"{posnum}: Odd.")

    done()

    # 2. Write a program that checks if a number is positive or
    # negative and prints the result. If the number is positive,
    # print "Positive", if the number is negative, print "Negative"

    if fullnum > 0:
        print(f"{fullnum}: Positive")
    elif fullnum < 0:
        print(f"{fullnum}: Negative")
    else:
        print(f"{fullnum}: Zero")

    done()

    # 3. Write a program that checks if a number is divisible by 5,
    # if it is print "Divisible by 5", else if it's divisible by 3,
    # print "Divisible by 3" else print "Not divisible by 3 or 5"

    if fullnum % 5 == 0:
        print(f"{fullnum}: Divisible by 5")
    elif fullnum % 3 == 0:
        print(f"{fullnum}: Divisible by 3")
    else:
        print(f"{fullnum}: Not devisible by 3 or 5")

    done()

    # 4. Write a program that checks if a number is greater than 10,
    # if it is print "Greater than 10", else if it's less than 10,
    # print "Less than 10", else print "Equal to 10"

    if longposnum > 10:
        print(f"{longposnum}: Greater than 10")
    elif longposnum < 10:
        print(f"{longposnum}: Less than 10")
    else:
        print(f"{longposnum}: Equal to 10")

    done()

    # 5. Write a program that checks if a string contains a specific
    # letter, if it does print "String contains letter", else print
    # "String does not contain letter"

    if word.find(letter) != -1:
        print(f"{word}: String contains letter {letter}")
    else:
        print(f"{word}: String does not contain letter {letter}")

    done()

    # 6. Write a program that checks if a list is empty, if it is
    # print "List is empty", else print "List is not empty"

    if len(e_rand_word_list) == 0:
        print(f"{e_rand_word_list} is empty")
    else:
        print(f"{e_rand_word_list} is not empty")

    done()

    # 7. Write a program that checks if a number is between 50 and
    # 100, if it is print "Number between 50 and 100", else print
    # "Number not between 50 and 100"

    if vlongposnum > 50 and vlongposnum < 100:
        print(f"{vlongposnum}: Number between 50 and 100")
    else:
        print(f"{vlongposnum}: Number not between 50 and 100")

    done()

    # 8. Write a program that checks if a number is not equal to 0,
    # if it is print "Not equal to 0", else print "Equal to 0"

    if fullnum != 0:
        print(f"{fullnum}: Not equal to 0")
    else:
        print(f"{fullnum}: Equal to 0")

    done()

    # 9. Write a program that checks if a string starts with a
    # specific letter, if it does print "String starts with letter",
    # else print "String does not start with letter"

    if word.startswith(letter) == True:
        print(f"{word}: String starts with letter {letter}")
    else:
        print(f"{word}: String does not start with letter {letter}")

    done()

    # 10. Write a program that checks if a string ends with a
    # specific letter, if it does print "String ends with letter",
    # else print "String does not end with letter"

    if word.endswith(letter) == True:
        print(f"{word}: String ends with letter {letter}")
    else:
        print(f"{word}: String does not end with letter {letter}")

    done()

    # 11. Write a program that checks if a number is a multiple of 7,
    # if it is print "Multiple of 7", else if it's a multiple of 9,
    # print "Multiple of 9", else print "Not multiple of 7 or 9"

    if fullnum % 7 == 0:
        print(f"{fullnum}: Multiple of 7")
    elif fullnum % 9 == 0:
        print(f"{fullnum}: Multiple 9")
    else:
        print(f"{fullnum}: Not multiple of 7 or 9")

    done()

    # 12. Write a program that checks if a number is a prime number,
    # if it is print "Prime", else print "Not prime"

    if is_prime(longposnum) == True:
        print(f"{longposnum}: Prime")
    else:
        print(f"{longposnum}: Not prime")
    done()

    # 13. Write a program that checks if a string is a palindrome, if
    # it is print "Palindrome", else print "Not a palindrome"

    if is_palindrome(word):
        print(f"{word}: Palindrome")
    else:
        print(f"{word}: Not a palindrome")

    done()

    # 14. Write a program that checks if a number is a perfect
    # square, if it is print "Perfect square", else print "Not a
    # perfect square"

    if is_square(longfullnum):
        print(f"{longfullnum}: Perfect square")
    else:
        print(f"{longfullnum}: Not a perfect square")

    done()

    # 15. Write a program that checks if a number is greater than
    # 100, if it is print "Greater than 100", else if it's greater
    # than 50, print "Greater than 50", else if it's greater than 20,
    # print "Greater than 20", else print "Less than or equal to 20"

    if vlongposnum > 100:
        print(f"{vlongposnum}: Greater than 100")
    elif vlongposnum > 50:
        print(f"{vlongposnum}: Greater than 50")
    elif vlongposnum > 20:
        print(f"{vlongposnum}: Greater than 20")
    else:
        print(f"{vlongposnum}: Less than or equal to 20")

    done()

    # 16. Write a program that checks if a number is divisible by 4,
    # if it is print "Divisible by 4", else if it's divisible by 2,
    # print "Divisible by 2", else print "Not divisible by 2 or 4"

    if fullnum % 4 == 0:
        print(f"{fullnum}: Divisible by 4")
    elif fullnum % 2 == 0:
        print(f"{fullnum}: Divisible by 2")
    else:
        print(f"{fullnum}: Not devisible by 2 or 4")

    done()

    # 17. Write a program that checks if a number is between 1 and
    # 10, if it is print "Number between 1 and 10", else if it's
    # between 11 and 20, print "Number between 11 and 20", else if
    # it's between 21 and 30, print "Number between 21 and 30", else
    # print "Number not between 1 and 30"

    if longposnum >= 1 and longposnum <= 10:
        print(f"{longposnum}: Number between 1 and 10")
    elif longposnum >= 11 and longposnum <= 20:
        print(f"{longposnum}: Number between 11 and 20")
    elif longposnum >= 21 and longposnum <= 30:
        print(f"{longposnum}: Number between 21 and 30")
    else:
        print(f"{longposnum}: Number not between 1 and 30")

    done()

    # 18. Write a program that checks if a number is even or odd, if
    # it is even print "Even", else if it's odd, print "Odd", else
    # print "Not a number"

    if posnum % 2 == 0:
        print(f"{posnum}: Even.")
    elif posnum % 2 == 1:
        print(f"{posnum}: Odd.")
    else:
        print(f"{posnum}: Not a number.")
    done()

    # 19. Write a program that checks if a string is alphanumeric, if
    # it is print "Alphanumeric", else if it's numeric, print
    # "Numeric", else print "Not alphanumeric or numeric"
    
    if combo_string.isnumeric():
        print(f"{combo_string}: Numeric")
    elif combo_string.isalnum():
        print(f"{combo_string}: Alphanumeric")
    else:
        print(f"{combo_string}: Not alphanumeric or numeric.")

    done()
        

    # 20. Write a program that checks if a number is a multiple of 3,
    # if it is print "Multiple of 3", else if it's a multiple of 5,
    # print "Multiple of 5", else print "Not multiple of 3 or 5"

    if fullnum % 5 == 0:
        print(f"{fullnum}: Multiple of 5")
    elif fullnum % 3 == 0:
        print(f"{fullnum}: Multiple of 3")
    else:
        print(f"{fullnum}: Not multiple of 3 or 5")

    done()

    # 21. Write a program that checks if a number is less than or
    # equal to 10, if it is print "Less than or equal to 10", else if
    # it's less than or equal to 20, print "Less than or equal to
    # 20", else if it's less than or equal to 30, print "Less than or
    # equal to 30", else print "Greater than 30"

    if longposnum <= 10:
        print(f"{longposnum}: Less than or equal to 10")
    elif longposnum <= 20:
        print(f"{longposnum}: Less than or equal to 20")
    elif longposnum <= 30:
        print(f"{longposnum}: Less than or equal to 30")
    else:
        print(f"{longposnum}: Greater than 30")

    done()

    # 22. Write a program that checks if a number is between -10 and
    # 10, if it is print "Number between -10 and 10", else if it's
    # between -20 and 20, print "Number between -20 and 20", else if
    # it's between -30 and 30, print "Number between -30 and 30",
    # else print "Number not between -30 and 30"

    if longfullnum >= -10 and longfullnum <= 10:
        print(f"{longfullnum}: Number between -10 and 10")
    elif longfullnum >= -20 and longfullnum <= 20:
        print(f"{longfullnum}: Number between -20 and 20")
    elif longfullnum >= -30 and longfullnum <= 30:
        print(f"{longfullnum}: Number between -30 and 30")
    else:
        print(f"{longfullnum}: Number not between -30 and 30")

    done()

    # 23. Write a program that checks if a string is uppercase, if it
    # is print "Uppercase", else if it's lowercase, print
    # "Lowercase", else print "Not uppercase or lowercase"

    if randcase_word == randcase_word.upper():
        print(f"{randcase_word}: Uppercase")
    elif randcase_word == randcase_word.lower():
        print(f"{randcase_word}: Lowercase")
    else:
        print(f"{randcase_word}: Not uppercase nor lowercase")

    done()

    # 24. Write a program that checks if a number is a multiple of 4,
    # if it is print "Multiple of 4", else if it's a multiple of 6,
    # print "Multiple of 6", else print "Not multiple of 4 or 6"

    if longfullnum % 4 == 0:
        print(f"{longfullnum}: Multiple of 4")
    elif longfullnum % 6 == 0:
        print(f"{longfullnum}: Multiple of 6")
    else:
        print(f"{longfullnum}: Not multiple of 4 or 6")

    done()    

    # 25. Write a program that checks if a number is greater than or
    # equal to 50, if it is print "Greater than or equal to 50", else
    # if it's greater than or equal to 40, print "Greater than or
    # equal to 40", else if it's greater than or equal to 30, print
    # "Greater than or equal to 30", else print "Less than 30"

    if longposnum >= 50:
        print(f"{longposnum}: Greater than or equal to 50")
    elif longposnum >= 40:
        print(f"{longposnum}: Greater than or equal to 40")
    elif longposnum >= 30:
        print(f"{longposnum}: Greater than or equal to 30")
    else:
        print(f"{longposnum}: Less than 30")

    done()

    print("Done.")


main()
