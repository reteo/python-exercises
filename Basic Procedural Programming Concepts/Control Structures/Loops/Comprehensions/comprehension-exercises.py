"""comprehension-exercises: Exercises covering comprehensions"""

import itertools
from math import sqrt
from random import randint, random


def is_prime(number: int):
    if number > 1 or number < -1:
        for i in range(2, int(number / 2) + 1):
            if number % i == 0:
                return False
    else:
        return False
    return True


def done():
    print("\n---\n")


def main():

    print(
        "1. Create a list comprehension that generates the squares of the numbers from 0 to 9."
    )

    print([x * x for x in range(0, 10)])

    done()

    print(
        "2. Use a set comprehension to create a set of the even numbers from 0 to 10."
    )

    print({x for x in range(0, 11) if x % 2 == 0})

    done()

    print(
        "3. Use a dictionary comprehension to create a dictionary that maps the letters of the alphabet to their ASCII code."
    )

    letter_dict = {
        ascii: letter
        for ascii, letter in zip(range(97, 123), "abcdefghijklmnopqrstuvwxyz")
    }
    print(letter_dict)

    done()

    print(
        "4. Use a list comprehension to create a list of the first 100 prime numbers."
    )

    # Added the "is_prime()" function to complete this.
    prime_generator = (x for x in itertools.count(1) if is_prime(x))
    primes = [x for x in itertools.islice(prime_generator, 100)]
    print(primes)

    done()

    print("5. Use a comprehension to create a list of the first 10 perfect squares.")

    print([x * x for x in range(11)])

    done()

    print("6. Use a comprehension to create a set of the divisors of a given number.")

    num = randint(2, 100)
    print(f"Number: {num}")

    factors = [x for x in range(2, int(abs(num / 2))) if num % x == 0]
    print(factors if len(factors) > 0 else "Prime Number")

    done()

    print(
        "7. Use a comprehension to create a dictionary that maps the integers from 1 to 10 to their respective square roots."
    )

    roots_dict = {num: sqrt(num) for num in range(1, 11)}
    print(roots_dict)

    done()

    print(
        "8. Create a list comprehension that squares each element in a given list of integers."
    )

    integers = list({randint(0, 20) for x in range(0, randint(4, 10))})
    print(f"List of integers: {integers}")

    print([x*x for x in integers])

    done()

    print(
        "9. Create a set comprehension that filters out all even numbers from a given list of integers."
    )

    integers = list(randint(0, 20) for x in range(0, randint(4, 10)))
    print(f"List of integers: {integers}")

    no_evens_set = {x for x in integers if x % 2 != 0}
    print(no_evens_set)

    done()

    print(
        "10. Create a dictionary comprehension that squares each key in a given dictionary of integers."
    )

    integers = set(randint(0, 20) for x in range(0, randint(4, 10)))
    print(f"List of integers: {integers}")

    squares = {x:x*x for x in integers}
    print(squares)
    
    done()
    
    print(
        '11. Create a list comprehension that filters out all words that contain the letter "a" from a given list of words.'
    )

    words = ["par", "limited", "gaffe", "rainbow", "just", "tear", "clothes", "kidney", "economist", "stun", "fuel"]
    print(f"Word list: {words}")

    no_a_words = [x for x in words if "a" not in x]
    print(no_a_words)

    done()
    
    print(
        "12. Create a list comprehension that converts a given list of strings to uppercase."
    )
    
    print(f"Word list: {words}")

    upper_words = [x.upper() for x in words]
    print(upper_words)

    done()

    print(
        "13. Create a list comprehension that extracts the first letter of each word in a given list of words."
    )
    
    print(f"Word list: {words}")

    first_letters = [word[0] for word in words]
    print(first_letters)

    done()
    
    print(
        "14. Create a set comprehension that filters out all words with a length greater than 4 from a given list of words."
    )
    
    print(f"Word list: {words}")
    small_words = [word for word in words if len(word) <= 4 ]
    print(small_words)

    done()

    print(
        "15. Create a dictionary comprehension that inverts the keys and values in a given dictionary."
    )
    word_dict = {words[x]:x for x in range(0, len(words))}
    print(word_dict)

    word_ref_dict = {num:word for word, num in word_dict.items()}
    print(word_ref_dict)
    

    done()

    print(
        "16. Create a list comprehension that filters out all negative numbers from a given list of integers."
    )

    numbers = [randint(-20, 20) for x in range(0, 10)]
    print(f"Numbers: {numbers}")

    positives = [x for x in numbers if x >= 0]
    print(positives)

    done()

    print(
        '17. Create a list comprehension that filters out all strings that end with the letter "s" from a given list of strings.'
    )

    print(f"Words: {words}")

    no_s_words = [word for word in words if not word.endswith("s")]
    print(no_s_words)

    done()

    print(
        "18. Create a set comprehension that filters out all numbers that are divisible by 3 from a given list of integers."
    )

    print(f"Number List: {numbers}")

    no_3_numbers = {num for num in numbers if num % 3 != 0}
    print(f"Result: {no_3_numbers}")

    done()

    print(
        '19. Create a list comprehension that filters out all words that contain the letter "e" from a given list of words.'
    )

    print(f"Word List: {words}")

    no_e_words = [word for word in words if "e" not in word]
    print(f"Result: {no_e_words}")

    done()

    print(
        "20. Create a list comprehension that filters out all numbers that are divisible by 5 from a given list of integers."
    )

    print(f"Integer List: {numbers}")

    no_5_numbers = [num for num in numbers if num % 5 != 0]
    print(f"Result: {no_5_numbers}")
    
    done()

    print(
        "21. Create a dictionary comprehension that filters out all items with a value less than 100 in a given dictionary of integers."
    )

    extreme_numbers = {x:randint(-200, 200) for x in range(0, 10)}
    print(f"Integer List: {extreme_numbers}")

    no_small_numbers = {k:v for k, v in extreme_numbers.items() if v >= 100}
    print(no_small_numbers)

    done()

    print(
        '22. Create a list comprehension that filters out all words that contain the letter "i" from a given list of words.'
    )

    print(f"Word List: {words}")

    no_i_words = [word for word in words if "i" not in word]
    print(f"Results: {no_i_words}")

    done()

    print(
        "23. Create a list comprehension that squares each element in a given list of floats."
    )

    float_list = [random() + randint(0, 100) for x in range(0, 10)]
    print(f"Float List: {float_list}")

    float_list = [x*x for x in float_list]
    print(float_list)
    
    done()
    
    print(
        "24. Create a set comprehension that filters out all words with a length less than or equal to 3 from a given list of words."
    )

    print(f"Word List: {words}")

    tiny_words = {word for word in words if len(word) <= 3}
    print(tiny_words)

    done()
    
    print(
        '25. Create a dictionary comprehension that removes all items with a key containing the letter "a" from a given dictionary.'
    )

    print(f"Word Dictionary: {word_dict}")

    no_a_words_dict = {word:value for word, value in word_dict.items() if "a" not in word}
    print(f"Results: {no_a_words_dict}")

    pass

main()
