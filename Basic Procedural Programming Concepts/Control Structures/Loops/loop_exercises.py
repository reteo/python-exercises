'''loop_exercises: Exercises covering loops in python'''

import random

def done():
    print("\n---\n")

def main():

    # These seem to be a glorious collection to use with exercises.

    word_list = [
        "innocent",
        "civic",
        "addicted",
        "satisfying",
        "brown",
        "kayak",
        "quixotic",
        "hissing",
        "trite",
        "noon",
        "trucks",
        "weigh",
        "radar",
        "art",
        "bell",
        "cheese",
    ]
    rand_word_list = [random.choice(word_list) for i in range(0, random.randint(1, 10))]
    number_list = [random.randint(-20, 20) for i in range(0, random.randint(3, 10))]
    combined_list = [str(x) for x in number_list]
    combined_list.extend(word_list)
    combined_list.extend(["!", "?", "'", "-", "=", "~", "/", ".", ",", " " ])
    word_number_list = [
        str(random.choice(combined_list))
        for i in range(1, random.randint(4, 10))
    ]
    combo_string = word_number_list[0] + word_number_list[1]
    e_rand_word_list = [
        random.choice(word_list) for i in range(0, random.randint(0, 3))
    ]
    e_number_list = [random.randint(-20, 20) for i in range(0, random.randint(0, 3))]
    posnum = random.randint(1, 10)
    longposnum = random.randint(1, 100)
    vlongposnum = random.randint(1, 200)
    negnum = random.randint(-10, -1)
    longnegnum = random.randint(-100, -1)
    vlongnegnum = random.randint(-200, -1)
    fullnum = random.randint(-10, 10)
    longfullnum = random.randint(-100, 100)
    vlongfullnum = random.randint(-200, 200)
    word = random.choice(word_list)
    randcase_word = random.choice([word.lower(), word.upper(), word.capitalize()])
    letter = random.choice("abcdefghijklmnopqrstuvwxyz")

    print("Random word list:", rand_word_list)
    print("Number list:", number_list)
    print("Combined list:", combined_list)
    print("Word and number list:", word_number_list)
    print("Combination String:", combo_string)
    print("Random word list (can be empty):", e_rand_word_list)
    print("Number list: (can be empty)", e_number_list)
    print("Positive Number:", posnum)
    print("Potentially-Large Positive Number:", longposnum)
    print("Potentially-Very-Large Positive Number:", vlongposnum)
    print("Negative Number:", negnum)
    print("Potentially-Large Negative Number:", longnegnum)
    print("Potentially-Very-Large Negative Number:", vlongnegnum)
    print("Number:", fullnum)
    print("Potentially-Large Number:", longfullnum)
    print("Potentially-Very-Large Number:", vlongfullnum)
    print("Random Word:", word)
    print("Random Word in random case:", randcase_word)
    print("Random Letter:", letter)

    done()

    print("1. Using a for loop, print the numbers 1 through 10")
    
    for i in range(1, 11):
        print(i, end = " ")
    print()
    
    done()

    print("2. Using a while loop, print the numbers 1 through 10")

    i = 1

    while i < 11:
        print(i, end = " ")
        i += 1
    print()

    done()

    print("3. Using a for loop, print the even numbers between 0 and 10")

    for i in range(0, 11, 2):
        print(i, end = " ")
    print()

    done()

    print("4. Using a while loop, print the even numbers between 0 and 10")

    i = 0

    while i < 11:
        print(i, end = " ")
        i += 2
    print()

    done()

    print("5. Using a for loop, print the odd numbers between 0 and 10")

    for i in range(1, 10, 2):
        print(i, end = " ")
    print()

    done()

    print("6. Using a while loop, print the odd numbers between 0 and 10")

    i = 1

    while i < 10:
        print(i, end = " ")
        i += 2
    print()

    done()

    print("7. Using a for loop, print the squares of the numbers between 0 and 10")

    for i in range (0, 11):
        print(i*i, end=" ")
    print()

    done()

    print("8. Using a while loop, print the squares of the numbers between 0 and 10")

    i = 0

    while i < 11:
        print(i*i, end = " ")
        i += 1
    print()

    done()

    print("9. Using a for loop, print all the elements in a given list")

    for item in word_list:
        print(item, end=" ")
    print()

    done()
    
    print("10. Using a while loop, print all the elements in a given list")

    i = 0
    while i < len(word_list):
        print(word_list[i], end=" ")
        i += 1
    print()

    done()

    print("11. Using a for loop, print all the elements in a given list in reverse order")

    for item in word_list[::-1]:
        print(item, end=" ")
    print()

    done()

    print("12. Using a while loop, print all the elements in a given list in reverse order")
    i = len(word_list)-1
    while i >= 0:
        print(word_list[i], end = " ")
        i -= 1
    print()

    done()

    print("13. Using a for loop, find the sum of all the elements in a given list")

    print(f"List: {number_list}")

    total = 0
    for number in number_list:
        total += number
    print(total)

    done()

    print("14. Using a while loop, find the sum of all the elements in a given list")

    print(f"List: {number_list}")
    
    total = 0
    count = 0
    while count < len(number_list):
        total += number_list[count]
        count += 1
    print(total)

    done()
    
    print("15. Using a for loop, find the product of all the elements in a given list")

    print(f"List: {number_list}")

    total = 0
    for number in number_list:
        if total == 0:
            total = number
        else:
            total *= number
    print(total)

    done()

    print("16. Using a while loop, find the product of all the elements in a given list")

    print(f"List: {number_list}")
    
    total = 0
    count = 0
    while count < len(number_list):
        if count == 0:
            total = number_list[count]
        else:
            total *= number_list[count]
        count += 1
    print(total)

    done()

    print("17. Using a for loop, find the largest element in a given list")

    print(f"List: {number_list}")

    largest = ""
    for number in number_list:
        if largest == "":
            largest = number
        else:
            if largest < number:
                largest = number
    print(largest)

    done()

    print("18. Using a while loop, find the largest element in a given list")

    print(f"List: {number_list}")

    i = 0
    largest = ""

    while i < len(number_list):
        if largest == "":
            largest = number_list[i]
        else:
            if largest < number_list[i]:
                largest = number_list[i]
        i += 1
    print(largest)

    done()

    print("19. Using a for loop, find the smallest element in a given list")

    print(f"List: {number_list}")

    smallest = ""
    for number in number_list:
        if smallest == "":
            smallest = number
        else:
            if smallest > number:
                smallest = number
    print(smallest)

    done()

    print("20. Using a while loop, find the smallest element in a given list")

    print(f"List: {number_list}")

    i = 0
    smallest = ""

    while i < len(number_list):
        if smallest == "":
            smallest = number_list[i]
        else:
            if smallest > number_list[i]:
                smallest = number_list[i]
        i += 1
    print(smallest)

    done()

    print("21. Using a for loop, print all the elements in a given list except multiples of 3")

    print(f"List: {number_list}")

    for item in number_list:
        if item % 3 == 0:
            continue
        print(item, end=" ")
    print()

    done()

    print("22. Using a while loop, print all the elements in a given list except multiples of 3")

    print(f"List: {number_list}")

    i = 0
    while i < len(number_list):
        if number_list[i] % 3 == 0:
            i += 1
            continue
        print(number_list[i], end=" ")
        i += 1
    print()

    done()

    print("23. Using a for loop, print all the elements in a given list until a specific element is reached")

    print(f"List: {number_list}")

    element_selection = random.choice(number_list)
    print(f"Element to reach: {element_selection}")

    for item in number_list:
        if item == element_selection:
            break
        print(item, end=" ")
    print()

    done()

    print("24. Using a while loop, print all the elements in a given list until a specific element is reached")

    print(f"List: {number_list}")
    print(f"Element to reach: {element_selection}")

    i = 0
    while i < len(number_list):
        if number_list[i]== element_selection:
            break
        print(number_list[i], end=" ")
        i += 1
    print()

    done()

    print("25. Using a for loop, print all the elements in a given list that are divisible by 2 and 3")

    print(f"List: {number_list}")

    for item in number_list:
        if item % 2 == 0:
            if (item/2) % 3 == 0:
                print(item, end=" ")
    print()

    done()

main()
