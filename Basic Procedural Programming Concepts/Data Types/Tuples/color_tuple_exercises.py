'''color_tuple_exercises: Exercises covering tuples, using colors as examples.'''

def main():
    # Create a tuple called "colors" with the elements ("red",
    # "green", "blue")
    colors = ("red", "green", "blue")

    # Print the length of the "colors" tuple
    print(len(colors))

    # Print the second element of the "colors" tuple
    print(colors[1])

    # Use slicing to print a sub-tuple of the "colors" tuple
    # containing the elements ("green", "blue")
    print(colors[1:3])

    # Check if the element "red" is in the "colors" tuple and print
    # the result
    print("red" in colors)

    # Use the "index()" method to find the index of the element
    # "green" in the "colors" tuple and print the result
    print(colors.index("green"))

    # Use the "count()" method to count the number of occurrences of
    # the element "blue" in the "colors" tuple and print the result
    print(tuple.count(colors, "blue"))

    # Use the "max()" method to find the maximum element in the
    # "colors" tuple and print the result
    print(max(colors))

    # Use the "min()" method to find the minimum element in the
    # "colors" tuple and print the result
    print(min(colors))

    # Use the "sum()" method to find the sum of a tuple of numbers (1,
    # 2, 3, 4, 5) and print the result
    print(sum((1,2,3,4,5)))

    # Use the "+" operator to concatenate the "colors" tuple with the
    # tuple ("yellow", "orange") and print the result
    colors = colors + ("yellow", "orange")
    print(colors)

    # Use the "*" operator to repeat the "colors" tuple three times
    # and print the result
    colors = colors * 3
    print(colors)

    # Use the "in" keyword to check if the element "green" is in the
    # "colors" tuple and print the result
    print("green" in colors)

    # Use the "not in" keyword to check if the element "purple" is not
    # in the "colors" tuple and print the result
    print("purple" not in colors)

    # Use the "tuple()" function to convert a list ["red", "green",
    # "blue"] to a tuple and print the result
    print(tuple(["red", "green", "blue"]))

    # Use the "list()" function to convert a tuple ("red", "green",
    # "blue") to a list and print the result
    print(list(("red", "green", "blue")))

    # Use the "len()" function to find the length of the "colors"
    # tuple and print the result
    print(len(colors))

    # Use a for loop to print each element in the "colors" tuple
    for color in colors:
        print(color)

    # Use the "enumerate()" function to print the index and value of
    # each element in the "colors" tuple
    for index, value in enumerate(colors):
        print("Index:", index, "\tColor:", value)

    # Use the "zip()" function to combine two tuples ("red", "green",
    # "blue") and ("yellow", "orange") and print the result
    for color in zip(("red", "green", "blue"), ("yellow", "orange")):
        print(color)

    # Use the "sorted()" function to sort the elements of the "colors"
    # tuple and print the result
    print(sorted(colors))

    # Use the "reversed()" function to reverse the elements of the
    # "colors" tuple and print the result
    print(reversed(colors))

    # Use the "join()" function to join the elements of the "colors"
    # tuple with a separator "-" and print the result
    print("-".join(colors))

    # Use the "slice()" function to extract a sub-tuple from the
    # "colors" tuple containing the elements ("green", "blue")
    color_pair = slice(1,3)
    print(colors[color_pair])

    # Use the "format()" method to insert a tuple into a string and
    # print the result.
    string = "{}.  My name is {} {}.  You {} my {}.  Prepare to {}."
    mad_libs = ("Hello", "Inigo", "Montoya", "killed", "father", "die")
    print(string.format(*mad_libs))

main()
