'''hello: "Hello World" program consisting of exercises around the
string named "greeting"'''


def main():
    # Exercise 1: Create a string variable called "greeting" and
    # assign it the value "Hello, World!"
    greeting = "Hello, World!"

    # Exercise 2: Print the length of the string stored in the
    # "greeting" variable
    print("String Length: ", len(greeting))

    # Exercise 3: Concatenate the string "Nice to meet you, " with the
    # "greeting" variable and print the result. Do not change the
    # "greeting" variable.
    print(greeting + "  Nice to meet you!")

    # Exercise 4: Print the third character of the "greeting" variable
    print(greeting[3])

    # Exercise 5: Use slicing to print the substring "World" from the
    # "greeting" variable
    print(greeting[7:12])

    # Exercise 6: Check if the string "Hello" is a substring of the
    # "greeting" variable and print the result
    print("Hello" in greeting)

    # Exercise 7: Convert the "greeting" variable to uppercase and
    # print the result
    print(greeting.upper())

    # Exercise 8: Replace the substring "World" with "Universe" in the
    # "greeting" variable and print the result
    greeting = greeting.replace("World", "Universe")
    print(greeting)

    # Exercise 9: Split the "greeting" variable into a list of words
    # and print the result
    print(greeting.split())

    # Exercise 10: Join a list of words ["Nice", "to", "meet", "you"]
    # using a space character as the separator and print the result
    nice_list = ["Nice", "to", "meet", "you"]
    nice = " ".join(nice_list)
    print(nice)

    # Exercise 11: Use the "in" keyword to check if the letter "o" is
    # in the "greeting" variable and print the result
    print("o" in greeting)

    # Exercise 12: Use the "find()" method to find the index of the
    # first occurrence of the letter "o" in the "greeting" variable
    # and print the result
    print(greeting.find("o"))

    # Exercise 13: Use the "count()" method to count the number of
    # occurrences of the letter "o" in the "greeting" variable and
    # print the result
    print(greeting.count("o"))
    # NOTE: Remember that "World" was changed to "Universe"

    # Exercise 14: Use the "strip()" method to remove whitespace
    # characters from the beginning and end of the "greeting" variable
    # and print the result
    print(greeting.strip())

    # Exercise 15: Use the "replace()" method to replace all
    # occurrences of the letter "o" with the letter "a" in the
    # "greeting" variable and print the result
    greeting = greeting.replace("o", "a")
    print(greeting)

    # Exercise 16: Use the "startswith()" method to check if the
    # "greeting" variable starts with the letter "H" and print the
    # result
    print(greeting.startswith("H"))

    # Exercise 17: Use the "endswith()" method to check if the
    # "greeting" variable ends with the letter "!" and print the
    # result
    print(greeting.endswith("!"))

    # Exercise 18: Use the "format()" method to insert a variable into
    # a string and print the result
    print("You have clearance, {}!".format("Clarence"))
    string = "Roger, {}!  What's our vector, {}?"
    first_name = "Roger"
    second_name = "Victor"
    print(string.format(first_name, second_name))

    # Exercise 19: Use the "escape" sequences to include a newline and
    # tab characters in a string and print the result
    print("One\tTwo\tThree\tFour\n1\t2\t3\t4")

    # Exercise 20: Create a string with multiple lines using triple
    # quotes and print the result
    print(
        """
    This is the first line.
    this is the second line.
    This is the final line.
    """
    )

    # Exercise 21: Use the "isalpha()" method to check if the
    # "greeting" variable contains only alphabetic characters and
    # print the result.
    print(str.isalpha(greeting))

    # Exercise 22: Use the "isalnum()" method to check if the
    # "greeting" variable contains only alphanumeric characters and
    # print the result
    print(str.isalnum(greeting))

    # Exercise 23: Use the "isdigit()" method to check if the
    # "greeting" variable contains only digits and print the result
    print(str.isdigit(greeting))

    # Exercise 24: Use the "islower()" method to check if all the
    # characters in the "greeting" variable are lowercase and print
    # the result
    print(str.islower(greeting))

    # Exercise 25: Use the "isupper()" method to check if all the
    # characters in the "greeting" variable are uppercase and print
    # the result
    print(str.isupper(greeting))


main()
