"""regular_expressions.py: This is a series of exercises covering regular expressions."""

# The original module name is pretty vague.  Using "regex" instead of
# "re" should make it more descriptive.
import re


def done():
    """Marks the end of an exercise"""
    print("\n---\n")


def string() -> str:
    """Generates the string for exercises without being mutable itself."""
    return """John Doe was born in 1990 in the city of New York. He
studied computer science at the University of California, Los
Angeles from 2008 to 2012. After graduation, he worked as a
software engineer at Google for 5 years. In 2017, he decided
to start his own company, Doe Inc., which specializes in
developing artificial intelligence solutions. Today, the
company has a global presence and is considered a leader in
its field."""

def main():
    """Main function."""

    # Note, the functions above are meant to practice the Python re
    # module.  However, all the exercises below are meant to practice
    # the regular expressions themselves.

    print("1. Extract the name of the person.")

    pattern = "^(\\w* \\w*)"

    print(re.findall(pattern, string()))

    done()

    print("2. Extract the year of birth.")

    pattern = "was born in (\\d*)"

    print(re.findall(pattern, string()))

    done()

    print("3. Extract the name of the city where the person was born.")

    pattern = "in the city of (\\w*\\s?\\w*?)\\."

    print(re.findall(pattern, string()))

    done()

    print("4. Extract the name of the university where the person studied.")

    pattern = "studied (?:\\w*\\s)*at the ((?:\\w*[,\\s]*?)+) from"

    result = re.findall(pattern, string())

    # for university in range(0, len(result)):
    for university in enumerate(result):
        result[university[0]] = replace_in_string(result[university[0]], " ", "\\n")

    print(result)

    done()

    print("5. Extract the years during which the person studied at the university.")

    pattern = "from (\\d+) to (\\d+)"

    print(re.findall(pattern, string()))

    done()

    print(
        "6. Extract the name of the company where the person worked after graduation."
    )

    pattern = "fter graduation, he worked as (?:\\w+[\\s,])+at\\s((?:\\w+\\s?)+) for"

    print(re.findall(pattern, string()))

    done()

    print("7. Extract the length of time the person worked at the company.")

    pattern = "for ((?:\\d+) (?:\\w+))."

    print(re.findall(pattern, string()))

    done()

    print("8. Extract the name of the company started by the person.")

    pattern = "his own company, ((?:\\w+[ ,.]+)+), which"

    print(re.findall(pattern, string()))

    done()

    print("9. Extract the field in which the person's company specializes.")

    pattern = "specializes in\\s((?:\\w+\\s)+(?:\\w+))"

    print(re.findall(pattern, string()))

    done()

    print('10. Verify if the string contains the word "California".')

    contains_california = re.search("California", string())

    print(f'String contains the word "California": {bool(contains_california)}')

    done()

    print('11. Verify if the string contains the phrase "software engineer".')

    contains_software_engineer = re.search("software\\sengineer", string())

    print(
        f'String contains the phrase "software engineer": {bool(contains_software_engineer)}'
    )

    done()

    print('12. Replace the word "University" with "College" throughout the string.')

    pattern = "University"
    replace = "College"

    print(re.sub(pattern, replace, string()))

    done()

    print('13. Replace the word "software engineer" with "developer".')

    pattern = "software\\sengineer"
    replace = "developer"

    print(re.sub(pattern, replace, string()))

    done()

    print('14. Remove the word "the" from the beginning of every line in the string.')

    pattern = "^to\\s"

    print(re.sub(pattern, "", string(), flags=re.MULTILINE))

    done()

    print("15. Extract all the years mentioned in the string.")

    pattern = "(\\d{4})"

    print(re.findall(pattern, string()))

    done()

    print("16. Extract all the cities mentioned in the string.")

    pattern = (
        "city of ((?:\\w+\\s*)+)|(?:University of California, )((?:\\w+\\s*)+) from"
    )

    cities = re.findall(pattern, string())
    result = []

    for pair in cities:
        if pair[0] != "":
            result.append(pair[0])
        else:
            result.append(pair[1])

    for city in enumerate(result):
        result[city[0]] = re.sub("\\n", " ", result[city[0]])

    print(result)

    done()

    print("17. Verify if the string contains any numbers.")

    print(bool(re.search("[\\d]", string())))

    done()

    print('18. Replace all numbers in the string with the word "NUMBER".')

    print(re.sub("\\d+", "NUMBER", string()))

    done()

    print("19. Extract the initials of the person (i.e. JD).")

    pattern = "^(\\w* \\w*)"

    name = re.findall(pattern, string())

    pattern = "\\b(\\w)"

    initials = "".join(re.findall(pattern, name[0]))

    print(initials)

    done()

    print("20. Find all instances of a two word company (e.g. Doe Inc.).")

    pattern = "((?:\\w+) Inc.)"

    print(re.findall(pattern, string()))

    done()

    print('21. Extract all words that start with the letter "J".')

    pattern = "\\b(J\\w+)"

    print(re.findall(pattern, string()))

    done()

    print('22. Replace all instances of "he" with "she".')

    stage_1 = re.sub("\\bhe\\b", "she", string())
    stage_2 = re.sub("\\bHe\\b", "She", stage_1)

    print(stage_2)

    done()

    print('23. Replace all instances of "His" with "Her".')

    stage_3 = re.sub("\\bhis\\b", "her", stage_2)
    stage_4 = re.sub("\\bHis\\b", "Her", stage_3)

    print(stage_4)

    done()

    print('24. Replace all instances of "he\'s" with "she\'s".')

    stage_5 = re.sub("\\bhe's\\b", "she's", stage_4)
    stage_6 = re.sub("\\bHe's\\b", "She's", stage_5)

    print(stage_6)

    done()

    print("25. Extract all occurrences of four-digit numbers (e.g. 1990).")

    print(re.findall("(\\d{4})", string()))

    done()


main()
