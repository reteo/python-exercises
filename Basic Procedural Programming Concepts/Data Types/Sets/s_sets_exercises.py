'''s_sets_exercises: Exercises covering the use of sets in Python'''

def main():
    # Create an empty set called "s1" and print it.

    s1 = set()
    print(s1)

    # Add the numbers 1, 2, and 3 to the set "s1" and print the
    # result.

    s1.update({1, 2, 3})
    print(s1)

    # Remove the number 2 from the set "s1" and print the result.

    s1.remove(2)
    print(s1)

    # Use the union() method to merge set "s1" with set {4,5} and save
    # the result in set "s1" and print the result.
    s1 = s1.union({4,5})
    print(s1)

    # Use the intersection() method to find common elements in set
    # "s1" and set {5,6} and save the result in set "s2" and print the
    # result.
    s2 = s1.intersection({5, 6})
    print(s2)

    # Use the difference() method to find the elements in set "s2"
    # that are not in set {6,7} and save the result in set "s3" and
    # print the result.
    s3 = s2.difference({6, 7})
    print(s3)

    # Use the symmetric_difference() method to find elements that are
    # unique to each set "s2" and {6,7} and save the result in set
    # "s4" and print the result.
    s4 = s2.symmetric_difference({6, 7})
    print(s4)

    # Check if set "s2" is a subset of set {6,7} using the issubset()
    # method and print the result.
    print(s2.issubset({6,7}))
    

    # Check if set {6,7} is a superset of set "s2" using the
    # issuperset() method and print the result.
    print({6,7}.issuperset(s2))

    # Clear the set "s2" using the clear() method and print the
    # result.
    s2.clear()
    print(s2)

    # Use the update() method to add elements {8,9,10} to set "s1" and
    # print the result.
    s1.update({8, 9, 10})
    print(s1)

    # Use the intersection_update() method to keep only the common
    # elements between set "s1" and {10,11,12} and save the result in
    # set "s1" and print the result.
    s1.intersection_update({10, 11, 12})
    print(s1)

    # Use the difference_update() method to remove the common elements
    # between set "s1" and {11,12,13} and print the result.
    s1.difference_update({11, 12, 13})
    print(s1)

    # Use the symmetric_difference_update() method to update set "s1"
    # with elements that are unique to each set "s1" and {12,13,14}
    # and print the result.
    s1.symmetric_difference_update({12, 13, 14})
    print(s1)

    # Use the isdisjoint() method to check if set "s1" has any common
    # elements with set {13,14,15} and print the result
    print(s1.isdisjoint({13, 14, 15}))

    # Use the add() method to add the number 16 to the set "s1" and
    # print the result.
    s1.add(16)
    print(s1)

    # Use the remove() method to remove the number 16 from the set
    # "s1" and print the result.
    s1.remove(16)
    print(s1)

    # Use the discard() method to remove the number 14 from the set
    # "s1" and print the result.
    s1.discard(14)
    print(s1)

    # Use the pop() method to remove a random element from the set
    # "s1" and print the result.
    print(s1.pop())
    print(s1)

    # Use the clear() method to empty the set "s1" and print the
    # result.
    s1.clear()
    print(s1)

    # Create a set "s5" with elements {1,2,3} and "s6" with elements
    # {3,4,5}, use the copy() method to copy the elements of set "s5"
    # to "s6" and print the result.
    s5 = {1, 2, 3}
    print(s5)
    s6 = {3, 4, 5}
    print(s6)
    s6 = s5.copy()
    print(s6)

    # Create a set "s7" with elements {1,2,3}, use the frozen set()
    # method to make it immutable and try to add an element to it and
    # see if it raises an error.
    s7 = {1, 2, 3}
    s7 = frozenset(s7)
    print(type(s7))
    print(s7)
    #s7.add(12)
    # Error occurred: AttributeError: 'frozenset' object has
    # no attribute 'add'
    
    # Create a set "s8" with elements {1,2,3} and use the set()
    # constructor to create a new set "s9" from it and print the
    # result.
    s8 = {1, 2, 3}
    s9 = set(s8)
    print(s9)

    # Create a set "s10" with elements {1,2,3} and use a set
    # comprehension to create a new set "s11" that contains the
    # squared values of the elements in "s10" and print the result.
    s10 = {1, 2, 3}
    s11 = {x for x in s10}
    print(s11)

    # Create a set "s12" with elements {1,2,3,4,5} and use a set
    # comprehension to create a new set "s13" that contains the
    # elements from "s12" that are even numbers and print the result.
    s12 = {1, 2, 3, 4, 5}
    s13 = {x for x in s12 if x % 2 == 0}
    print(s12, s13)

main()
