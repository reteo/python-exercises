'''math_exercises: Exercises regarding numbers and mathematics.'''
from math import isinf, isnan, sqrt, pi

def main():
    # Exercise 1: Create two variables, "a" and "b" and assign them
    # the values 5 and 10 respectively.
    a = 5
    b = 10
    
    # Exercise 2: Print the sum of "a" and "b"
    print(a+b)
    
    # Exercise 3: Print the difference of "a" and "b"
    print(a-b)
    
    # Exercise 4: Print the product of "a" and "b"
    print(a*b)
    
    # Exercise 5: Print the quotient of "a" and "b"
    print(a/b)
    
    # Exercise 6: Print the remainder of "a" divided by "b"
    print(a%b)
    
    # Exercise 7: Print the result of "a" raised to the power of "b"
    print(a**b)

    # Exercise 8: Use the math module to import the "sqrt()" function
    # and print the square root of "a"
    print(sqrt(a))

    # Exercise 9: Use the math module to import the "pi" constant and
    # print the result of "a" multiplied by pi
    print(a*pi)

    # Exercise 10: Use the "round()" function to round "a" to the
    # nearest integer and print the result
    print(round(a))

    # Exercise 11: Use the "abs()" function to get the absolute value
    # of "b" and print the result
    print(abs(b))

    # Exercise 12: Use the "divmod()" function to get the quotient
    # and remainder of "a" divided by "b" and print the result
    print(divmod(a, b))

    # Exercise 13: Use the "complex()" function to create a complex
    # number with "a" as the real part and "b" as the imaginary part
    # and print the result
    print(complex(a, b))

    # Exercise 14: Use the "float()" function to convert "a" to a
    # float and print the result
    print(float(a))

    # Exercise 15: Use the "int()" function to convert "b" to an
    # integer and print the result
    print(int(b))

    # Exercise 16: Use the "max()" function to find the maximum
    # between "a" and "b" and print the result
    print(max(a, b))

    # Exercise 17: Use the "min()" function to find the minimum
    # between "a" and "b" and print the result
    print(min(a, b))

    # Exercise 18: Use the "sum()" function to find the sum of a list
    # of numbers [1, 2, 3, 4, 5] and print the result
    print(sum([1,2,3,4,5]))

    # Exercise 19: Use the "pow()" function to find "a" raised to the
    # power of "b" and print the result
    print(pow(a, b))

    # Exercise 20: Use the "hex()" function to convert "a" to a
    # hexadecimal string and print the result
    print(hex(a))

    # Exercise 21: Use the "oct()" function to convert "b" to an
    # octal string and print the result
    print(oct(b))

    # Exercise 22: Use the "bin()" function to convert "a" to a
    # binary string and print the result
    print(bin(a))

    # Exercise 23: Use the "isnan()" function to check if "a" is "not
    # a number" and print the result
    print(isnan(a))

    # Exercise 24: Use the "isinf()" function to check if "b" is
    # "infinity" and print the result
    print(isinf(b))

    # Exercise 25: Use the type() or isinstance() function to check if
    # "a" is an integer and print the result.
    print(isinstance(a, int))

main()
