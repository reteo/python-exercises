'''conversion_exercises: data structure conversion exercises'''

def main():
    ### Atomic variable (single-value) conversions.

    # 1. Convert the string "10" to an integer using the int()
    # function

    # 2. Convert the integer 20 to a string using the str() function

    # 3. Convert the float 3.14 to a string using the str() function

    # 4. Convert the string "True" to a boolean using the bool()
    # function

    # 5. Convert the boolean value False to a string using the str()
    # function

    # 6. Convert the string "25.6" to a float using the float()
    # function

    # 7. Convert the integer 45 to a boolean using the bool() function

    # 8. Convert the string "100" to an integer, then multiply it by 2
    # and convert the result to a string

    # 9. Convert the float 7.5 to an integer using the int() function

    # 10. Convert the string "35" to an integer, then add 5 and
    # convert the result to a string

    # 11. Convert the boolean value True to an integer using the int()
    # function

    # 12. Convert the float 3.14 to a string, then convert the string
    # to a float again

    # 13. Convert the integer 100 to a string, then convert the string
    # to an integer again

    # 14. Convert the boolean value False to a string, then convert
    # the string to a boolean again

    # 15. Convert the string "0" to a boolean using the bool()
    # function

    # 16. Convert the string "1" to an integer, then convert the
    # integer to a boolean using the bool() function

    # 17. Convert the integer 5 to a string, then convert the string
    # to a float

    # 18. Convert the float 4.2 to an integer, then convert the
    # integer to a string

    # 19. Convert the string "True" to a boolean, then convert the
    # boolean to an integer using the int() function

    # 20. Convert the integer 8 to a string, then convert the string
    # to a boolean using the bool() function

    # 21. Convert the string "3.14" to a float, then convert the float
    # to an integer using the int() function

    # 22. Convert the boolean value True to a string, then convert the
    # string to an integer

    # 23. Convert the string "25" to an integer, then convert the
    # integer to a float using the float() function

    # 24. Convert the string "false" to a boolean using the bool()
    # function

    # 25. Convert the float 2.5 to a string, then convert the string
    # to a boolean using the bool() function   
    
    ### Collection or container (lists, tuples, sets. etc.)
    ### conversions.

    # 1. Convert a list [1, 2, 3] to a tuple using the tuple()
    # function

    # 2. Convert a tuple (4, 5, 6) to a list using the list() function

    # 3. Convert a dictionary {"a": 1, "b": 2} to a list using the
    # items() method

    # 4. Convert a list of tuples [("c", 3), ("d", 4)] to a dictionary
    # using the dict() function

    # 5. Convert a set {5, 6, 7} to a list using the list() function

    # 6. Convert a list [8, 9, 10] to a set using the set() function

    # 7. Convert a tuple (11, 12, 13) to a dictionary, where each
    # element in the tuple becomes a key-value pair, where the key is
    # the element's index and the value is the element

    # 8. Convert a dictionary {"x": 14, "y": 15} to a tuple using the
    # items() method

    # 9. Convert a list [16, 17, 18] to a array using the array()
    # function

    # 10. Convert a array (51, 52, 53) to a list using the tolist()
    # method

    # 11. Convert a tuple (19, 20, 21) to a array using the array()
    # function

    # 12. Convert a dictionary {"a": 22, "b": 23} to a array using the
    # array() function

    # 13. Convert a set {24, 25, 26} to a array using the array()
    # function

    # 14. Convert a array (54, 55, 56) to a dictionary, where each
    # element in the array becomes a key-value pair, where the key is
    # the element's index and the value is the element

    # 15. Convert a tuple (27, 28, 29) to a set using the set()
    # function

    # 16. Convert a list [30, 31, 32] to a dictionary, where each
    # element in the list becomes a key-value pair, where the key is
    # the element's index and the value is the element

    # 17. Convert a dictionary {"c": 33, "d": 34} to a set using the
    # items() method

    # 18. Convert a array (57, 58, 59) to a tuple using the tuple()
    # function

    # 19. Convert a set {35, 36, 37} to a dictionary, where each
    # element in the set becomes a key-value pair, where the key is
    # the element's index and the value is the element

    # 20. Convert a tuple (38, 39, 40) to a list

    # 21. Convert a list [41, 42, 43] to a array using the array()
    # function and set the data type as int

    # 22. Convert a array (60, 61, 62) to a set using the set()
    # function

    # 23. Convert a dict {44: "a", 45: "b"} to a tuple of tuples using
    # the items() method

    # 24. Convert a tuple (46, 47, 48) to a dict using the dict()
    # function

    # 25. Convert a set {49, 50} to a array using the array() function
    # and set the data type as float




main()
