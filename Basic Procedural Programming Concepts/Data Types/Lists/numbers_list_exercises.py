'''numbers_list_exercises: A list of exercises covering lists.'''

def main():
    # Create a list called "numbers" with the elements [1, 2, 3, 4, 5]
    numbers = [1,2,3,4,5]

    # Print the length of the "numbers" list
    print(len(numbers))

    # Print the third element of the "numbers" list
    print(numbers[2])

    # Use slicing to print a sublist of the "numbers" list containing
    # the elements [2, 3, 4]
    print(numbers[1:4])

    # Add the element 6 to the "numbers" list
    numbers.append(6)

    # Remove the element 2 from the "numbers" list
    numbers.remove(2)

    # Check if the element 4 is in the "numbers" list and print the
    # result
    print(4 in numbers)

    # Use the "index()" method to find the index of the element 3 in
    # the "numbers" list and print the result
    print(list.index(numbers, 3))

    # Use the "count()" method to count the number of occurrences of
    # the element 5 in the "numbers" list and print the result
    print(list.count(numbers, 5))

    # Use the "sort()" method to sort the "numbers" list in ascending
    # order and print the result
    numbers.sort()
    print(numbers)

    # Use the "reverse()" method to reverse the order of the "numbers"
    # list and print the result
    numbers.reverse()
    print(numbers)

    # Use the "append()" method to add the element 7 to the end of the
    # "numbers" list
    numbers.append(7)

    # Use the "insert()" method to insert the element 0 at index 2 in
    # the "numbers" list
    numbers.insert(2, 0)

    # Use the "extend()" method to add the elements [8, 9, 10] to the
    # end of the "numbers" list
    numbers.extend([8, 9, 10])

    # Use the "pop()" method to remove and return the last element of
    # the "numbers" list
    print(numbers.pop())

    # Use the "remove()" method to remove the first occurrence of the
    # element 5 from the "numbers" list
    numbers.remove(5)

    # Use the "+" operator to concatenate the "numbers" list with the
    # list [6, 7, 8] and print the result
    numbers = numbers + [6, 7, 8]
    print(numbers)

    # Use the "*" operator to repeat the "numbers" list three times
    # and print the result
    numbers = numbers * 3
    print(numbers)

    # Use the "in" keyword to check if the element 3 is in the
    # "numbers" list and print the result
    print(3 in numbers)

    # Use the "not in" keyword to check if the element 0 is not in the
    # "numbers" list and print the result
    print(0 not in numbers)

    # Use the "del" keyword to remove the element at index 2 from the
    # "numbers" list
    del numbers[2]

    # Use the "enumerate()" function to print the index and value of
    # each element in the "numbers" list
    for index, value in enumerate(numbers):
        print("Index:", index, "\tValue:", value)

    # Use a for loop to print the square of each element in the
    # "numbers" list.
    for number in numbers:
        print(number ** 2)

    # Use the "copy()" method to create a shallow copy of the
    # "numbers" list
    new_numbers = numbers.copy()

    # Use the "clear()" method to remove all elements from the
    # "numbers" list
    numbers.clear()
    print("Original list:", numbers)
    print("Copied list:", new_numbers)


main()
