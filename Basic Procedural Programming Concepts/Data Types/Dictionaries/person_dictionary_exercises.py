'''person_dictionary_exercises: Exercises for Python dictionaries
using data about people.'''

def main():
    # Create a dictionary called "person" with the keys "name", "age",
    # "gender" and respective values "John", 25, "male"
    person = {
        "name": "John",
        "age": 25,
        "gender": "male"
    }

    # Print the length of the "person" dictionary
    print(len(person))

    # Print the value of the key "name" in the "person" dictionary
    print(person["name"])

    # Use the "keys()" method to print all the keys in the "person"
    # dictionary
    print(person.keys())

    # Use the "values()" method to print all the values in the
    # "person" dictionary
    print(person.values())

    # Use the "items()" method to print all the key-value pairs in the
    # "person" dictionary
    print(person.items())

    # Add a new key-value pair "address" : "New York" to the "person"
    # dictionary
    person["address"] = "New York"

    # Remove the key "age" from the "person" dictionary
    del person["age"]

    # Check if the key "gender" is in the "person" dictionary and
    # print the result
    print("gender" in person)

    # Use the "get()" method to retrieve the value of the key "name"
    # in the "person" dictionary and print the result
    print(person.get("name"))

    # Use the "update()" method to update the "person" dictionary with
    # the key-value pairs {"age": 30, "address": "Los Angeles"}
    person.update({"age": 30, "address": "Los Angeles"})

    # Use the "pop()" method to remove and return the value of the key
    # "gender" from the "person" dictionary
    print(person.pop("gender"))

    # Use a for loop to print all the keys in the "person" dictionary
    for key in person.keys():
        print("Key:", key)

    # Use a for loop to print all the values in the "person"
    # dictionary
    for value in person.values():
        print("Value:", value)

    # Use a for loop to print all the key-value pairs in the "person"
    # dictionary
    for (key, value) in person.items():
        print ("Key:", key, "\tValue:", value)

    # Use the "setdefault()" method to add a new key-value pair to the
    # "person" dictionary if the key does not exist
    employee = person.setdefault("job", "unemployed")
    print(employee)

    # Use the "items()" method to loop through the "person" dictionary
    # and update the value of each key
    for (key, value) in person.items():
        if type(person[key]) == str:
            person[key] = person[key].lower()
        elif type(person[key]) == int:
            person[key] += 1
    print(person)

    # Use the "keys()" method to loop through a copy of the "person"
    # dictionary and remove a specific key-value pair.
    for key in list(person.keys()):
        if key == "job":
            del person[key]
    print(person)

    # Use the "items()" method to check if a value exists in the
    # "person" dictionary and return the key
    for key, value in person.items():
        if type(value) == str:
            if value == "john":
                print(key)
    
    # Use the "fromkeys()" method to create a new dictionary with
    # specified keys and default value

    animal = dict.fromkeys(("type", "breed"))
    print(animal)

    # Use the "popitem()" method to remove and return an arbitrary
    # key-value pair from the "person" dictionary

    person.popitem()

    # Use the "update()" method to merge one dictionary with another
    # dictionary

    animal.update(person)
    print(animal)

    # Use the "values()" method to count the number of occurrences of
    # a specific value in the "person" dictionary.

    count = 0
    for value in person.values():
        if value == "john":
            count += 1
    print(count)

    # Use the "copy()" method to create a shallow copy of the "person"
    # dictionary
    new_person = person.copy()

    # Use the "clear()" method to remove all key-value pairs from the
    # "person" dictionary
    person.clear()
    print("Original:", person)
    print("Copy:", new_person)

main()
