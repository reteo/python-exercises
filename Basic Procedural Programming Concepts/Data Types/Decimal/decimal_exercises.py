"""

Decimal Exercises from ChatGPT

"""

# Required for the exercises in this code.
import decimal
import random
import math

CURRENCY = decimal.Decimal("0.01")


def done():
    """Creates a line indicating the conclusion of an exercise."""
    print("\n---\n")


# For exercise 16
def dec_factorial(n: decimal.Decimal) -> decimal.Decimal:
    """Calculates factorial using Decimal objects"""
    if n == decimal.Decimal(0):
        return decimal.Decimal(1)
    else:
        return n * dec_factorial(n - decimal.Decimal(1))


# For exercise 18
def dec_euclidian(num1: decimal.Decimal, num2: decimal.Decimal) -> decimal.Decimal:
    if num1 == decimal.Decimal(0):
        return num2
    elif num2 == decimal.Decimal(0):
        return num1
    else:
        return dec_euclidian(num2, num1 % num2)

    
def main():
    print("1. Import the decimal module and set the precision to 5 decimal places.\n")

    decimal.getcontext().prec = 5

    value = decimal.Decimal(100) / decimal.Decimal(7)

    # Should be 14.286 instead of 14.2857142857... (which then repeats '142857')
    print("100 ÷ 7 =", value, "to 5 significant digits.")

    done()

    print(
        "2. Create two Decimal objects, representing the numbers 3.14159 and 2.71828. Add them together and display the result.\n"
    )

    PI = decimal.Decimal(3.14159)
    E_CONSTANT = decimal.Decimal(2.71828)

    print("π + e =", PI + E_CONSTANT)

    done()

    print(
        "3. Subtract the two Decimal objects created in exercise 2 and display the result.\n"
    )

    print("π - e =", PI - E_CONSTANT)

    done()

    print(
        "4. Multiply the two Decimal objects created in exercise 2 and display the result.\n"
    )

    print("π × e =", PI * E_CONSTANT)

    done()

    print(
        "5. Divide the two Decimal objects created in exercise 2 and display the result.\n"
    )

    print("π ÷ e =", PI / E_CONSTANT)

    done()

    print(
        "6. Calculate the square root of the first Decimal object created in exercise 2 using the decimal module.\n"
    )

    print("The square root of π is", PI ** decimal.Decimal(1 / 2))

    done()

    print(
        "7. Calculate the cube root of the second Decimal object created in exercise 2 using the decimal module.\n"
    )

    print("The cube root of e is", E_CONSTANT ** decimal.Decimal(1 / 3))

    done()

    print(
        "8. Round the first Decimal object created in exercise 2 to 3 decimal places using the decimal module.\n"
    )

    print(E_CONSTANT.quantize(decimal.Decimal("0.001")))

    done()

    print(
        "9. Compare the two Decimal objects created in exercise 2 using the decimal module's compare function.\n"
    )

    comparison = PI.compare(E_CONSTANT)

    print(
        f'π is {"greater than" if comparison == 1 else "less than" if comparison == -1 else "equal to"} e.'
    )

    done()

    print(
        "10. Calculate the absolute value of the subtraction of the two Decimal objects created in exercise 2.\n"
    )

    print(abs(E_CONSTANT - PI))

    done()

    print(
        "11. Create a list of 5 Decimal objects with different numbers. Use the decimal module to calculate the sum of all the Decimal objects in the list.\n"
    )

    objects = [
        decimal.Decimal(random.random() * 10).quantize(decimal.Decimal("0.01"))
        for _ in range(0, 5)
    ]

    print(objects, "\n" + str(sum(objects)))

    done()

    print(
        "12. Calculate the mean of the list of Decimal objects created in exercise 11.\n"
    )

    print(sum(objects) / len(objects))

    done()

    print(
        "13. Sort the list of Decimal objects created in exercise 11 in ascending order.\n"
    )

    print(sorted(objects))

    done()

    print(
        "14. Set the precision to 10 decimal places and create a new Decimal object representing the number 1.4142135624. Display the result.\n"
    )

    decimal.getcontext().prec = 10

    decimal_1 = decimal.Decimal(1.4142135624).quantize(decimal.Decimal("0.000000001"))
    print(decimal_1)

    done()

    print(
        "15. Perform a currency conversion using the decimal module. Convert 100.50 USD to EUR, given an exchange rate of 1 USD = 0.8536 EUR.\n"
    )

    conversion_ratio = decimal.Decimal(1) / decimal.Decimal(0.8536)
    dollars = decimal.Decimal(100.50)
    euros = dollars * conversion_ratio

    print(f"${dollars.quantize(CURRENCY)} = {euros.quantize(CURRENCY)} €")

    done()

    print(
        "16. Create a Decimal object representing the number 5. Calculate its factorial using the decimal module.\n"
    )

    decimal_2 = decimal.Decimal(5)
    print(dec_factorial(decimal_2))

    done()

    print(
        "17. Convert a given fraction (e.g., 3/7) to a Decimal object using the decimal module.\n"
    )

    decimal_3 = decimal.Decimal(3) / decimal.Decimal(7)

    print(decimal_3)

    done()

    print(
        "18. Calculate the greatest common divisor of two Decimal objects, 27 and 63, using the decimal module.\n"
    )

    decimal_4 = decimal.Decimal(27)
    decimal_5 = decimal.Decimal(63)

    print(dec_euclidian(decimal_4, decimal_5))

    done()

    print(
        "19. Calculate the least common multiple of two Decimal objects, 12 and 18, using the decimal module.\n"
    )

    decimal_6 = decimal.Decimal(12)
    decimal_7 = decimal.Decimal(18)

    print((decimal_6 * decimal_7)/dec_euclidian(decimal_6, decimal_7))

    done()

    print(
        "20. Use the decimal module to calculate the natural logarithm of the second Decimal object created in exercise 2.\n"
    )
    
    dec_log = decimal.Decimal(math.log(E_CONSTANT)).quantize(decimal.Decimal('0.0001'))
    
    print(dec_log)

    done()

    print(
        "21. Calculate the sine, cosine, and tangent of the first Decimal object created in exercise 2 using the decimal module.\n"
    )

    significance = decimal.Decimal('0.1')

    print("Sine:", decimal.Decimal(math.sin(PI)).quantize(significance))
    print("Cosine:", decimal.Decimal(math.cos(PI)).quantize(significance))
    print("Tangent:", decimal.Decimal(math.tan(PI)).quantize(significance))

    done()

    print(
        "22. Calculate the power of the first Decimal object created in exercise 2 raised to the second Decimal object created in exercise 2.\n"
    )

    print(PI**E_CONSTANT)

    done()

    print(
        "23. Create a Decimal object representing the number 123456.789. Format the number as a string with thousands separators using the decimal module.\n"
    )

    decimal_8 = 123456.789

    print(f"{decimal_8:,}")

    done()

    print(
        "24. Create a Decimal object representing the number -12345.6789. Format the number as a string with a leading '+' sign for positive numbers and a leading '-' sign for negative numbers using the decimal module.\n"
    )

    decimal_9 = decimal.Decimal(-123456.789)

    print(f"{decimal_9:+}")

    done()

    print(
        "25. Use the decimal module to calculate the compound interest for an initial investment of 10,000.00 USD, an annual interest rate of 5.5%, and a duration of 7 years.\n"
    )

    principal = decimal.Decimal(10000)
    interest = decimal.Decimal(0.055)
    duration = decimal.Decimal(7)

    # We'll assume continuously compounded
    result = (principal * (E_CONSTANT ** (interest * duration)))

    print(f'The result of continuously compunding ${principal:,} over {duration} years, with an interest rate of {interest.quantize(decimal.Decimal("0.0001")):%}, is ${result:,}.' )

    done()


main()
