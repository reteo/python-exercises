'''datetime_exercises: datetime exercises'''

import datetime, random, pytz

def done():
    print("\n---\n")

def random_date_time():
    return datetime.datetime(
        random.randint(2000, 2030),
        random.randint(1, 12),
        random.randint(1, 28),
        random.randint(0, 23),
        random.randint(0, 59),
        random.randint(0, 59),
    )

def random_date_string():
    return random_date_time().strftime("%Y/%m/%d")

def random_time_string():
    return random_date_time().strftime("%H:%M:%S")

def random_date_time_string():
    return random_date_time().strftime("%Y/%m/%d %H:%M:%S")

print('1. Convert a timestamp string to a datetime object.\n')

current_datetime = datetime.datetime.strptime(random_date_time_string(), "%Y/%m/%d %H:%M:%S")
print(current_datetime)

done()

print('2. Get the current date and time.\n')

print(datetime.datetime.now().strftime("%A, %B %d, %Y at %I:%M:%S %p"))

done()

print('3. Extract the year, month, day, hour, minute, and second from a datetime',
      'object.\n')

print(f"Current date/time: {current_datetime}")
print(f"Year: {current_datetime.year}")
print(f"Month: {current_datetime.month}")
print(f"Day: {current_datetime.day}")
print(f"Hour: {current_datetime.hour}")
print(f"Minute: {current_datetime.minute}")
print(f"Second: {current_datetime.second}")

done()

print('4. Format a datetime object as a string.\n')

now_time = datetime.datetime.now()
print(f"Now is: {now_time}")
print(now_time.strftime("%A, %B %d, %Y at %I:%M:%S %p"))

done()

print('5. Calculate the difference between two dates or times.\n')

date1 = random_date_time()
date2 = random_date_time()
delta = (date1-date2 if date1>date2 else date2-date1)

print("Dates:\n" +
      f"* {date1}\n" +
      f"* {date2}\n\n" +
      f"Length of time: {delta}")

done()

print('6. Determine the day of the week for a given date.\n')

print(random_date_time().strftime("Day of week for %B %d, %Y: %A"))

done()

print('7. Calculate the number of days between two dates.\n')

date1 = random_date_time()
date2 = random_date_time()
delta = (date1-date2 if date1>date2 else date2-date1)

print(f"Date 1: {date1}")
print(f"Date 2: {date2}")
print(f"Days Between: {delta.days}")

done()

print('8. Determine the date and time of the next occurrence of a specific day of',
      'the week.\n')

date = random_date_time()
date_midnight = date.replace(hour=0, minute=0, second = 0)
next_week = datetime.timedelta(days=7)
next_week_day_midnight = date_midnight + next_week

print(f"The {date.strftime('%A')} following {date.strftime('%A, %B %d, %Y')} is " +
      f"{next_week_day_midnight.strftime('%A, %B %d, %Y')}")


done()

# print('9. Get the date and time of the next full moon.\n')

# # Not working; pyright seems to go nuts when I try.  We'll just have
# # to leave it for another time.

# done()

print('10. Get the time zone information for a given location.\n')

print(pytz.timezone("US/Eastern"))

done()

print('11. Convert a datetime object to UTC time.\n')

tz1 = pytz.timezone("US/Eastern")
tz2 = pytz.timezone("UTC")

current_time = datetime.datetime.now()
eastern_time = tz1.localize(current_time, is_dst=None)
utc_time = eastern_time.astimezone(tz2)

print(f"Eastern: {eastern_time}\nUTC: {utc_time}")


done()

print('12. Add or subtract time from a datetime object.\n')

current_time = datetime.datetime.now()
to_be_elapsed = datetime.timedelta(hours=4)

future_time = current_time + to_be_elapsed

print(f"Current Time: {current_time}\nFuture Time: {future_time}")

done()

print('13. Find the dates for all weekends in a given month.\n')

def is_leap_year(year: int) -> bool:
    return year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)

def total_days(year: int, month: int) -> int:
    if month in (1, 3, 5, 7, 8, 10, 12):
        return 31
    if month in (4, 6, 9, 11):
        return 30
    if month == 2 and is_leap_year(year):
        return 29
    return 28

def weekend_dates(year: int, month: int) -> list[datetime.datetime]:
    
    START_DATE = datetime.datetime(year, month, 1)
    FIRST_DAY = 1
    LAST_DAY = total_days(year, month)

    weekend_list = []
    
    for day in range (0, LAST_DAY):
        current_day = START_DATE + datetime.timedelta(days = day)
        weekday = current_day.weekday()

        if weekday > 4:
            weekend_list.append(current_day)

    return weekend_list

print("Weekends in March of 2023:")
for date in weekend_dates(2023, 3):
    print(date.strftime("* %A, %B %d, %Y"))

done()

print('14. Calculate the number of business days between two dates.\n')

def weekday_count(year: int, month: int) -> int:
    
    START_DATE = datetime.datetime(year, month, 1)
    FIRST_DAY = 1
    LAST_DAY = total_days(year, month)

    weekday_count = 0
    
    for day in range (0, LAST_DAY):
        current_day = START_DATE + datetime.timedelta(days = day)
        weekday = current_day.weekday()

        if weekday < 5:
            weekday_count += 1

    return weekday_count

print(f"Weekdays in March of 2023: {weekday_count(2023, 3)}")

done()

print('15. Determine the next holiday.\n')

def next_holiday(date: datetime.datetime) -> tuple:

    HOLIDAYS = (
        (2, 2, "Groundhog Day"),
        (2, 14, "Valentine's Day"),
        (2, 20, "President's Day"),
        (2, 22, "Ash Wednesday")
    )

    for m, d, h in HOLIDAYS:
        if datetime.datetime(2023, m, d) > date:
            return (datetime.datetime(2023, m, d), h)

    return (datetime.datetime(1970, 1, 1), "NO HOLIDAY FOUND")

current_day = datetime.datetime(2023, 2, 10)
print(next_holiday(current_day))

done()

print('16. Get the number of seconds since the epoch.\n')

def since_epoch(desired_time: datetime.datetime) -> int:
    return int(
        (desired_time - datetime.datetime(1970,1,1,0,0,0))
        .total_seconds())

print(since_epoch(datetime.datetime.now()))

done()

print('17. Calculate the age of a person based on their birthdate.\n')

def age_from_birthdate(birthdate: datetime.datetime) -> int:
    return int((datetime.datetime.now() - birthdate).days / 365.25)

print(age_from_birthdate(datetime.datetime(1975, 2, 28)))

done()

# print('18. Find the time for the sunrise or sunset for a given location on a given',
#       'date.\n')

# # Not working; pyright seems to go nuts when I try.  We'll just have
# # to leave it for another time.

# done()

print('19. Determine the next payday.\n')

# My pay period is 2 weeks.
def paydate(last_paycheck: datetime.datetime) -> datetime.datetime:
    return last_paycheck + datetime.timedelta(days = 14)

print(paydate(datetime.datetime(2023, 2, 3)))

done()

print('20. Calculate the number of working hours between two dates.\n')

def get_working_hours(start: datetime.datetime, end: datetime.datetime) -> int:
    # I work 4 hours a day on all 5 business days.
    current = start
    step = datetime.timedelta(days=1)
    hours = 4
    total_hours = 0

    while current <= end:
        if current.weekday() < 5:
            total_hours += hours
        current += step        

    return total_hours

day_count = total_days(2023, 6)
start_date = datetime.datetime(2023, 6, 1)
end_date = datetime.datetime(2023, 6, day_count)

print(get_working_hours(start_date, end_date))

done()

print('21. Determine the date of the next meeting based on a recurring schedule.\n')

last_meeting = datetime.datetime(2023, 2, 8)
meeting_interval = datetime.timedelta(days=14)

def next_recurrence(last: datetime.datetime, interval: datetime.timedelta) -> datetime.datetime:
    return last + interval

print(next_recurrence(last_meeting, meeting_interval))

done()

# print('22. Extract the date and time information from a log file.\n')

# # Can be a project for later.

# done()

# print('23. Generate a report that shows the distribution of events over time.\n')

# # Can be a project for later.

# done()

# print('24. Determine the date of the next payment based on a payment schedule.\n')

last_payment = datetime.datetime(2023, 2, 8)
payment_interval = datetime.timedelta(days=total_days(last_payment.year, last_payment.month))

def next_meeting(last: datetime.datetime, interval: datetime.timedelta) -> datetime.datetime:
    return last + interval

print(next_recurrence(last_payment, payment_interval))

done()

print('25. Calculate the elapsed time between two events.\n')

event1 = datetime.datetime(2023, 2, 8, 13, 40)
event2 = datetime.datetime(2023, 2, 8, 15, 00)

print(event2 - event1)

done()
