'''array_arr_exercises: Exercises covering arrays'''
from array import array

def main():
    # Create an array of integers [1, 2, 3, 4, 5, 6, 7] using the
    # array module and assign it to a variable arr.
    arr = array('i', [1, 2, 3, 4, 5, 6, 7])
    print("Array 'arr':", [x for x in arr])

    # Print the type of the array using the type() function.
    print("Array Type:", type(arr))

    # Print the length of the array using the len() function.
    print("Array length:", len(arr))

    # Access the second element of the array using indexing and print
    # it.
    print("Second Element:", arr[1])

    # Modify the third element of the array to be 10 using indexing
    # and print the modified array.
    arr[2] = 10
    print("New array after changing 3rd element:", arr)

    # Use the append() method to add the number 8 to the array and
    # print the modified array.
    arr.append(8)
    print("New array after appending '8':", arr)

    # Use the insert() method to insert the number 9 at the index 2 in
    # the array and print the modified array.
    arr.insert(2, 9)
    print("New array after inserting '9' at index 2:", arr)

    # Use the pop() method to remove the last element of the array and
    # print the element and the modified array.
    arr.pop()
    print("New array after using pop():", arr)    

    # Use the remove() method to remove the number 4 from the array
    # and print the modified array.
    arr.remove(4)
    print("New array after removing '4':", arr)

    # Use slicing to access a subarray of the original array from
    # index 1 to 4 and print it.
    print("1-4 Slice of array:", arr[1:5])

    # Use the extend() method to add multiple elements [11,12,13,14]
    # to the array and print the modified array.
    arr.extend([11, 12, 13, 14])
    print("Extended array:", arr)

    # Use the index() method to find the index of the number 6 in the
    # array and print the result.
    print("Index of '6' in the array:", arr.index(6))

    # Use the count() method to count the number of occurrences of the
    # number 5 in the array and print the result.
    print("Number of occurrences of '5' in the array:", arr.count(5))

    # Use the reverse() method to reverse the order of elements in the
    # array and print the modified array.
    arr.reverse()
    print("Reversed array:", arr)

    # Use the sorted() function to sort the elements in the array in
    # ascending order, saving it to arr, and print the modified array.
    arr = array("i", sorted(arr))
    print("Sorted array:", arr)

    # Use the tolist() method to convert the array to a list and print
    # the result.
    arrlist = array.tolist(arr)
    print("Array converted to list: ", arrlist)

    # Use the fromlist() method to convert a list [15,16,17,18,19] to
    # an array and print the result.
    arr2 = array('i')
    arr2.fromlist([15, 16, 17, 18, 19])
    print("List converted to array 2:", arr2)

    # Use the byteswap() method to change the byte order of the array
    # and print the modified array.
    arr2.byteswap()
    print("Byte-swapped array 2:", arr2)

    # Use the buffer_info() method to get information about the
    # array's memory buffer and print the length of the buffer and the
    # address of the buffer.
    print("Buffer info for array (address, length):", arr.buffer_info())

    # Use the count() method to count the number of occurrences of the
    # number 2 in the array and print the result.
    print("Occurrences of the number 2 in the array:", arr.count(2))

main()
