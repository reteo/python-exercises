'''boolean_x_and_y_exercises: Exercises covering boolean data structures.'''
import random

# Define functions

def OR(x: bool, y: bool):
    return x or y

def AND(x: bool, y: bool):
    return x and y

def NOT(x: bool):
    return not x

def NOR(x: bool, y: bool):
    return not (x or y)

def NAND(x: bool, y: bool):
    return not (x and y)

def XOR(x: bool, y: bool):
    return (x or y) and (not (x and y))

def XNOR(x: bool, y: bool):
    return (not ((x or y)) and (not (x and y)))

def n_over_5(n: int, b: bool):
    return True if ((n > 5) and b) else False

def m_under_10(m: int, b: bool):
    return True if ((m < 10) or b) else False

def p_equals_0(p: int, b: bool):
    return True if ((p == 0) and (b == False)) else False

# Main function

def main():
    # Assign the boolean value True to a variable x and the boolean
    # value False to a variable y.
    x = True
    y = False

    # Well be using additional variables later.  Let's generate them
    # randomly now.
    

    ### From here, we'll use the following variables.
    # bool_list = [False, False, False, False, False]
    bool_list = list(random.choice([True, False]) for i in range(5))
    a, b = tuple(random.choice([True, False]) for i in range(2))
    c = True

    print("Bool List:", bool_list)
    print("a:", a)
    print("b:", b)
    print("c:", c)
    print("x:", x)
    print("y:", y)

    # Use the and operator to check if both x and y are True and print
    # the result.
    print("AND check (x, y):", x and y)

    # Use the or operator to check if either x or y is True and print
    # the result.
    print("OR Check (x, y):", x or y)

    # Use the not operator to check if x is not True and print the
    # result.
    print("NOT Check (x):", not x)

    # Define a function that takes two boolean arguments, and returns
    # True if both arguments are True, and False otherwise. Call the
    # function and print the result.
    print("AND Function (x, y):", AND(x, y))

    # Define a function that takes two boolean arguments, and returns
    # True if either argument is True, and False otherwise. Call the
    # function and print the result.
    print("OR Function (x, y):", OR(x, y))

    # Define a function that takes a boolean argument, and returns the
    # opposite value of the argument. Call the function with x as the
    # argument and print the result.
    print("NOT Function (x):", NOT(x))

    # Define a function that takes two boolean arguments, and returns
    # True if only one of the arguments is True, and False
    # otherwise. Call the function and print the result.
    print("XOR Function (x, y):", XOR(x, y))

    # Use the is operator to check if a variable y is of boolean type
    # and print the result.

    print ("'y' is Boolean:", type(y) is bool)

    # Use the == operator to check if x is equal to True and print the
    # result.
    print("'x' is True:", x == True)    

    # Use the != operator to check if x is not equal to y and print
    # the result.
    print("'x' is NOT y:", x != y)

    # Use the in operator to check if True is in bool_list and print
    # the result.
    print("Bool List has True:", "Yes" if True in bool_list else "No")

    # Use the if-else statement to check the truth value of a boolean
    # variable b and print a message accordingly.
    if b == True:
        print("'b' is True.")
    else:
        print("'b' is False.")

    # Use the while loop to run a block of code as long as a boolean
    # variable c is True.

    count = 0
    while c == True:
        count += 1
        c = random.choice([True, False])
    print("'c' was True", count, "time" if count == 1 else "times")

    # Define a function that takes in a variable `n`and a boolean
    # argument, and returns True if n is greater than 5 and the
    # boolean argument is True, and False otherwise. Call the function
    # and print the result.
    n = random.randint(0, 10)
    print("'n' is", n)
    print("'n' is greater than 5 and 'b' is true:", n_over_5(n, b))

    # Define a function that takes in a variable m and a boolean
    # argument, and returns True if m is less than 10 or the boolean
    # argument is True, and False otherwise. Call the function and
    # print the result.
    m = random.randint(0, 20)
    print("'m' is", m)
    print("'m' is less than 10 or 'a' is true:", m_under_10(m, a))

    # Define a function that takes in a variable p and a boolean
    # argument, and returns True if p is equal to 0 and the boolean
    # argument is False, and False otherwise. Call the function and
    # print the result.
    p = random.randint(-1, 1)
    print("'p' is", p)
    print("'p' is zero and 'b' is false:", p_equals_0(p, b))

    # Use the and operator to check if a variable q is greater than -5
    # and less than 5 and print the result.
    q = random.randint(-15, 15)
    print("'q' is", q)
    print("'q' is greater than -5 and less than 5:", ((q < 5) and (q > -5)))

    # Use the or operator to check if a variable r is equal to 0 or
    # greater than 10 and print the result.
    r = random.randint(0, 20)
    print("'r' is", r)
    print("'r' is equal to zero or greater than 10:", ((r == 0) or (r > 10)))

    # Use the not operator to check if a variable s is not equal to
    # "True" and print the result.
    s = random.choice([True, False])
    print("'s' is", s)
    print("'s' is not True:", not s)

    # Use a for loop to check if any of the variables in the bool_list
    # are True and print the result.
    bools_found = False
    for item in bool_list:
        if item == True:
            bools_found = True
    print("'bool_list' has 'True' variables." if bools_found == True else "bool_list is all 'False'")
        
main()
