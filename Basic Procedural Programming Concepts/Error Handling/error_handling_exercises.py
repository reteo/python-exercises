'''error_handling_exercises.py: A series of exercises covering the handling of exceptions.'''

def done():
    """Creates a line of dashes indicating the end of an exercise in output."""
    print('\n---\n')

# print('1. Write a try-except block to handle a ValueError exception.')

# try:
#     num = int(input("Enter a number: "))
# except ValueError:
#     print("Please enter a number.")
# else:
#     print(f"The number is {num}.")

# done()


print('2. Write a try-except block to handle an IndexError exception.')

try: 
    my_list = [1, 2, 3]
    print(my_list[3])
except IndexError:
    print("Unfortunately, the number is outside the range of the list.")

done()


print('3. Write a try-except block to handle an AttributeError exception.')

class MyClass:
    pass

try:
    obj = MyClass()
    obj.nonexistent_method()
except AttributeError:
    print("I'm sorry, that attribute is not available in the class.")

done()


print('4. Write a try-except block to handle a KeyError exception.')

my_dict = {"a": 1, "b": 2}

try:
    print(my_dict["c"])
except KeyError:
    print("Unfortunately, that key is not currently available.")

done()


print('5. Write a try-except block to handle a TypeError exception.')

num = 5
text = "hello"

try:
    print(num + text)
except TypeError as e:
    print("An error has occurred: ", e)

done()


print('6. Write a try-except block to handle a FileNotFoundError exception.')
try:
    with open("nonexistent_file.txt", "r") as f:
        print(f.read())
except FileNotFoundError:
    print("File was not found.")

done()


# print('7. Write a try-except block to handle an EOFError exception.')

# try:
#     input("Enter some text: ")
# except EOFError:
#     print("Something happened while collecting text.")

# done()


# print('8. Write a try-except block to handle a KeyboardInterrupt exception.')

# try:
#     while True:
#         continue
# except KeyboardInterrupt:
#     print("The loop was interrupted.")
    
# done()


print('9. Write a try-except block to handle a MemoryError exception.')

try:
    big_list = [0] * (10 ** 9) * (10 ** 9)
except MemoryError:
    print("Not enough memory.  Sorry!")

done()


print('10. Write a try-except block to handle a NotImplementedError exception.')

class MyClass:
    def my_method(self):
        raise NotImplementedError("This method has not been implemented yet")

my_object = MyClass()

try:
    my_object.my_method()
except NotImplementedError as e:
    print(e)
    
done()


print('11. Write a try-except block to handle a RecursionError exception.')

def my_func(num):
    return my_func(num + 1)

try:
    my_func(0)
except RecursionError:
    print("Unfortunately, the function stack was insufficient for the task.")

done()


print('12. Write a try-except block to handle a SyntaxError exception.')

try:
    eval("print(1")
except SyntaxError:
    print("Ya did something stupid, ya numpty!")

done()


# print('13. Write a try-except block to handle a SystemError exception.')
# Not testable! ###

# import sys

# try:
#     print(sys.getsizeof(2 ** 10000))
# except SystemError as e:
#     print(e)

# done()


# print('14. Write a try-except block to handle a TabError exception.')
# Cannot be caught! ###

# def my_func():
#     try:
#         print("Indented line")
#       print("Not indented line")
#     except TabError:
#         print("This code was incorrectly indented.")

# done()


print('15. Write a try-except block to handle a TimeoutError exception.')

import asyncio

async def my_coroutine():
    await asyncio.sleep(2) # Simulate a long-running operation
    return "Done!"

async def timeout_exercise():
    try:
        result = await asyncio.wait_for(my_coroutine(), timeout=1)
    except asyncio.TimeoutError:
        print("Took too long.")

asyncio.run(timeout_exercise())

done()


print('16. Write a try-except block to handle an UnboundLocalError exception.')

def my_func():
    try:
        print(num)
        num = 5
    except UnboundLocalError:
        print("The cart should not go before the horse.")
    
my_func()


done()


print('17. Write a try-except block to handle a UnicodeError exception.')

try:
    bytes_obj = b"\xe9"
    string_obj = bytes_obj.decode("ascii")
except UnicodeError as e:
    print(e)

done()


print('18. Write a try-except block to handle a ZeroDivisionError exception.')

try:
    num = 1 / 0
except:
    print("Divide by zero is a no-no.")

done()


# print('19. Write a try-except block to handle multiple exceptions in a single block.')

# # The following has the potential for an index error and a value
# # error.

# try:
#     num = int(input("Enter a number: "))
#     my_list = [1, 2, 3]
#     print(my_list[num])
# except IndexError:
#     print("The number is outside the range.")
# except ValueError:
#     print("Please enter a number.")
    
# done()


print('20. Write a try-finally block to ensure a certain block of code is executed.')

file = None
try:
    file = open("myfile.txt", "r")
    data = file.read()
    print(data)
except IOError:
    print("The file does not exist.")
    
# In this case, we want to close the file after this code is executed.

finally:
    if file is not None:
        file.close()
    print("Finished.")

done()


# print('21. Write a try-except-else block to execute certain code when no exceptions are raised.')

# try:
#     num = int(input("Enter a number: "))
# except ValueError:
#     print("Please enter a number.")

# # In this case, we want the code to print the square of the input
# # number:

# else:
#     print(f"The square of {num} is {num ** 2}")

# done()


# print('22. Write a try-except-else-finally block to execute certain code when no exceptions are raised, and to ensure a certain block of code is executed.')

# try:
#     num = int(input("Enter a number: "))
# except ValueError:
#     print("Please enter a number.")

# # In this case, we want the code to print the square of the input
# # number:

# else:
#     print(f"The square of {num} is {num ** 2}")

# # And, when it's all done, the following must be run, exception or no.
# finally:
#     print("Execution complete")

# done()


print('23. Write a custom exception class to raise a custom exception.')

class NegativeNumberException(Exception):
    pass

def my_func(num):
    if num < 0:
        raise NegativeNumberException("The number needs to be non-negative.")

try:
    my_func(-1)
except NegativeNumberException as e:
    print(e)

done()


print('24. Write a function that raises an exception if an input is not valid.')

class IntegerException(ValueError):
    pass

def check_input(num):
    if not isinstance(num, int):
        raise IntegerException("The number must be an integer.")
    if num < 0:
        raise NegativeNumberException("The number must be non-negative.")

try:
    check_input(-1)
except IntegerException as e:
    print(e)
except NegativeNumberException as e:
    print(e)

done()


print('25. Write a function that catches and handles exceptions that occur within it.')

# Potential for value error or index error.

def my_func():
    num = int(input("Enter a number: "))
    my_list = [1, 2, 3]
    print(my_list[num])

try:
    my_func()
except ValueError:
    print("Please enter a number.")
except IndexError:
    print("The number is out of range.")

done()

