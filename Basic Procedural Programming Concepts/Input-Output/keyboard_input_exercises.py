# """keyboard_input_exercises.py: Exercises focusing on keyboard input"""

import datetime
import string
import math
import re

def done():
    """Prints a marker indicating that the exercise is complete."""
    print("\n---\n")

# print("1. Ask the user for their name and greet them using the input() function.\n")

# name = input("Enter your name: ")

# print(f"Hello, {name}!")

# done()

# print(
#     "2. Ask the user for two numbers and calculate their sum using the input() function.\n"
# )

# number_string_list = input("Enter two numbers, separated by a space: ").split(" ")

# numbers = [int(number) for number in number_string_list]

# print(sum(numbers))

# done()

# print(
#     "3. Ask the user for a sentence and count the number of words in it using the input() function.\n"
# )

# sentence_list = input("Enter a sentence: ").split(" ")

# print(len(sentence_list))

# done()

# print(
#     "4. Ask the user for a number and determine if it is even or odd using the input() function.\n"
# )

# number = int(input("Enter a number: "))

# print(number % 2 == 0)

# done()

# print(
#     "5. Ask the user for their favorite color and print it using the input() function.\n"
# )

# print(f"Your favorite color is {input('What is your favorite color? ')}.")

# done()

# print(
#     "6. Ask the user for three numbers and find the largest number using the input() function.\n"
# )

# print(max([int(number) for number in input("Enter three numbers separated by spaces: ").split(" ")]))

# done()

# print("7. Ask the user for a sentence and reverse it using the input() function.\n")

# print(
#     "".join(
#         [letter for letter in input("Enter a sentence: ")
#          if letter not in string.punctuation])
#     .lower()
#     .split(" ")[::-1]
# )

# print("\nAlternatively,\n")

# print("".join([letter for letter in input("Enter a sentence: ").lower()[::-1]
#        if letter not in string.punctuation]))

# done()

# print(
#     "8. Ask the user for a number and calculate its factorial using the input() function.\n"
# )

# print(math.factorial(int(input("Enter a number to generate a factorial: "))))

# done()

# print(
#     "9. Ask the user for two numbers and calculate the quotient and remainder when one is divided by the other using the input() function.\n"
# )

# numerator = int(input("Enter the numerator: "))
# denominator = int(input("Enter the denominator: "))

# quotient = numerator//denominator
# remainder = numerator%denominator

# print(f"{numerator} ÷ {denominator} = {quotient} R {remainder}")

# done()

# print(
#     "10. Ask the user for a sentence and replace all occurrences of a specific word with another word using the input() function.\n"
# )

# sentence = input("Enter a sentence: ")
# word_to_replace = input("Which word in the previous sentence should we replace? ")
# replacement = input("What word should we replace it with? ")

# print (f"Original: {sentence}\nUpdated: {re.sub(word_to_replace, replacement, sentence)}")

# done()

# print(
#     "11. Ask the user for a string and print it in uppercase using the input() function.\n"
# )

# print(input("Enter a sentence: ").upper())

# done()

# print(
#     "12. Ask the user for a string and print it in lowercase using the input() function.\n"
# )

# print(input("Enter a sentence: ").lower())

# done()

# print(
#     "13. Ask the user for a number and determine if it is prime using the input() function.\n"
# )

# def is_prime(n):
#     if n % 2 == 0 and n > 2: 
#         return False
#     return all(n % i for i in range(3, int(math.sqrt(n)) + 1, 2))

# print(is_prime(int(input("Enter a number: "))))

# done()

# print(
#     "14. Ask the user for their birth year and calculate their age using the input() function.\n"
# )

# birth_date_entry = input("Enter your birth date (yyyy/mm/dd): ")
# birth_date = datetime.datetime.strptime(birth_date_entry, "%Y/%m/%d")

# age = datetime.datetime.now() - birth_date
# age_years = int(age.days//365.25)
# age_days = int(age.days%365.25)
# next_birthday = int(365.25-age_days)

# print(f"Your age is {age_years} years and {age_days} days.  You have {next_birthday} days before your next birthday.")

# done()

# print(
#     "15. Ask the user for a sentence and print the first letter of each word using the input() function.\n"
# )

# sentence = input("Enter a sentence: ")

# sentence_list = sentence.lower().split(" ")

# print("".join([word[0] for word in sentence_list]))

# done()
