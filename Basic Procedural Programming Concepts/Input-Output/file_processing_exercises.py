"""file_processing_exercises.py: File processing exercises"""
import random
import json
import statistics
import string

def done():
    """Prints a line indicating the end of an assignment."""
    print("\n---\n")


print("1. Create a file and write your name to it using the write() method.\n")

filename = "files/file1.txt"

with open(filename, "w") as fopen:
    fopen.write("This is the first exercise.")

done()


print("2. Read the contents of a file and print it using the read() method.\n")

with open(filename, "r") as fopen:
    for line in fopen:
        print(line)

done()


print("3. Create a file and write a list of numbers to it using the write() method.\n")

filename = "files/file2.txt"
list_of_numbers = [str(random.randint(0, 9)) for _ in range(4, random.randint(5, 20))]

with open(filename, "w") as fopen:
    fopen.write(", ".join(list_of_numbers))

done()


print(
    "4. Read the contents of a file, convert the numbers to a list, and find the sum using the read() method.\n"
)

with open(filename, "r") as fopen:
    string = fopen.read()

list_of_numbers = [int(number) for number in string.split(", ")]

print(sum(list_of_numbers))

done()

print("5. Create a file and write a sentence to it using the write() method.\n")

filename = "files/file3.txt"
sentence = "This is a sentence.  It's not the only sentence, but it is mine."

with open(filename, "w") as fopen:
    fopen.write(sentence)

done()


print(
    "6. Read the contents of a file, find the length of the sentence, and print it using the read() method.\n"
)

with open(filename, "r") as fopen:
    file_sentence = fopen.read()
    print(file_sentence)
    print(len(file_sentence))

done()


# print("7. Create a file and write a string to it using the write() method.\n")

# # This is getting repetitive.

# done()


print(
    "8. Read the contents of a file, reverse the string, and print it using the read() method.\n"
)

with open(filename, "r") as fopen:
    file_string = fopen.read()
    print(file_string[::-1])

done()


# print(
#     "9. Create a file and write a string of characters to it using the write() method.\n"
# )

# Skipping due to its repeating nature.

# done()


print(
    "10. Read the contents of a file, find the frequency of each character, and print it using the read() method.\n"
)

ex_10_histogram = {}

with open(filename, "r") as fopen:
    file_string = fopen.read()
    for character in file_string:
        ex_10_histogram[character.lower()] = ex_10_histogram.get(character, 0) + 1

print(ex_10_histogram)
done()


print("11. Create a file and write a list of words to it using the write() method.\n")

filename = "files/file4.txt"
wordlist = [
    "powerful",
    "pot",
    "nation",
    "test",
    "interfere",
    "cap",
    "purpose",
    "company",
    "oceanic",
    "gun",
    "sniff",
    "apparel",
    "tub",
    "push",
    "round",
]

current_wordlist = [random.choice(wordlist) for _ in range(0, random.randint(4, 10))]

with open(filename, "w") as fopen:
    word_string = ', '.join(current_wordlist)
    fopen.write(word_string)

done()


print(
    "12. Read the contents of a file, convert the words to a list, and find the length of the longest word using the read() method.\n"
)

with open(filename, "r") as fopen:
    string = fopen.read()
    words = string.split(", ")
    print(sorted(words, key=lambda word: len(word), reverse=True)[0])

done()


print(
    "13. Create a file and write a list of sentences to it using the write() method.\n"
)

sentences = [
    "The quick brown fox jumped over the lazy dog.",
    "She sells sea shells by the sea shore.",
    "Peter piper picked a peck of pickled peppers.",
    "Now is the time, for all good men, to come to the aid of the party."
]

filename = "files/file5.txt"
sentence_string = "\n".join(sentences)

with open(filename, "w") as fopen:
    fopen.write(sentence_string)

done()


print(
    "14. Read the contents of a file, convert the sentences to a list, and find the number of sentences using the read() method.\n"
)

sentence_string = ""
new_sentences = []

with open (filename, "r") as fopen:
    sentence_string = fopen.read()

new_sentences = sentence_string.split("\n")

print(new_sentences)

done()

print(
    "15. Create a file and write a string of numbers to it using the write() method.\n"
)

filename = "files/file6.txt"
number_list = [random.randint(0, 10) for _ in range(0, random.randint(5, 10))]
number_string_list = [str(number) for number in number_list]

number_string = ", ".join(number_string_list)

# for number in number_list:
#     number_string += str(number) + ","

# number_string = number_string[:-1] # There will always be a trailing
#                                    # comma.  Remove it.

with open(filename, "w") as fopen:
    fopen.write(number_string)

done()


print(
    "16. Read the contents of a file, convert the numbers to a list, and find the average using the read() method.\n"
)

new_number_list = []

with open(filename) as fopen:
    new_number_string = fopen.read()

new_number_string_list = new_number_string.split(",")

for number in new_number_string_list:
    new_number_list.append(int(number))

print(round(sum((new_number_list))/len(new_number_list),2))

done()


print("17. Create a file and write a list of numbers to it using the write() method.\n")

filename = "files/file7.txt"
number_list = [random.randint(0, 10) for _ in range(0, random.randint(5, 10))]
number_string_list = [str(number) for number in number_list]
number_string = ", ".join(number_string_list)

with open(filename, "w") as fopen:
    fopen.write(number_string)

done()


print(
    "18. Read the contents of a file, convert the numbers to a list, and find the median using the read() method.\n"
)

new_number_list = []
new_number_string = ""

with open(filename, "r") as fopen:
    new_number_string = fopen.read()

new_number_string_list = new_number_string.split(", ")
new_number_list = [int(number) for number in new_number_string_list]

print(statistics.median(new_number_list))

done()


print(
    "19. Create a file and write a string of characters to it using the write() method.\n"
)

filename = "files/file8.txt"
char_list = [random.choice("abcdefghijklmnopqrstuvwxyz1234567890")
             for _ in range(0, random.randint(4, 10))]
char_string = "".join(char_list)

with open(filename, "w") as fopen:
    fopen.write(char_string)

done()


print(
    "20. Read the contents of a file, find the first and last character, and print it using the read() method.\n"
)

new_char_string = ""

with open(filename, "r") as fopen:
    new_char_string = fopen.read()
    
print(new_char_string[0] + ", " + new_char_string[-1])

done()


# print("21. Create a file and write a list of words to it using the write() method.\n")

# done()


print(
    "22. Read the contents of a file, convert the words to a list, and find the first and last word using the read() method.\n"
)

filename = "files/file4.txt"
new_word_list = []

with open(filename, "r") as fopen:
    string = fopen.read()
    new_word_list = string.split(", ")

print(new_word_list[0] + ", " + new_word_list[-1])

done()


# print(
#     "23. Create a file and write a list of sentences to it using the write() method.\n"
# )

# done()


print(
    "24. Read the contents of a file, convert the sentences to a list, and find the first and last sentence using the read() method.\n"
)

filename = "files/file5.txt"
new_sentences = []
new_sentences_string = ""

with open(filename, "r") as fopen:
    new_sentences_string = fopen.read()

new_sentences = new_sentences_string.split("\n")

print("Debugging:", new_sentences)
print(new_sentences[0] + "\n" + new_sentences[-1])

done()
