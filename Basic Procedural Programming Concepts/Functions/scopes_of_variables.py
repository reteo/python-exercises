"""scopes_of_variables: Exercises covering the scope of variables in functions."""

# pylint: disable = C0301, C0116, C0103, W0621

from random import randint

def done() -> None:
    print("\n---\n")

print('1. Demonstrate the difference between local and global variables')

value_global = 5

def exercise_1() -> None:
    print(f"Value (Global): {value_global}")
    value_local = 10
    print(f"Value (Local): {value_local}")

exercise_1()
done()

print('2. Create a variable with the same name in a local and global scope')
print('3. Access a global variable from within a local scope')
print('4. Modify a global variable from within a local scope\n')

value = 5

def exercise_2_3_4() -> None:
    value = 10

    print(f"Local Value: {value}\nGlobal Value: {globals()['value']}")

    globals()['value'] = 15
    value = 20

    print(f"Local Value: {value}\nGlobal Value: {globals()['value']}")

exercise_1()
done()

print('5. Use the "nonlocal" keyword to modify a variable in an enclosing scope\n')

def exercise_5() -> str:

    name = "Mike"

    def exercise_5b() -> str:
        nonlocal name
        name = "Jack"

    exercise_5b()
    return name

print(f"Hello, {exercise_5()}")
done()

print('6. Show the effect of shadowing a global variable with a local variable\n')

value = 5

def exercise_6() -> None:

    value = 10
    print(f"Local value: {value}")

exercise_6()
print(f"Global value: {value}")

done()

print('7. Use the "globals()" and "locals()" functions to inspect variables in the current scope\n')

value = 10

def exercise_7():
    value = 20

    print(f"Global: {globals()['value']}")
    print(f"Local: {locals()['value']}")

exercise_7()

done()

print('8. Create a nested function and demonstrate variable scope within the nested function')
print('11. Create a variable with the same name in multiple nested scopes\n')


value = 5

def exercise_8():
    value = 10

    def inner():
        value = 15

        print("Local:", value)
        
    inner()
    print("Enclosed:", value)

exercise_8()
print("Global:", value)

done()

print('9. Use a global variable within a nested function\n')

value = 5

def exercise_9():

    def inner():

        print("Global:", value)
        
    inner()

exercise_9()

done()

print('10. Modify a variable in an enclosing scope using the "nonlocal" keyword\n')

def exercise_10():

    value = 10

    def inner():
        nonlocal value
        value = 20

    inner()
    print(value)

exercise_10()

done()


print('12. Use the "global" keyword to declare a variable as global within a function\n')

value = 5
print(value)

def exercise_12():
    global value
    value = 10

exercise_12()
print(value)

done()

print('13. Demonstrate how variable scope is affected by function arguments\n')

value = 5
number = randint(1, 10)

def exercise_13(number):
    global value
    value += number
    return value

number = exercise_13(number)
print(number)

done()


print('14. Pass a global variable as an argument to a function and modify it within the function\n')

value = [1, 4, 5]

def exercise_14(number):
    value.append(3)
    value.append(2)
    value.sort()

print(value)
exercise_14(value)
print(value)

done()

print('15. Use the "del" keyword within a function to delete a global variable\n')

value = 10

def exercise_15():
    global value
    del value

exercise_15()
print("deleted" if 'value' not in globals() else f"Value: {value}")

done()
    

print('16. Create a function that returns a local variable and access it outside the function\n')

def exercise_16():
    return randint(0, 10)

value = exercise_16()

print(value)

done()

print('17. Create a function with default argument values and demonstrate how they are treated in terms of scope\n')

def exercise_17(person = "John", greetings = "Hello"):
    print(f"Local Variables: {locals()}")

exercise_17()

done()

print('18. Use the "global" keyword within a nested function to modify a global variable\n')

value = 5

def exercise_18():
    value = 10

    def inner():
        global value
        value = 20

    inner()

print(value)
exercise_18()
print(value)

done()

print('19. Use the "global" keyword to declare multiple variables as global within a function\n')

value = test = fun = 5

def exercise_19():
    global value
    global test
    global fun

    value = randint(1, 10)
    test = randint(1, 10)
    fun = randint(1, 10)

exercise_19()
print (f"Value: {value}, Test: {test}, Fun: {fun}")

done()

print('20. Demonstrate how the scope of variables is affected by loops')
print('25. Demonstrate how variable scope is affected by the use of "for" and "while" loops.\n')

def exercise_20():
    value = 0
    
    for count in range(0, 5):
        value += randint(1, 10)
    
    print(f"Value: {value}, Count:",
          f"{'not in scope' if 'count' not in locals().keys() else count}")

    while True:
        something = True
        break

    print(f"Break is {something}")

exercise_20()

done()

print("21. Create a closure and demonstrate how variables are maintained in the closure's scope\n")

def exercise_21(x):
    def project(y):
        print(f"Outer: {x}, Inner: {y}")
    return project

closure = exercise_21(15)
closure(10)

done()

print('22. Use the "global" keyword to declare a variable as global within a closure\n')

value = 5

def exercise_22():
    global value
    def project(y):
        print(f"Global: {value}, Inner: {y}")
    return project

closure = exercise_22()

closure(10)
value = 10
closure(10)

done()


print('23. Access a variable from the global scope within a closure\n')

value = 5

def exercise_23(x):
    def project(y):
        print(f"Outer: {x}, Inner: {y}, Global: {value}")
    return project

closure = exercise_23(15)
closure(10)

done()

print('24. Use the "nonlocal" keyword within a closure to modify a variable in an enclosing scope\n')

value = 5

def exercise_24(x):
    def project(y):
        nonlocal x
        x = x * x
        print(f"Outer: {x}, Inner: {y}, Global: {value}")
    return project

closure = exercise_24(15)
closure(10)

done()



