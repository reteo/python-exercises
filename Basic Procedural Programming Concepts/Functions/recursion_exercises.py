"""recursion_exercises: Exercises covering recursive functions"""

# Imports Here

from random import randint
import math

# Functions Here


def done() -> None:
    """Marks the end of an exercise, and the start of the next."""
    print("\n---\n")


def factorial(value: int) -> int:
    """1. Write a recursive function to calculate the factorial of a
    given number.

    """
    if value == 0:
        return 1
    return value * factorial(value - 1)


def fibonacci(value: int) -> int:
    """2. Write a recursive function to calculate the Fibonacci sequence
    denominator of two numbers.

    """
    return (
        1 if value == 0
        else 1 if value == 1
        else fibonacci(value - 1) + fibonacci(value - 2)
    )

def euclidian(value_1: int, value_2: int) -> int:
    """3. Write a recursive function to calculate the greatest common
    denominator of two numbers.

    """
    return (
        value_1 if value_2 == 0
        else euclidian(value_2, value_1 % value_2)
    )


def recursive_sum(in_number_list: list[int]) -> int:
    """5. Write a recursive function to find the sum of all elements
    in a given list.

    """
    number_list = in_number_list.copy()
    return (
        number_list.pop() if len(number_list) == 1
        else number_list.pop() + recursive_sum(number_list)
        )
    
    # if len(number_list) == 1:
    #     return number_list.pop()
    # return number_list.pop() + recursive_sum(number_list)


def recursive_product(in_number_list: list[int]) -> int:
    """6. Write a recursive function to find the product of all
    elements in a given list.

    """
    number_list = in_number_list.copy()
    return(
        number_list.pop() if len(number_list) == 1
        else number_list.pop() * recursive_product(number_list)
    )


def recursive_highest(number_list: list[int]) -> int:
    """7. Write a recursive function to find the highest value in a
    given list.

    """
    if len(number_list) == 1:
        return number_list[0]
    first = number_list[0]
    rest = recursive_highest(number_list[1:])
    return first if first > rest else rest


def recursive_lowest(number_list: list[int]) -> int:
    """8. Write a recursive function to find the lowest value in a
    given list.

    """
    if len(number_list) == 1:
        return number_list[0]
    first = number_list[0]
    rest = recursive_lowest(number_list[1:])
    return first if first < rest else rest


def recursive_average(number_list: list[int]) -> float:
    """9. Write a recursive function to find the average value of a
    given list.

    """
    if len(number_list) == 1:
        return number_list[0]
    first = number_list[0]
    length = len(number_list)
    rest = recursive_average(number_list[1:])
    return (first + (length - 1) * rest) / length


def recursive_mode(in_number_list: list[int], counts={}) -> int:
    number_list = in_number_list.copy()
    if len(number_list) == 0:
        count_list = sorted([(v, k) for k, v in counts.items()])[::-1]
        return count_list[0][1]
    counts[number_list[0]] = counts.get(number_list[0], 0) + 1
    return recursive_mode(number_list[1:], counts)


def recursive_range(in_number_list: list[int]) -> list[int]:
    number_list = in_number_list.copy()
    if len(number_list) == 1:
        return [number_list[0], number_list[0]]

    high = low = number_list[0]
    current_range = recursive_range(number_list[1:])
    if low > current_range[0]:
        low = current_range[0]
    if high < current_range[1]:
        high = current_range[1]

    return [low, high]


def recursive_even_add(in_number_list: list[int]) -> int:
    number_list = in_number_list.copy()
    
    if len(number_list) == 0:
        return 0

    if number_list[0] % 2 == 1:
        return recursive_even_add(number_list[1:])

    return number_list[0] + recursive_even_add(number_list[1:])


def recursive_odd_add(in_number_list: list[int]) -> int:
    number_list = in_number_list.copy()

    if len(number_list) == 0:
        return 0

    if number_list[0] % 2 == 1:
        return recursive_odd_add(number_list[1:])

    return number_list[0] + recursive_odd_add(number_list[1:])


def recursive_even_multiply(in_number_list: list[int]) -> int:
    number_list = in_number_list.copy()

    if len(number_list) == 0:
        return 1

    if number_list[0] % 2 == 1:
        return recursive_even_multiply(number_list[1:])

    return number_list[0] * recursive_even_multiply(number_list[1:])


def recursive_odd_multiply(in_number_list: list[int]) -> int:
    number_list = in_number_list.copy()

    if len(number_list) == 0:
        return 1

    if number_list[0] % 2 == 0:
        return recursive_odd_multiply(number_list[1:])

    return number_list[0] * recursive_odd_multiply(number_list[1:])


def recursive_count_even(in_number_list: list[int]) -> int:
    number_list = in_number_list.copy()

    if len(number_list) == 0:
        return 0

    return (
        0
        if number_list[0] % 2 == 1
        else 1) + recursive_count_even(number_list[1:])


def recursive_count_odd(in_number_list: list[int]) -> int:
    number_list = in_number_list.copy()

    if len(number_list) == 0:
        return 0

    return (
        0
        if number_list[0] % 2 == 0
        else 1
    ) + recursive_count_odd(number_list[1:])

# def is_prime(number: int) -> bool:
#     if -2 < number < 2:
#         return False
#     for factor in range(2, int(abs(math.sqrt(number)+1))):
#         if number % factor == 0:
#             return False
#     return True

def is_prime(number: int, factor=2) -> bool:
    # Base cases
    if number == factor:
        return True
    if -2 < number < 2:
        return False
    if number % factor == 0:
        return False
    if factor > round(abs(math.sqrt(number))):
        return True
    return is_prime(number, factor+1)

def recursive_count_prime(in_number_list: list[int]) -> int:
    number_list = in_number_list.copy()

    # Base case.
    if len(number_list) == 0:
        return 0
    return (
        1
        if is_prime(number_list[0])
        else 0
    ) + recursive_count_prime(number_list[1:])

def recursive_count_composite_numbers(in_number_list: list[int]) -> int:
    number_list = in_number_list.copy()
    # Base case.
    if len(number_list) == 0:
        return 0
    return (
        1
        if (number_list[0] != 1) and (not is_prime(number_list[0]))
        else 0
    ) + recursive_count_composite_numbers(number_list[1:])


# def recursive_find_even(in_number_list: list[int]) -> list[int]:
#     number_list = in_number_list.copy()

#     if len(number_list) == 0:
#         return []

#     return (
#         recursive_find_even(number_list[1:])
#         if number_list[0] % 2 == 1
#         else [number_list[0]] + recursive_find_even(number_list[1:])
#     )


# def recursive_find_odd(in_number_list: list[int]) -> list[int]:
#     number_list = in_number_list.copy()

#     if len(number_list) == 0:
#         return []

#     return (
#         recursive_find_odd(number_list[1:])
#         if number_list[0] % 2 == 0
#         else [number_list[0]] + recursive_find_odd(number_list[1:])
#     )

# Main Function Here


def main():

    """Main loop"""
    print(
        "1. Write a recursive function to calculate the factorial of a given number.\n",
    )

    number = randint(0, 10)
    print(f"Starting value: {number}")
    print(f"Factorial: {factorial(number)}")

    done()

    print(
        "2. Write a recursive function to calculate the Fibonacci sequence up to a given index.\n",
    )

    fibonacci_range = []
    for value in range(2, randint(3, 10)):
        fibonacci_range.append(fibonacci(value))

    print(f"Fibonacci Numbers to 20: {fibonacci_range}")

    done()

    print(
        "3. Write a recursive function to calculate the greatest common denominator of two numbers.\n",
    )

    number_1 = randint(0, 100)
    number_2 = randint(0, 100)

    print(
        f"The greatest common denominator between {number_1}",
        f"and {number_2} is {euclidian(number_1, number_2)}",
    )

    done()

    print(
        "4. Write a recursive function to find the nth element in the Fibonacci sequence.\n",
    )

    number = randint(2, 10)

    print(
        f"{number}{'nd' if number == 2 else 'rd' if number == 3 else 'th'} number in the Fibonacci Sequence: {fibonacci(number)}"
    )

    done()

    print(
        "5. Write a recursive function to find the sum of all elements in a given list.\n",
    )

    number_list = [randint(0, 10) for _ in range(0, randint(5, 10))]

    print(f"Sum of {number_list}: {recursive_sum(number_list)}")

    done()

    print(
        "6. Write a recursive function to find the product of all elements",
        "in a given list.\n",
    )

    number_list = [randint(1, 10) for _ in range(0, randint(5, 10))]

    print(f"Product of {number_list}: {recursive_product(number_list)}")

    done()

    print("7. Write a recursive function to find the highest value in a given list.\n")

    number_list = [randint(1, 10) for _ in range(0, randint(5, 10))]

    print(f"Highest in {number_list}: {recursive_highest(number_list)}")

    done()

    print("8. Write a recursive function to find the lowest value in a given list.\n")

    number_list = [randint(1, 10) for _ in range(0, randint(5, 10))]

    print(f"Lowest in {number_list}: {recursive_lowest(number_list)}")

    done()

    print("9. Write a recursive function to find the average value of a given list.\n")

    number_list = [randint(1, 10) for _ in range(0, randint(5, 10))]

    print(f"Average of {number_list}: {round(recursive_average(number_list),2)}")

    done()

    # print(
    #     "10. Write a recursive function to find the median value of a given list.\n"
    # )

    # number_list = [randint(0,10) for x in range(0, randint(4, 10))]

    # print(f"The median in the list {number_list} is {recursive_median(number_list)}.")
    # print(f"The median in the list {number_list} is {statistics.median(number_list)}.")

    # done()

    # I can't seem to come up with an accurate way to do so.

    print("11. Write a recursive function to find the mode value of a given list.\n")

    number_list = [randint(1, 10) for _ in range(0, randint(5, 10))]

    print(f"Mode of {number_list}: {recursive_mode(number_list)}")

    done()

    print(
        "12. Write a recursive function to find the range of values in a given list.\n"
    )

    number_list = [randint(1, 10) for _ in range(0, randint(5, 10))]
    number_range = recursive_range(number_list)

    print(f"Range of {number_list}: {number_range[0]}–{number_range[1]}")

    done()

    # print(
    #     "13. Write a recursive function to find the variance of values in a given list.\n"
    # )

    # print(
    #     "14. Write a recursive function to find the standard deviation of values in a given list.\n"
    # )

    print(
        "15. Write a recursive function to find the sum of all even numbers in a given list.\n"
    )

    number_list = [randint(1, 10) for _ in range(0, randint(5, 10))]

    print(
        f"Adding all even numbers of {number_list}: {recursive_even_add(number_list)}"
    )

    done()

    print(
        "16. Write a recursive function to find the sum of all odd numbers in a given list.\n"
    )

    number_list = [randint(1, 10) for _ in range(0, randint(5, 10))]

    print(f"Adding all odd numbers of {number_list}: {recursive_odd_add(number_list)}")

    done()

    print(
        "17. Write a recursive function to find the product of all even numbers in a given list.\n"
    )

    number_list = [randint(1, 10) for _ in range(1, randint(5, 10))]

    print(
        f"Multiplying all even numbers of {number_list}: {recursive_even_multiply(number_list)}"
    )

    done()

    print(
        "18. Write a recursive function to find the product of all odd numbers in a given list.\n"
    )

    number_list = [randint(1, 10) for _ in range(1, randint(5, 10))]

    print(
        f"Multiplying all odd numbers of {number_list}: {recursive_odd_multiply(number_list)}"
    )

    done()

    print(
        "19. Write a recursive function to find the number of even numbers in a given list.\n"
    )

    number_list = [randint(1, 10) for _ in range(1, randint(5, 10))]

    print(
        f"Counting all even numbers of {number_list}: {recursive_count_even(number_list)}"
    )

    done()

    print(
        "20. Write a recursive function to find the number of odd numbers in a given list.\n"
    )

    number_list = [randint(1, 10) for _ in range(1, randint(5, 10))]

    print(
        f"Counting all odd numbers of {number_list}: {recursive_count_odd(number_list)}"
    )

    done()

    print(
        "21. Write a recursive function to find the number of prime numbers in a given list.\n"
    )

    number_list = [randint(1, 20) for _ in range(1, randint(5, 10))]

    
    print(
        f"Counting all prime numbers of {number_list}: {recursive_count_prime(number_list)}"
    )

    done()

    print(
        "22. Write a recursive function to find the number of composite numbers in a given list.\n"
    )

    number_list = [randint(1, 20) for _ in range(1, randint(5, 10))]

    
    print(
        f"Counting all composite numbers of {number_list}: {recursive_count_composite_numbers(number_list)}"
    )

    done()

    print(
        "23. Write a recursive function to determine if a given number is prime.\n"
    )

    primes = [2, 3, 5, 7, 11, 13, 17]
    non_primes = [1, 4, 6, 8, 9, 10, 12, 14, 15]

    print("Primes:", [is_prime(x) for x in primes])
    print("Non-Primes:", [is_prime(x) for x in non_primes])

    # print(
    #     "24. Write a recursive function to calculate the sum of the digits of a given number.\n"
    # )

    # print(
    #     "25. Write a recursive function to determine if a given string is a palindrome.\n"
    # )


main()
