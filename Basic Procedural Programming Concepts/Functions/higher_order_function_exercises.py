"""higher_order_function_exercises: Exercises covering higher-order functions."""

from functools import reduce
from random import choice, randint
from itertools import groupby

number_list = [1, 5, 2, 4, 3]
wordlist = ["the", "quick", "brown", "fox", "jumped", "over", "the", "lazy", "dog", "trick", "mathematics", "leave", "deputy", "object", "cruelty", "vegetarian", "aspect", "abolish", "tumour"]
random_letter = choice("abcdefghijklmnopqrstuvwxyz")
random_number = randint(0, 10)
random_word = choice(wordlist)
contact_list = [
    {
        "name": "Michael Davis",
        "phone": "098-765-4321",
        "email": "mdavis@randome-name.net",
        "grade": "F",
        "standing": "suspended"
    },
    {
        "name": "John Doe",
        "phone": "000-111-2222",
        "email": "jdoe@szyzynedd.com",
        "grade": "A"
    },
    {
        "name": "Jack Bauer",
        "phone": "123-456-7890",
        "email": "jbauer@twentyfour.com",
        "standing": "pending review"
    }
]
tld_list = [".com", ".edu", ".org", ".net"]
key_list = ["grade", "email", "standing"]
random_tld = choice(tld_list)
cities = [
    {
        "name": "New York",
        "latitude": "North",
        "longitude": "East",
        "population": 8336817
    },
    {
        "name": "Los Angeles",
        "latitude": "South",
        "longitude": "West",
        "population": 3979576
    },
    {
        "name": "Miami",
        "latitude": "South",
        "longitude": "East",
        "population": 467963
    },
    {
        "name": "Seattle",
        "latitude": "North",
        "longitude": "West",
        "population": 753675
    },
]
random_number_list = [randint(0, 100) for _ in range(0, randint(4, 10))]

def done():
    print("\n---\n")

print(
    "1. Create a function that takes a list of numbers and returns a list of their squares.         \n"
)

def square_list(number_list_in: list[int]) -> list[int]:
    return list(map(lambda number: number * number, number_list_in))

print(square_list(number_list))

done()

print(
    "2. Create a function that takes a list of numbers and returns the sum of their squares.\n"
)

def higher_sum(number_list_in: list[int]) -> int:
    return int(reduce(lambda accumulator, number: accumulator + number, number_list_in))

print(higher_sum(number_list))

done()

print(
    "3. Create a function that takes a list of words and returns a list of their lengths.\n"
)

def wordlist_length(in_wordlist: list[str]) -> list[int]:
    return list(map(lambda word: len(word), in_wordlist))

print(wordlist_length(wordlist))

done()

print(
    "4. Create a function that takes a list of words and returns the total number of letters in them.\n"
)

def list_letter_count(wordlist: list[str]) -> int:
    return reduce(lambda count, size: size + count,
                  map(lambda word: len(word),
                       wordlist))

print(list_letter_count(wordlist))

done()

print(
    "5. Create a function that takes a list of numbers and returns the largest number.\n"
)

def maximum(in_number_list: list[int]) -> int:
    return reduce(lambda acc, number: (number if number > acc else acc), in_number_list)

print(maximum(number_list))

done()

print(
    "6. Create a function that takes a list of numbers and returns the smallest number.\n"
)

def minimum(in_number_list: list[int]) -> int:
    return reduce(lambda acc, number: (number if number < acc else acc), in_number_list)

print(minimum(number_list))

done()

print("7. Create a function that takes a list of numbers and returns the average.\n")

def average(in_number_list: list[int]) -> float:
    return (
        int(reduce(lambda acc, number: acc + number, in_number_list)) /
        int(reduce(lambda acc, _: acc + 1, in_number_list))
    )

print(average(number_list))

done()

print(
    "8. Create a function that takes a list of numbers and returns a new list with only the even numbers.\n"
)

def even_only(in_number_list: list[int]) -> list[int]:
    return list(filter(lambda number: number % 2 == 0, in_number_list))

print(even_only(number_list))

done()

print(
    "9. Create a function that takes a list of numbers and returns a new list with only the odd numbers.\n"
)

def odd_only(in_number_list: list[int]) -> list[int]:
    return list(filter(lambda number: number % 2 == 1, in_number_list))

print(odd_only(number_list))

done()

print(
    "10. Create a function that takes a list of numbers and returns a new list with the numbers sorted in ascending order.\n"
)

def sorted_list(in_number_list: list[int]) -> list[int]:
    return sorted(in_number_list, key=lambda x: x)

print(sorted_list(number_list))

done()

print(
    "11. Create a function that takes a list of numbers and returns a new list with the numbers sorted in descending order.\n"
)

def reverse_sorted_list(in_number_list: list[int]) -> list[int]:
    return sorted(in_number_list, key=lambda x: -x)

print(reverse_sorted_list(number_list))

done()

print(
    "12. Create a function that takes a list of words and returns a new list with only the words that start with a certain letter.\n"
)

def words_of_a_letter(wordlist: list[str], letter: str) -> list[str]:
    return list(filter(lambda word: (word.startswith(letter)), wordlist))

print(f"Chosen Letter: {random_letter}")
print(words_of_a_letter(wordlist, random_letter))

done()

print(
    "13. Create a function that takes a list of words and returns a new list with only the words that contain a certain letter.\n"
)

def words_containing_letter(wordlist: list[str], letter: str) -> list[str]:
    return list(filter(lambda word: (letter in word), wordlist))

print(f"Chosen Letter: {random_letter}")
print(words_containing_letter(wordlist, random_letter))

done()

print(
    "14. Create a function that takes a list of words and returns a new list with the words sorted in alphabetical order.\n"
)

def sort_words(wordlist: list[str]) -> list[str]:
    return sorted(wordlist, key=lambda word: word)

print(f"Sorted wordlist: {sort_words(wordlist)}")

done()

print(
    "15. Create a function that takes a list of words and returns a new list with the words sorted in reverse alphabetical order.\n"
)

def sort_words_reverse(wordlist: list[str]) -> list[str]:
    return sorted(wordlist, key=lambda word: word)[::-1]

print(f"Reverse-sorted wordlist: {sort_words_reverse(wordlist)}")

done()

print(
    "16. Create a function that takes a list of dictionaries and returns a new list with only the dictionaries that have a certain key.\n"
)

def check_grade(contacts: list[dict]) -> list[dict]:
    return list(filter(lambda contact: 'grade' in contact.keys(), contacts))

print(f"Students with grades:\n {check_grade(contact_list)}")

done()

print(
    "17. Create a function that takes a list of dictionaries and returns a new list with only the dictionaries that have a certain key value.\n"
)

def tld_domains(contacts: list[dict], tld: str) -> list[dict]:
    return list(filter(lambda contact: tld in contact['email'], contacts))

print(f"Top-Level Domain: {random_tld}")
print(tld_domains(contact_list, random_tld))    

done()

print(
    "18. Create a function that takes a list of dictionaries and returns a new list with the dictionaries sorted by a certain key.\n"
)

def sorted_contacts(contacts: list[dict]) -> list[dict]:
    return sorted(contacts, key=lambda contact: contact['name'])

print(sorted_contacts(contact_list))

done()

print(
    "19. Create a function that takes a list of dictionaries and returns the sum of values for a certain key for all the dictionaries.\n"
)

def total_population(cities: list[dict]) -> int:
    return int(
        reduce(lambda acc, num: (acc + num),
               map(lambda city: city['population'], cities))
    )

print(f"Total Population of Coastal Cities: {total_population(cities)}")

done()

print(
    "20. Create a function that takes a list of dictionaries and returns the average of values for a certain key for all the dictionaries.\n"
)

def average_population(cities: list[dict]) -> float:
    coastal_populations = list(map(lambda city: city["population"], cities))
    return total_population(cities)/len(coastal_populations)

print(f"Average population of Coastal Cities: {average_population(cities)}")

done()

print(
    "21. Create a function that takes a list of dictionaries and returns the maximum value for a certain key.\n"
)

def highest_population(cities: list[dict]) -> int:
    return int(reduce(lambda acc, city: city if city > acc else acc,
                       map(lambda city: city["population"], cities)))

print(f"Largest Coastal City Population: {highest_population(cities)}")

done()

print(
    "22. Create a function that takes a list of dictionaries and returns the minimum value for a certain key.\n"
)

def lowest_population(cities: list[dict]) -> int:
    return int(reduce(lambda acc, city: city if city < acc else acc,
                       map(lambda city: city["population"], cities)))

print(f"Smallest Coastal City Population: {lowest_population(cities)}")

done()

print(
    "23. Create a function that takes a list of dictionaries and returns a new list with the dictionaries grouped by a certain key.\n"
)

def city_latitude(cities: list[dict]) -> list[list[dict]]:
    key_func = lambda x: x["latitude"]
    return [list(group) for (_, group) in groupby(sorted(cities, key=key_func), key=key_func)]

print(city_latitude(cities))

done()

print(
    "24. Create a function that takes a list of numbers and returns a new list with the numbers grouped by their range (e.g. 0-9, 10-19, etc.).\n"
)

def number_group(number: int) -> str:
    if number < 10:
        return "0-9"
    elif number <20:
        return "10-19"
    elif number <30:
        return "20-29"
    elif number <40:
        return "30-39"
    elif number <50:
        return "40-49"
    elif number <60:
        return "50-59"
    elif number <70:
        return "60-69"
    elif number <80:
        return "70-79"
    elif number <90:
        return "80-89"
    elif number <100:
        return "90-99"
    else:
        return "100+"

def organize(number_list: list[int]):
    ordered_list = sorted(number_list)
    return [[group, list(values)] for group, values in groupby(ordered_list, key=number_group)]

for group, numbers in organize(random_number_list):
    print(f"{group}: {numbers}")
    
done()

print(
    "25. Create a function that takes a list of numbers and returns a new list with the numbers grouped by their parity (even or odd).\n"
)

def parity(number):
    return 'Even' if number % 2 == 0 else 'Odd'

def group_by_parity(number_list: list[int]):
    ordered_list = sorted(number_list, key=parity)
    return [[group, list(values)] for group, values in groupby(ordered_list, parity)]

for group, numbers in group_by_parity(random_number_list):
    print(f"{group}: {numbers}")

done()
