"""lambda_exercises: Exercises to practice lambda functions."""

# Imports

from itertools import count
from random import randint
from functools import reduce

# Global variables



# Separates exercises

def done():
    print("\n---\n")

print("1. Write a lambda function to add two numbers and print the result.\n")

add_numbers = lambda num1, num2: num1 + num2

print(add_numbers(1, 2))

done()

print(
    "2. Write a lambda function to calculate the square of a number and print the",
    "result.\n",
)

perfect_square = lambda num: num * num

print(perfect_square(3))

done()

print("3. Write a lambda function to convert Celsius to Fahrenheit.\n")

c_to_f = lambda c: (9/5) * c + 32
random_number = randint(0, 32)

print(f"{random_number}°C = {round(c_to_f(random_number),1)}°F")

done()

print("4. Write a lambda function to calculate the factorial of a number.\n")

factorial = lambda number: reduce(lambda acc, num: acc * num, range(1, number+1))

random_number = randint(1, 6)
print(f"Factorial of {random_number} is {factorial(random_number)}")

done()

print("5. Write a lambda function to find the sum of all elements in a list.\n")

list_sum = lambda number_list: reduce(lambda acc, number: acc + number, number_list)
random_list = [randint(1, 10) for _ in range(0, randint(4, 10))]

print(random_list)
print(list_sum(random_list))

done()

print("6. Write a lambda function to find the largest number in a list.\n")

largest = lambda number_list: reduce(lambda acc, num: num if num > acc else acc, number_list)

print(random_list)
print(list_largest(random_list))

done()

print("7. Write a lambda function to find the smallest number in a list.\n")

smallest = lambda numbers: reduce(lambda acc, num: num if num < acc else acc, numbers)

print(random_list)
print(smallest(random_list))

done()

print("8. Write a lambda function to find the average of all elements in a list.\n")

average = lambda numbers: sum(numbers)/len(numbers)

print(random_list)
print(average(random_list))

done()

print("9. Write a lambda function to reverse a string.\n")

reverse = lambda string: string[::-1]

print(reverse("words"))

done()

print(
    "10. Write a lambda function to count the number of characters in a",
    "string.\n",
)

string_count = lambda element: len(element)

print(string_count("something"))

done()

print("11. Write a lambda function to find the length of a list.\n")

list_count = string_count

print(random_list)
print(list_count(random_list))

done()

print("12. Write a lambda function to check if a number is even.\n")

is_even = lambda num: num % 2 == 0

print(random_number)
print(is_even(random_number))

done()

print("13. Write a lambda function to check if a number is odd.\n")

is_odd = lambda num: num % 2 == 1

print(random_number)
print(is_odd(random_number))

done()

print("14. Write a lambda function to check if a number is positive.\n")

is_positive = lambda num : num > 0

random_number = randint(-10, 10)

print(random_number)
print(is_positive(random_number))

done()

print("15. Write a lambda function to check if a number is negative.\n")

is_negative = lambda num : num < 0

print(random_number)
print(is_negative(random_number))

done()

print("16. Write a lambda function to check if a number is zero.\n")

is_zero = lambda num : num == 0

print(random_number)
print(is_zero(random_number))

done()

print("17. Write a lambda function to check if a string is a palindrome.\n")

is_palindrome = lambda string: string == string[::-1]

print(is_palindrome("something"), is_palindrome("radar"))

done()

print(
    "18. Write a lambda function to check if a list is sorted in ascending",
    "order.\n",
)

is_sorted_ascending = lambda lst: lst == sorted(lst)

print(is_sorted_ascending([1, 2, 3, 4, 5]), is_sorted_ascending([1, 3, 2, 4, 5]))

done()

print(
    "19. Write a lambda function to check if a list is sorted in descending",
    "order.\n",
)

is_sorted_ascending = lambda lst: lst == sorted(lst)[::-1]

print(is_sorted_ascending([1, 2, 3, 4, 5]), is_sorted_ascending([5, 4, 3, 2, 1]))

done()

print("20. Write a lambda function to find the second largest number in a list.\n")

second_largest = lambda numlist: sorted(set(numlist), reverse=True)[1]

print(second_largest([5,3,4,2,1]))

done()

print("21. Write a lambda function to find the second smallest number in a list.\n")

second_smallest = lambda numlist: sorted(set(numlist))[1]

print(second_smallest([5,3,4,2,1]))

done()

print("22. Write a lambda function to find the median of all elements in a list.\n")

median = lambda numlist: sorted(set(numlist))[round(len(set(numlist))/2)]

print(median([5,3,4,2,1]))

done()

print("23. Write a lambda function to find the mode of all elements in a list.\n")

list_mode = lambda numlist: max(numlist, key=numlist.count)

print(list_mode([9, 9, 9, 9, 9, 4, 3, 3, 2, 2, 2, 2, 1]))

done()

print("24. Write a lambda function to calculate the nth Fibonacci number.\n")

fib_number = lambda num: num if num <= 1 else fib_number(num-1) + fib_number(num-2)

print(fib_number(6))

done()

print("25. Write a lambda function to calculate the nth power of a number.\n")

number_power = lambda num, power: num ** power

print(number_power(2,4))

done()
