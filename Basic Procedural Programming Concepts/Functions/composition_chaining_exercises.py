"""composition_chaining_exercises: Exercises for composing and chaining multiple functions."""

from random import randint
import re
from typing import Callable
import math
import string

# Marks exercises


def done() -> None:
    print("\n---\n")


# Create functions

print(
    "1. Compose two functions to convert a string to uppercase and strip leading",
    "and trailing whitespaces.\n",
)


def convert_to_uppercase(string: str) -> str:
    return string.upper()


def strip_leading(string: str) -> str:
    return string.strip()


text_string = " this is something to work on "

print(f"'{strip_leading(convert_to_uppercase(text_string))}'")

done()

print(
    "2. Compose three functions to split a string into words, count the number of",
    "words, and sum the length of all the words.\n",
)


def string_to_words(text_string: str) -> list[str]:
    return text_string.strip().split(" ")


def count_words(wordlist: list[str]) -> list[int]:
    count_list = []
    for word in wordlist:
        count = 0
        for _ in word:
            count += 1
        count_list.append(count)
    return count_list


def total_list(numlist: list[int]) -> int:
    total = 0
    for number in numlist:
        total += number
    return total


text_string = "this is a string to work on"

print(
    f"Number of letters in '{string}':",
    f"{total_list(count_words(string_to_words(text_string)))}",
)

done()

print(
    "3. Compose two functions to check if a given number is even or not and return ",
    '"EVEN" or "ODD".\n',
)


def is_even(number: int) -> bool:
    return number % 2 == 0


def even_or_odd(even: bool) -> str:
    return "EVEN" if even else "ODD"


number = randint(0, 10)

print(f"{number}: {even_or_odd(is_even(number))}")

done()


print(
    "4. Compose two functions to format a list of numbers into a string with commas",
    'and add "and" before the last number.\n',
)


def list_to_string(data: list) -> str:
    string_list = []
    for item in data:
        string_list.append(str(item))
    return ", ".join(string_list)


def insert_and(string: str) -> str:
    string_list = string.split(", ")
    string_list[-1] = "and " + string_list[-1]
    return ", ".join(string_list)


number_list = [randint(0, 10) for _ in range(0, randint(4, 10))]

print(
    f"Formatting {number_list} for use in a sentence:\n"
    + f"We have the winning lottery numbers {insert_and(list_to_string(number_list))}"
)

done()

print(
    "5. Compose two functions to sort a list of dictionaries based on values in a",
    "specific key and return the first element.\n",
)

list_of_dicts = [
    {"name": "Jack", "age": 15, "hair color": "blonde", "eye color": "green"},
    {"name": "Bob", "age": 12, "hair color": "brunette", "eye color": "blue"},
    {"name": "David", "age": 10, "hair color": "red", "eye color": "brown"},
]


def sort_list_by_dict(
    list_of_dicts: list[dict],
    key,
) -> list[dict]:
    # In this case, we're using a lambda with sorted() to sort the lists.
    return sorted(list_of_dicts, key=lambda attribute: attribute[key])


def first_element(list_of_dicts: list[dict]) -> dict:
    return list_of_dicts[0]


print(first_element(sort_list_by_dict(list_of_dicts, "age")))

done()

print(
    "6. Compose two functions to find the average of all the numbers in a list after",
    "removing the maximum and minimum values.\n",
)

list_of_numbers = [randint(0, 10) for _ in range(randint(4, 10))]

print("Debugging:", list_of_numbers)

def strip_outliers(in_numlist: list[int]) -> list[int]:

    print("Debugging:", in_numlist)
    numlist = in_numlist.copy()
    maximum = max(numlist)
    minimum = min(numlist)

    for _ in range(0, numlist.count(maximum)):
        numlist.remove(maximum)

    for _ in range(0, numlist.count(minimum)):
        numlist.remove(minimum)

    return numlist


def list_average(numlist: list[int]) -> float:

    print("Debugging:", numlist)
    return sum(numlist) / len(numlist)


print(list_of_numbers)
print(list_average(strip_outliers(list_of_numbers)))

done()

print(
    "7. Compose two functions to extract all the URLs from a string and return a",
    "list of unique URLs.\n",
)

text_string = (
    "The most important sites to visit when looking into Python information are: "
    + "https://www.python.org, https://www.w3schools.com/python/, "
    + "https://www.geeksforgeeks.org/python-programming-language/, "
    + "and most importantly, https://www.stackoverflow.com."
    + "Keep in mind that https://www.stackoverflow.com is not a tutorial site, but "
    + "a place where you can get answers to questions."
)


def extract_urls(string: str) -> list[str]:
    url_regex = "(http[s]*:\\/\\/[\\w\\.\\/?&%_-]+)[.,\\s]"
    urls = []
    urls.extend(list(re.findall(url_regex, string)))
    return urls


def unique_string_list(list_of_urls: list[str]) -> list[str]:
    return list(set(list_of_urls))


print(unique_string_list(extract_urls(str(string))))

done()

print(
    "8. Compose two functions to find all the palindromic words in a list of words",
    "and return them in a new list.\n",
)

word_list = (
    "radar",
    "racecar",
    "madam",
    "civic",
    "rotator",
    "unrest",
    "sulphur",
    "anger",
    "confusion",
    "banquet",
)


def is_palindrome(word: str) -> bool:
    return word == word[::-1]


palindrome_words = list(map(str, filter(is_palindrome, word_list)))

print(palindrome_words)

done()

print(
    "9. Compose two functions to reverse a string and check if it is a palindrome",
    "or not.\n",
)


def if_palindrome(word: str) -> bool:
    return word == word[::-1]


def string_compare(argument: str, comparator: Callable) -> bool:
    return comparator(argument)


print(string_compare("radar", if_palindrome))

done()

print(
    "10. Compose two functions to extract all the email addresses from a string and",
    "return a list of unique email addresses.\n",
)

text_string = (
    "I have a few email addresses: reteo.varala@gmail.com, reteo@lampros.com, "
    + "lampros_liontos@outlook.com and lampros@petexec.net.  I really don't use "
    + "the last one much (lampros@petexec.net), nor do I use reteo.varala@gmail.com"
)


def extract_email_addresses(string: str) -> list[str]:
    return list(re.findall("(\\w\\S*@\\S*[a-z])", string))


print(unique_string_list(extract_email_addresses(text_string)))

done()

# print("11. Compose two functions to find the factorial of a number.\n")

# # Not really viable; requires a single function.

# done()

print(
    "12. Compose two functions to convert a string of hexadecimal values to a list",
    "of integers.\n",
)

numlist = [hex(randint(0, 255)) for _ in range(0, randint(4, 10))]


def hex_to_int(hexadecimal: str) -> int:
    return int(hexadecimal, 16)


def other_base_to_decimal(num_list: list[str], conversion) -> list[int]:
    return [conversion(number) for number in num_list]


print(numlist)
print(other_base_to_decimal(numlist, hex_to_int))

done()

print(
    "13. Compose two functions to find all the prime numbers in a list of numbers",
    "and return them in a new list.\n",
)


def sieve_of_eratosthenes(target):
    testing_list = [True for _ in range(0, target + 1)]
    testing_list[0] = testing_list[1] = False

    for potential_prime in range(2, int(math.sqrt(target)) + 1):
        if testing_list[potential_prime] == True:
            prime = potential_prime
            for factor in range(prime**2, target + 1, prime):
                testing_list[factor] = False

    return [prime for prime in range(2, target) if testing_list[prime] == True]


def find_matches(numlist: list[int], match_function) -> list[int]:
    high_number = max(numlist)
    match_list = match_function(high_number)
    return [number for number in numlist if number in match_list]


random_list = [randint(0, 20) for _ in range(0, randint(4, 10))]

print("Random List:", random_list)
print("Matches:", find_matches(random_list, sieve_of_eratosthenes))

done()

print(
    "14. Compose two functions to reverse a list of dictionaries based on values in",
    "a specific key.\n",
)

contacts = [
    {"name": "Jack Davis", "phone": "000-555-1212", "email": "jdavis@nooky.net"},
    {"name": "James Jordan", "phone": "123-456-7890", "email": "jjordan@morgan.com"},
    {"name": "Megan Nolan", "phone": "987-654-3210", "email": "mnolan@northfield.edu"},
]


def sort_by_element(contact_list: list[dict], key) -> list[dict]:
    return sorted(contact_list, key=key)


def reverse_order(contact_list: list[dict]) -> list[dict]:
    return contact_list[::-1]


print(reverse_order(sort_by_element(contacts, lambda contact: contact["phone"])))

done()

print(
    "15. Compose two functions to extract all the phone numbers from a string and",
    "return a list of unique phone numbers.\n",
)

phone_string = "If you need to get in touch with us, you can reach us at 555-123-4567. Our customer service team is available Monday to Friday from 9am to 5pm. If you're calling outside of these hours, you can leave a message at (555)123-4567 and we'll get back to you as soon as possible. For any urgent matters, please call +1-555-345-6789. If you're a preferred customer, you can call our dedicated line at (555) 456-7890. And if you're calling from outside the country, please dial 001-555-567-8901."


def extract_phone_numbers(text: str) -> list[str]:
    regex = "\\+?(\\d{0,3})[\\s.-]?\\(?(\\d{3})\\)?[\\s.-]?(\\d{3})[\\s.-]?(\\d{4})"

    phone_list = re.findall(regex, text)

    stage = []

    for number in phone_list:
        stage.append("-".join(number))

    result = []

    for number in stage:
        result.append(re.sub("^-", "", number))

    return result


def unique(text_list: list[str]) -> list[str]:
    return list(set(text_list))


print(unique(extract_phone_numbers(phone_string)))


done()

print(
    "16. Compose two functions to sort a list of numbers in descending order and",
    "return the sum of the first three numbers.\n",
)


def descending_sort(numlist: list[int]) -> list[int]:
    return sorted(numlist)[::-1]


def first_three_sum(numlist: list[int]) -> int:
    return sum(numlist[0:3])


print(numlist := [randint(0, 9) for _ in range(0, randint(4, 10))])
print(first_three_sum(descending_sort(numlist)))

done()

print(
    "17. Compose two functions to find the standard deviation of a list of numbers.\n"
)


done()

# print(
#     "18. Compose two functions to find the frequency of words in a string and return",
#     "a dictionary with word and frequency pairs.\n"
# )

# # There's not really a way to make a composable pair of functions,
# # since creating a dictionary with word and frequency pairs are the
# # explicit method of calculating frequency.

# done()

# print(
#     "19. Compose two functions to calculate the dot product of two lists of numbers.\n"
# )

# # That involves math I'm not ready to tackle at this time.  The dot
# # product is the scalar product of two vectors.

# done()

# print(
#     "20. Compose two functions to find all the substrings in a string and return",
#     "them in a new list.\n"
# )

text_string = "This is the time for all good men to come to the aid of the party!"


def remove_punctuation(text_string: str) -> str:
    return "".join(
        [letter for letter in text_string if letter not in string.punctuation]
    ).lower()


def find_substrings(text_string: str) -> list[str]:
    return text_string.split(" ")


def find_unique_items(source_list: list) -> list:
    return list(set(source_list))


print(find_unique_items(find_substrings(remove_punctuation(text_string))))

# done()

print(
    "21. Compose two functions to convert a list of numbers to a string of",
    "hexadecimal values.\n"
)

def convert_to_hex(numlist: list[int]) -> list[str]:
    return [hex(number) for number in numlist]

def convert_to_string(stringlist: list[str]) -> str:
    return ", ".join(stringlist)

numlist = [randint(0, 255) for _ in range(0, randint(4, 10))]

print(numlist)
print(convert_to_string(convert_to_hex(numlist)))

done()

# print(
#     "22. Compose two functions to remove all the punctuation from a string and",
#     "return the resulting string.\n"
# )

# # Not really a way to split the functionality; 

# done()

print("23. Compose two functions to find the mode of a list of numbers.\n")

numlist = [randint(1, 4) for _ in range(0, randint(6, 20))]

def histogram(numlist: list[int]) -> dict:
    histogram_dict = {}
    
    for num in numlist:
        histogram_dict[num] = histogram_dict.get(num, 0) + 1

    return histogram_dict

def find_highest_key(histogram_values: dict) -> int:
    histogram = [(k, v) for k, v in histogram_values.items()]
    histogram = sorted(histogram, key=lambda v: -v[1])
    return histogram[0][0]

print(numlist)
print("Mode:", find_highest_key(histogram(numlist)))

done()

# print("24. Compose two functions to calculate the magnitude of a 2D vector.\n")

# # Higher math than I'm willing to tackle right now.

# done()

# print("25. Compose two functions to calculate the roots of a quadratic equation.\n")

# # Higher math than I'm willing to tackle right now.

# done()
