"""arguments_return_values_exercises: Exercises covering the return
values and parameters of functions.

"""

from random import choice, randint


def done():
    """Prints a 3-dash line between two blank ones to show the end
    of an exercise

    """
    print("\n---\n")


# Enter functions here.


def make_sum(num1: int, num2: int):
    """1. Define a function that takes two arguments and returns their
    sum."""
    return num1 + num2


def count_list(data: list):
    """2. Define a function that takes a list as an argument and
    returns the length of the list.

    """
    return len(data)


def count_vowels(string: str):
    """3. Define a function that takes a string as an argument and
    returns the number of vowels in the string.

    """
    vowels = [x for x in string if x in "aeiou"]
    return len(vowels)


def larger_number(num1: int, num2: int):
    """4. Define a function that takes two numbers as arguments and
    returns the larger of the two.

    """
    return num1 if num1 > num2 else num2


def list_avg(numlist: list[int]):
    """5. Define a function that takes a list of numbers as an
    argument and returns the average of the numbers.

    """
    return sum(numlist) / len(numlist)


def longest_string(strings: list[str]):
    """6. Define a function that takes a list of strings as an
    argument and returns the longest string.

    """
    longest = ""
    for string in strings:
        if len(string) > len(longest):
            longest = string
    return longest


def generate_list_of_lists():
    """Generates a list of lists containing integers."""
    list_of_lists = []
    for _ in range(1, randint(3, 7)):
        current_list = [randint(0, 10) for x in range(1, randint(3, 7))]
        list_of_lists.append(current_list)
    return list_of_lists


def sum_of_lists(all_lists: list[list[int]]):
    """7. Define a function that takes a list of lists as an argument
    and returns the sum of all the elements.

    """
    total = 0
    for current_list in all_lists:
        for number in current_list:
            total = total + number
    return total


def card_list(cards: dict):
    """8. Define a function that takes a dictionary as an argument and
    returns the keys of the dictionary.

    """
    return list(cards.keys())


def first_tuple_element(data: tuple[int]):
    """9. Define a function that takes a tuple as an argument and
    returns the first element of the tuple.

    """
    return data[0]


def reverse_string(string: str):
    """10. Define a function that takes a string as an argument and
    returns the string reversed.

    """
    return string[::-1]


def list_even_sum(data: list[int]):
    """11. Define a function that takes a list of numbers as an
    argument and returns the sum of all even numbers in the list.

    """
    return sum((x for x in data if x % 2 == 0))


def string_count(data: list[str], string: str):
    """12. Define a function that takes a list of strings as an
    argument and returns the number of times a specific string appears
    in the list.

    """
    return len([word for word in data if word is string])


def is_prime(number: int):
    """Determine if a number is a prime."""
    if -1 < number < 1:
        return False
    for factor in range(2, int(abs(number / 2))):
        if number % factor == 0:
            return False
    return True


def find_primes(data: list[int]):
    """13. Define a function that takes a list of numbers as an
    argument and returns a new list of only the prime numbers from the
    original list.

    """
    prime_list = []
    for num in data:  # use 'continue' if not a prime
        if is_prime(num) is True:
            prime_list.append(num)
    return prime_list


def find_smallest_integer(data: list[int]):
    """14. Define a function that takes a list of numbers as an
    argument and returns the second smallest number in the list.

    """
    return min(data)


def sort_inner_lists(data: list[list[int]]):
    """15. Define a function that takes a list of lists as an argument
    and returns a new list of lists, each inner list sorted in
    ascending order.

    """
    new_data = []

    for current_list in data:
        current_list.sort()
        new_data.append(current_list)
    return new_data


def three_vowel_words(data: list[str]):
    """16. Define a function that takes a list of words as an argument
    and returns a new list of words that have at least 3 vowels.

    """
    # We're gonna cheat a little; we'll use the count_vowels()
    # function we created earlier.
    return [word for word in data if count_vowels(word) >= 3]


def remove_vowels(word: str):
    """17. Define a function that takes a string as an argument and
    returns a new string with all the vowels removed.

    """
    return "".join([letter for letter in word if letter not in "aeiou"])


def product_list(data: list[int]):
    """18. Define a function that takes a list of numbers as an
    argument and returns the product of all the numbers in the list.

    """
    total = ""
    for number in data:
        if total == "":
            total = number
        else:
            total *= number
    return total


def generate_substring(word: str):
    """Generates a substring of a word."""
    half = int(len(word) / 2)
    start = randint(0, half - 1)
    end = randint(start + 2, start + half)

    return word[start:end]


def list_using_substring(wordlist: list[str], substring: str):
    """19. Define a function that takes a list of strings as an
    argument and returns a new list of strings that contain a specific
    substring.

    """
    return [word for word in wordlist if substring in word]


def multiples_of_5(numbers: list[int]):
    """20. Define a function that takes a list of numbers as an
    argument and returns a new list of numbers that are multiples of a
    specific number.

    """
    return [x for x in numbers if x % 5 == 0]


def card_value_list(cards: dict):
    """21. Define a function that takes a dictionary as an argument
    and returns the values of the dictionary.

    """
    return list(cards.values())


def above_50_numbers_avg(data: list[int]):
    """22. Define a function that takes a list of numbers as an
    argument and returns the average of all the numbers that are
    greater than a specific number.

    """
    return list_avg([x for x in data if x > 50])

def string_list_e_count(data: list[str]):
    """23. Define a function that takes a list of strings as an
    argument and returns the number of times a specific character
    appears in all the strings combined.

    """
    total = 0
    for word in data:
        total += word.count("e")
    return total

def list_median(data):
    """24. Define a function that takes a list of numbers as an
    argument and returns the median of the numbers.

    """
    data.sort()
    return data[int(round(len(data)/2))]

def words_starting_with_t(words: list[str]):
    """25. Define a function that takes a list of words as an argument
    and returns a new list of words that have a specific letter as
    their first character.

    """
    return [word for word in words if word.startswith("t")]

# Main loop

# Main loop should be exempt from the "too many statements" issue.
# pylint: disable=R0915
def main():
    """Main Loop"""
    ## Define initial values here

    # Word List
    word_list = [
        "testify",
        "pity",
        "rumor",
        "transparent",
        "flat",
        "coach",
        "high",
        "deviation",
        "exclusive",
        "hostile",
        "survey",
        "activity",
        "tragedy",
        "foreigner",
        "ambiguity",
    ]

    words = [choice(word_list) for x in range(3, 10)]

    # Word
    word = choice(words)

    # Number List
    number_list = [randint(1, 100) for x in range(0, randint(3, 15))]

    # Number 1
    number_1 = choice(number_list)

    # Number 2
    number_2 = choice(number_list)

    # Dictionary
    cards_scores = dict(
        zip(
            [
                "two",
                "three",
                "four",
                "five",
                "six",
                "seven",
                "eight",
                "nine",
                "ten",
                "jack",
                "queen",
                "king",
                "ace",
            ],
            [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14],
        )
    )
    # Tuple

    number_tuple = tuple(randint(1, 100) for x in range(0, randint(3, 15)))

    ## Questions start here ###

    print("1. Define a function that takes two arguments and returns their sum.\n")

    print(f"{number_1} + {number_2} = {make_sum(number_1, number_2)}")

    done()

    print(
        "2. Define a function that takes a list as an argument and",
        "returns the length of the list.\n",
    )

    print(f"The list {number_list} has {count_list(number_list)} elements")

    done()

    print(
        "3. Define a function that takes a string as an argument and",
        "returns the number of vowels in the string.\n",
    )

    print(f"The word {word} has {count_vowels(word)} vowels.")

    done()

    print(
        "4. Define a function that takes two numbers as arguments and",
        "returns the larger of the two.\n",
    )

    print(
        f"The larger of {number_1} and {number_2} is",
        f"{larger_number(number_1, number_2)}",
    )

    done()

    print(
        "5. Define a function that takes a list of numbers as an argument",
        "and returns the average of the numbers.\n",
    )

    print(f"The average of the list {number_list} is {round(list_avg(number_list),2)}")

    done()

    print(
        "6. Define a function that takes a list of strings as an argument",
        "and returns the longest string.\n",
    )

    print(f"The list is {words}")
    print(
        f'The longest word in the list is "{longest_string(words)}"',
        f"at {len(longest_string(words))} characters.",
    )

    done()

    print(
        "7. Define a function that takes a list of lists as an argument",
        "and returns the sum of all the elements.\n",
    )

    lists_list = generate_list_of_lists()

    print(f"List of lists: {lists_list}")
    print("The sum of all numbers in all the lists is:", f"{sum_of_lists(lists_list)}")

    done()

    print(
        "8. Define a function that takes a dictionary as an argument and",
        "returns the keys of the dictionary.\n",
    )

    print(f"Card Dictionary: {cards_scores}")
    print(f"Card List: {card_list(cards_scores)}")

    done()

    print(
        "9. Define a function that takes a tuple as an argument and",
        "returns the first element of the tuple.\n",
    )

    print(
        f"The first element of {number_tuple} is",
        f"{first_tuple_element(number_tuple)}",
    )

    done()

    print(
        "10. Define a function that takes a string as an argument and",
        "returns the string reversed.\n",
    )

    print(f"The reverse of the string {word} is {reverse_string(word)}.")

    done()

    print(
        "11. Define a function that takes a list of numbers as an",
        "argument and returns the sum of all even numbers in the list.\n",
    )

    print(
        f"The sum of the even numbers in the list {number_list}",
        f"is {list_even_sum(number_list)}",
    )

    done()

    print(
        "12. Define a function that takes a list of strings as an argument",
        "and returns the number of times a specific string appears in the",
        "list.\n",
    )

    local_words = [choice(words) for x in range(1, 20)]
    local_word = choice(local_words)

    print(f"The list: {local_words}\n")
    print(
        f"The word '{local_word}' appears",
        f"{string_count(local_words, local_word)} times.",
    )

    done()

    print(
        "13. Define a function that takes a list of numbers as an argument",
        "and returns a new list of only the prime numbers from the",
        "original list.\n",
    )

    print(f"The number list is {number_list}.")
    prime_list = find_primes(number_list)
    print(
        "The list of primes from the number list is",
        f"{prime_list if len(prime_list) > 0 else 'empty'}.",
    )

    done()

    print(
        "14. Define a function that takes a list of numbers as an argument",
        "and returns the second smallest number in the list.\n",
    )

    print(f"The list is {number_list}")
    print(f"The smallest number is {find_smallest_integer(number_list)}.")

    done()

    print(
        "15. Define a function that takes a list of lists as an argument and",
        "returns a new list of lists, each inner list sorted in ascending",
        "order.\n",
    )

    value_lists = generate_list_of_lists()

    print(
        f"Before sorting: {value_lists}\nAfter sorting: {sort_inner_lists(value_lists)}"
    )

    done()

    print(
        "16. Define a function that takes a list of words as an argument and",
        "returns a new list of words that have at least 3 vowels.\n",
    )

    print(f"Word list: {words}")
    print(f"Word list with 3+ vowels: {three_vowel_words(words)}")

    done()

    print(
        "17. Define a function that takes a string as an argument and",
        "returns a new string with all the vowels removed.\n",
    )

    print(f"Original word: {word}")
    print(f"Vowel-stripped word: {remove_vowels(word)}")

    done()

    print(
        "18. Define a function that takes a list of numbers as an argument",
        "and returns the product of all the numbers in the list.\n",
    )

    print(f"Original List: {number_list}")
    print(f"Thew product of this list is: {product_list(number_list)}.")

    done()

    print(
        "19. Define a function that takes a list of strings as an argument",
        "and returns a new list of strings that contain a specific",
        "substring.\n",
    )

    substring = generate_substring(choice(word_list))

    print(f"Word List: {word_list}")
    print(f"Substring: {substring}")
    print("Word List using Substring:", f"{list_using_substring(word_list, substring)}")

    done()

    print(
        "20. Define a function that takes a list of numbers as an argument",
        "and returns a new list of numbers that are multiples of a specific",
        "number.\n",
    )

    mult5 = multiples_of_5(number_list)

    print(f"Numbers: {number_list}")
    print(f"Multiples of 5: {mult5 if len(mult5) > 0 else None}")

    done()

    print(
        "21. Define a function that takes a dictionary as an argument and",
        "returns the values of the dictionary.\n",
    )

    print(f"From cards: {cards_scores}")
    print(f"The scores: {card_value_list(cards_scores)}")

    done()

    print(
        "22. Define a function that takes a list of numbers as an argument",
        "and returns the average of all the numbers that are greater than a",
        "specific number.\n",
    )

    print(f"Numbers List: {number_list}")
    print(f"Average of items above 50: {above_50_numbers_avg(number_list)}")

    done()

    print(
        "23. Define a function that takes a list of strings as an argument",
        "and returns the number of times a specific character appears in all",
        "the strings combined.\n"
    )

    print(f"Word List: {word_list}")
    print(f"Number of times 'e' appears: {string_list_e_count(word_list)}")

    print(
        "24. Define a function that takes a list of numbers as an argument",
        "and returns the median of the numbers.\n"
    )

    print(f"Number List: {number_list}")
    print(f"Median Number: {list_median(number_list)}")

    done()

    print(
        "25. Define a function that takes a list of words as an argument and",
        "returns a new list of words that have a specific letter as their",
        "first character.\n"
    )

    print(f"Word List: {word_list}")
    print(f"Words starting with 't': {words_starting_with_t(word_list)}")

    done()

main()
