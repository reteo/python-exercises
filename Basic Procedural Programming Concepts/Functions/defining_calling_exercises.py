"""defining_calling_exercises: Exercises covering the definition and calling of fuctions."""

from random import randint, choice, random
from math import pi


def done():
    """Prints a line divided by spaces to split the exercises."""
    print("\n---\n")


# All the functions will be created here, and called from the main() function.


def greet(name: str):
    """Greets a person whose name is in the parameter."""
    print(f"Hello, {name}!")


def add(a: int, b: int):
    """Adds two numbers together."""
    return a + b


def is_even(number):
    """Returns True if a number is even, False if not."""
    return number % 2 == 0


def multiply_list(nums: list[int]):
    """Takes in a list of numbers and multiplies them together."""
    result = ""
    for num in nums:
        if result == "":
            result = num
        else:
            result *= num
    return result


def find_max(nums: list[int]):
    """Finds the maximum number in a list."""
    maximum = ""
    for num in nums:
        if maximum == "":
            maximum = num
        elif maximum < num:
            maximum = num
    return maximum


def is_palindrome(word: str):
    """Determines if the word is a palindrome (same if read both ways)"""
    return word == word[::-1]


def convert_to_fahrenheit(celsius: int):
    """Converts a Celsius temperature to Fahrenheit temperature."""
    return (9 / 5) * celsius + 32


def area_of_circle(radius: float):
    """Calculates the area of a circle from the radius"""
    return pi * (radius**2)


def greet_many(names: list[str]):
    """Takes in a list of names, and greets each one individually."""
    for name in names:
        print(f"Hello, {name}!")


def string_concat(string1: str, string2: str):
    """Concatenates two strings."""
    return string1 + string2


def is_prime(number: int):
    """If a number is prime, returns True.  Otherwise, returns False."""
    if number > 1 or number < -1:
        for i in range(2, int(abs(number / 2))):
            if number % i == 0:
                return False
        return True
    return False


def factorial(number: int):
    """Returns a factorial of a number."""
    result = ""
    for i in range(1, number + 1):
        if result == "":
            result = i
        else:
            result *= i
    return result


def convert_to_inches(centimeters: int):
    """Converts centimeters to inches."""
    return centimeters * 0.39370

def is_leap_year(year):
    """Returns true if the year is a leap year."""
    return (year % 4 == 0 and    # The year must be evenly divisible by 4.
            (year % 100 != 0 or  # Years evenly divisible by 100 are not leap years.
             year % 400 == 0))   # unless they are evenly divisible by 400.

def sum_of_digits(number: int):
    """Adds the digits of a number together."""
    digits = [int(x) for x in str(number)]
    return sum(digits)

def reverse_list(nums: list[int]):
    """Reverses the order of a list of numbers."""
    return nums[::-1]

def sort_list(nums: list[int]):
    """Sorts the numbers in a list"""
    new_nums = nums.copy()
    new_nums.sort()
    return new_nums

def is_vowel(char: str):
    """Checks to see if a character is a vowel."""
    return char in "aeiou"

def string_length(string: str):
    """Returns the length of a string."""
    return len(string)

def is_even_list(nums: list[int]):
    """Returns whether each element of a list is even or not."""
    return [num % 2 == 0 for num in nums]

def divisible_by_3(nums: list[int]):
    """Takes a list of numbers, and returns a list with only those numbers divisible by 3."""
    return [num for num in nums if num % 3 == 0]

def string_to_list(string: str):
    """Converts a string into a list of characters."""
    return list(string)

def list_to_string(lst: list[str]):
    """Converts a list of characters into a string."""
    return "".join(lst)

def remove_duplicates(nums: list[int]):
    """Removes duplicate numbers from a list."""
    nums_set = set(nums)
    return list(nums_set)

def power_of_two(num):
    """Raises a number to the power of two"""
    return pow(num, 2)

# The main function

# pylint: disable=R0915, disable=R0914, disable=C0301
def main():
    """Main loop."""
    print(
        '1. Define a function called "greet" that takes one parameter, "name", and prints "Hello, [name]!" when called.'
    )

    name = "Jack"
    print(f"Name: {name}")

    greet(name)

    done()

    print(
        '2. Define a function called "add" that takes two parameters, "a" and "b", and returns the sum of the two numbers when called.'
    )

    num1 = randint(0, 100)
    num2 = randint(0, 100)
    print(f"First: {num1}, Second: {num2}")

    print(add(num1, num2))

    done()

    print(
        '3. Define a function called "is_even" that takes one parameter, "num", and returns True if the number is even and False if the number is odd when called.'
    )

    number = randint(0, 100)
    print(f"Number: {number}")
    print(f"Even: {is_even(number)}")

    done()

    print(
        '4. Define a function called "multiply_list" that takes one parameter, "nums", a list of numbers, and returns the product of all the numbers in the list when called.'
    )

    number_list = [randint(1, 10) for i in range(0, randint(0, 10))]
    print(f"Numbers List: {number_list}")
    print(multiply_list(number_list))

    done()

    print(
        '5. Define a function called "find_max" that takes one parameter, "nums", a list of numbers, and returns the maximum number in the list when called.'
    )

    print(f"Numbers List: {number_list}")
    print(find_max(number_list))

    done()

    print(
        '6. Define a function called "is_palindrome" that takes one parameter, "word", and returns True if the word is a palindrome (the same forwards and backwards) and False if it is not when called.'
    )

    word1 = "something"  # Not a palindrome
    word2 = "radar"  # palindrome

    print(f"{word1} is palindrome?  {is_palindrome(word1)}")
    print(f"{word2} is palindrome?  {is_palindrome(word2)}")

    done()

    print(
        '7. Define a function called "convert_to_fahrenheit" that takes one parameter, "celsius", and returns the equivalent temperature in Fahrenheit when called.'
    )

    temp = randint(-6, 22)

    print(f"The temperature is {temp}°C, or {convert_to_fahrenheit(temp)}°F")

    done()

    print(
        '8. Define a function called "area_of_circle" that takes one parameter, "radius", and returns the area of a circle with that radius when called.'
    )

    radius = randint(1, 50) + round(random(), 2)

    print(
        f"The area of a circle with a radius of {radius} is {round(area_of_circle(radius), 2)}."
    )

    done()

    print(
        '9. Define a function called "greet_many" that takes one parameter, "names", a list of names, and prints "Hello, [name]!" for each name in the list when called.'
    )

    names = ["Michael", "Thomas", "Janet", "Emmett", "Nancy", "Susan"]
    print(f"List of names: {names}\n")

    greet_many(names)

    done()

    print(
        '10. Define a function called "string_concat" that takes two parameters, "string1" and "string2", and returns the concatenation of the two strings when called.'
    )

    string1 = "Ground Control"
    string2 = " to Major Tom"
    print(f"String1: {string1} \nString2: {string2}")

    print(string_concat(string1, string2))

    done()

    print(
        '11. Define a function called "is_prime" that takes one parameter, "num", and returns True if the number is prime and False if it is not when called.'
    )

    number = randint(0, 100)

    print(f"Is {number} a prime?  {is_prime(number)}")

    done()

    print(
        '12. Define a function called "factorial" that takes one parameter, "num", and returns the factorial of the number when called.'
    )

    number = randint(0, 10)

    fact_value = factorial(number)
    print(f" The factorial for {number} is {fact_value}.")

    done()

    print(
        '13. Define a function called "convert_to_inches" that takes one parameter, "centimeters", and returns the equivalent length in inches when called.'
    )

    distance = randint(1, 100) + random()

    print(
        f"{round(distance, 2)} centimeters is equal to {round(convert_to_inches(distance), 2)} inches."
    )

    done()

    print('14. Define a function called "is_leap_year" that takes one parameter, "year", and returns True if the year is a leap year and False if it is not when called.')

    leap_years = [2000, 2400, 2404, 1980]
    non_leap_years = [1800, 1900, 2100, 2200, 2300, 2500]

    print(f"Test leap years: {[is_leap_year(x) for x in leap_years]}")
    print(f"Test leap years: {[is_leap_year(x) for x in non_leap_years]}")

    done()

    print('15. Define a function called "sum_of_digits" that takes one parameter, "num", and returns the sum of the digits of the number when called.')

    number = randint(10, 999)

    print(f"Sum of digits for {number}: {sum_of_digits(number)}")

    done()

    print('16. Define a function called "reverse_list" that takes one parameter, "nums", a list of numbers, and returns the list in reverse order when called.')

    number_list = [randint(0, 100) for x in range(3, 10)]

    print(f"Original List: {number_list}")
    print(f"Reverse List: {reverse_list(number_list)}")

    done()

    print('17. Define a function called "sort_list" that takes one parameter, "nums", a list of numbers, and returns the list sorted in ascending order when called.')

    number_list = [randint(0, 100) for x in range(3, 10)]

    print(f"Original List: {number_list}")
    print(f"Reverse List:  {sort_list(number_list)}")

    done()

    print('18. Define a function called "is_vowel" that takes one parameter, "char", and returns True if the character is a vowel and False if it is not when called.')

    letter = choice("abcdefghijklmnopqrstuvwxyz")

    print(f"Is the letter \"{letter}\" a vowel?  {is_vowel(letter)}")

    done()

    print('19. Define a function called "string_length" that takes one parameter, "string", and returns the length of the string when called.')

    words = ["barrel", "abortion", "depart", "surround", "cassette", "triangle", "roof", "cheat", "red", "adult"]

    word = choice(words)

    print(f"The length of the string \"{word}\" is {string_length(word)}.")

    done()

    print('20. Define a function called "is_even_list" that takes one parameter, "nums", a list of numbers, and returns a list of Boolean values indicating whether each number in the input list is even or odd when called.')

    print(f"Original List: {number_list}")
    print(f"Result: {is_even_list(number_list)}")

    done()

    print('21. Define a function called "divisible_by_3" that takes one parameter, "nums", a list of numbers, and returns a list of all numbers in the input list that are divisible by 3 when called.')

    print(f"Original List: {number_list}")
    print(f"Result: {divisible_by_3(number_list)}")

    done()

    print('22. Define a function called "string_to_list" that takes one parameter, "string", and returns a list of the individual characters in the string when called.')

    word = choice(words)

    print(f"Original string: {word}")
    print(f"Result: {string_to_list(word)}")

    done()

    print('23. Define a function called "list_to_string" that takes one parameter, "lst", a list of characters, and returns the characters as a single string when called.')

    string_list = string_to_list(choice(words))

    print(f"Original List: {string_list}")
    print(f"Result: {list_to_string(string_list)}")

    done()

    print('24. Define a function called "remove_duplicates" that takes one parameter, "nums", a list of numbers, and returns the list with all duplicate elements removed when called.')

    number_list = [randint(0, 10) for x in range(4, randint(0, 10))]

    print(f"Original List: {number_list}")
    print(f"Result: {remove_duplicates(number_list)}")

    done()

    print('25. Define a function called "power_of_two" that takes one parameter, "num", and returns the number raised to the power of 2 when called.')

    number = randint(0, 100)

    print(f"Original number: {number}")
    print(f"Number squared:  {power_of_two(number)}")

    done()

main()
