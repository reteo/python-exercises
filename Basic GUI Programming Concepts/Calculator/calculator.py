# Entering Numbers and decimal point
def enter_number(entry_var, number):
    if entry_var.get() == "0":
        entry_var.set(str(number))
    else:
        entry_var.set(str(entry_var.get()) + str(number))


# Clear Calculation
def clear_calculation(entry_var, operation_var, answer_var):
    entry_var.set("0")
    operation_var.set("")
    answer_var.set("0")


# Operations
def calculate(entry, answer, operation):
    if answer.get() == "Error":
        return
    num1 = float(answer.get())
    num2 = float(entry.get())
    opchar = operation.get()
    if opchar == "+":
        answer.set(num1 + num2)
    if opchar == "-":
        answer.set(num1 - num2)
    if opchar == "×":
        answer.set(num1 * num2)
    if opchar == "÷":
        if num2 == 0.0:
            answer.set("Error")
        else:
            answer.set(num1 / num2)
    if opchar == "":
        answer.set = num1
    entry.set("")


def move_to_answer(entry_var, answer_var):
    answer_var.set(entry_var.get())
    entry_var.set("")


def is_empty(field):
    return len(field.get()) == 0


def debug_print(entry, operation, answer, opchar):
    ent_text = entry.get()
    ans_text = answer.get()
    op_text = operation.get()
    print(f"Debugging: {ent_text}, {ans_text}, {op_text}, {opchar}\n")


def opchar_blank(entry, operation, answer, opchar):
    # Assumes that the "submit" or "equals" button was pressed.

    if is_empty(entry):
        # entry=empty: do nothing
        return

    elif is_empty(answer):
        # entry=filled, answer=empty: move to answer, clear operation
        move_to_answer(entry, answer)
        operation.set("")
    elif is_empty(operation):
        # entry=filled, answer=filled, operation=empty: move to answer
        move_to_answer(entry, answer)
    else:
        # entry=filled, answer=filled, operation=filled: calculate
        calculate(entry, answer, operation)
        operation.set("")


def operation_blank(entry, operation, answer, opchar):
    # Assumes that there is an opchar, but not an existing operation.

    if is_empty(entry):
        if is_empty(answer):
            # entry=empty, answer=empty: do nothing
            return
        else:
            # entry=empty, answer=filled: set operation
            operation.set(opchar)
    elif is_empty(answer):
        # entry=filled, answer=empty: move to answer, set operation
        move_to_answer(entry, answer)
        operation.set(opchar)
    else:
        # entry=filled, answer=filled: set operation, calculate, set operation
        operation.set(opchar)
        calculate(entry, answer, operation)
        operation.set(opchar)


def operation_filled(entry, operation, answer, opchar):
    # Assumes that there is an opchar and an operation.
    
    if is_empty(entry):
        if is_empty(answer):
            # entry=empty, answer=empty: clear operation
            operation.set("")
        else:
            # entry=empty, answer=filled: set operation
            operation.set(opchar)
    elif is_empty(answer):
        # entry=filled, answer=empty: move to answer, set operation
        move_to_answer(entry, answer)
        operation.set(opchar)
    else:
        # entry=filled, answer=filled: calculate, set operation
        calculate(entry, answer, operation)
        operation.set(opchar)
    pass


def perform_operation(entry, operation, answer, opchar):
    if len(opchar) == 0:
        # If opchar is blank, continue to opchar_blank
        opchar_blank(entry, operation, answer, opchar)

    elif is_empty(operation):
        # Otherwise, if operation is blank, continue to
        # operation_blank
        operation_blank(entry, operation, answer, opchar)
    else:
        # Otherwise, if operation is not blank, continue to
        # operation_filled
        operation_filled(entry, operation, answer, opchar)
