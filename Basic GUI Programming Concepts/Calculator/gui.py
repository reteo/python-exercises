import tkinter as tk
import calculator as calc


class CalcluatorGui:
    """Graphical interface setup for the calculator."""

    def __init__(self, root: tk.Tk):
        root.title("Exercise Calculator")
        root.geometry("146x225")

        self.entry_var = tk.StringVar()  # The field holding the current number.
        self.operation_var = tk.StringVar()  # The field holding the current operation.
        self.answer_var = tk.StringVar()  # The field holding the previous number.

        self.entry_var.set("")

        # Creating three frames.
        self.frame_entry = tk.Frame(root)  # Holds the entry box
        self.frame_answer = tk.Frame(root)  # Holds the answer box
        self.frame_numpad = tk.Frame(root)  # Holds the number pad

        # The numpad will be a grid:
        self.frame_numpad.grid()

        ## Creating the GUI Objects.

        # Entry Field
        self.entry_field = tk.Entry(
            self.frame_entry, textvariable=self.entry_var, state="readonly"
        )
        self.operation_field = tk.Label(
            self.frame_entry, textvariable=self.operation_var
        )
        self.answer_field = tk.Entry(
            self.frame_answer, textvariable=self.answer_var, state="readonly"
        )

        # Digit Buttons
        # 1
        self.one_button = tk.Button(
            self.frame_numpad,
            text="1",
            command=lambda: calc.enter_number(self.entry_var, 1),
        )
        # 2
        self.two_button = tk.Button(
            self.frame_numpad,
            text="2",
            command=lambda: calc.enter_number(self.entry_var, 2),
        )
        # 3
        self.three_button = tk.Button(
            self.frame_numpad,
            text="3",
            command=lambda: calc.enter_number(self.entry_var, 3),
        )
        # 4
        self.four_button = tk.Button(
            self.frame_numpad,
            text="4",
            command=lambda: calc.enter_number(self.entry_var, 4),
        )
        # 5
        self.five_button = tk.Button(
            self.frame_numpad,
            text="5",
            command=lambda: calc.enter_number(self.entry_var, 5),
        )
        # 6
        self.six_button = tk.Button(
            self.frame_numpad,
            text="6",
            command=lambda: calc.enter_number(self.entry_var, 6),
        )
        # 7
        self.seven_button = tk.Button(
            self.frame_numpad,
            text="7",
            command=lambda: calc.enter_number(self.entry_var, 7),
        )
        # 8
        self.eight_button = tk.Button(
            self.frame_numpad,
            text="8",
            command=lambda: calc.enter_number(self.entry_var, 8),
        )
        # 9
        self.nine_button = tk.Button(
            self.frame_numpad,
            text="9",
            command=lambda: calc.enter_number(self.entry_var, 9),
        )
        # 0
        self.zero_button = tk.Button(
            self.frame_numpad,
            text="0",
            command=lambda: calc.enter_number(self.entry_var, 0),
        )

        # Decimal Button
        self.decimal_button = tk.Button(
            self.frame_numpad,
            text=".",
            command=lambda: calc.enter_number(self.entry_var, "."),
        )

        # Clear Button
        self.clear_button = tk.Button(
            self.frame_numpad,
            text="C",
            command=lambda: calc.clear_calculation(
                self.entry_var, self.operation_var, self.answer_var
            ),
        )

        # Operation Buttons
        # +
        self.plus_button = tk.Button(
            self.frame_numpad,
            text="+",
            command=lambda: calc.perform_operation(
                self.entry_var, self.operation_var, self.answer_var, "+"
            ),
        )
        # -
        self.minus_button = tk.Button(
            self.frame_numpad,
            text="-",
            command=lambda: calc.perform_operation(
                self.entry_var, self.operation_var, self.answer_var, "-"
            ),
        )
        # ×
        self.multiply_button = tk.Button(
            self.frame_numpad,
            text="×",
            command=lambda: calc.perform_operation(
                self.entry_var, self.operation_var, self.answer_var, "×"
            ),
        )
        # ÷
        self.divide_button = tk.Button(
            self.frame_numpad,
            text="÷",
            command=lambda: calc.perform_operation(
                self.entry_var, self.operation_var, self.answer_var, "÷"
            ),
        )

        # Submission Button (=)
        self.submit_button = tk.Button(
            self.frame_numpad,
            text="=",
            command=lambda: calc.perform_operation(
                self.entry_var, self.operation_var, self.answer_var, ""
            ),
        )

        ## Placement of Objects on the GUI

        # The entry frame should be placed
        self.frame_answer.grid(row=0)

        # The answer frame should be placed
        self.frame_entry.grid(row=1)

        # The number pad frame should be placed
        self.frame_numpad.grid(row=2)

        # The answer field
        self.answer_field.grid()

        # The entry and operation fields should be packed in the top frame.
        self.operation_field.grid(row=0)
        self.entry_field.grid(row=1)

        # The buttons will be applied in a grid in the bottom frame.
        # The layout will be as follows.  Note that +, =, and 0 are
        # doubled, as they will span rows/columns.

        # 0123

        # C÷×- 0
        # 789+ 1
        # 456+ 2
        # 123= 3
        # 00.= 4

        self.one_button.grid(row=3, column=0)
        self.two_button.grid(row=3, column=1)
        self.three_button.grid(row=3, column=2)
        self.four_button.grid(row=2, column=0)
        self.five_button.grid(row=2, column=1)
        self.six_button.grid(row=2, column=2)
        self.seven_button.grid(row=1, column=0)
        self.eight_button.grid(row=1, column=1)
        self.nine_button.grid(row=1, column=2)

        self.zero_button.grid(row=4, column=0, columnspan=2, ipadx=18)

        self.decimal_button.grid(row=4, column=2, ipadx=2)
        self.submit_button.grid(row=3, column=3, rowspan=2, ipady=15)
        self.clear_button.grid(row=0, column=0)

        self.plus_button.grid(row=1, column=3, rowspan=2, ipady=15)
        self.minus_button.grid(row=0, column=3, ipadx=2)
        self.multiply_button.grid(row=0, column=2)
        self.divide_button.grid(row=0, column=1)
