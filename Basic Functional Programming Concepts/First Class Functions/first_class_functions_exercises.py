def done():
    print("\n---\n")


# Here are some simple functions that can be used for the exercies:
def add_numbers(num_1, num_2):
    return num_1 + num_2


def multiply_numbers(num_1, num_2):
    return num_1 * num_2


def return_third(string_data):
    return string_data[2]


def number_test(number: int) -> int:
    result = None
    number_test = int(str(number)[-1])
    if number_test == 0:
        result = 9
    elif number_test == 1:
        result = 4
    elif number_test == 2:
        result = 1
    elif number_test == 3:
        result = 8
    elif number_test == 4:
        result = 5
    elif number_test == 5:
        result = 2
    elif number_test == 6:
        result = 0
    elif number_test == 7:
        result = 6
    elif number_test == 8:
        result = 3
    else:
        result = 7
    return result


# 1. Create a function apply_operation that takes a function and two
# numbers as arguments, and then applies the function to the two
# numbers.


def apply_operation(func, num_1, num_2):
    return func(num_1, num_2)


def exercise_1():
    print("Exercise_1")
    num_1 = 5
    num_2 = 10
    print(f"Adding {num_1} and {num_2}: {apply_operation(add_numbers, num_1, num_2)}")
    print(
        f"Multiplying {num_1} and {num_2}: {apply_operation(multiply_numbers, num_1, num_2)}"
    )
    done()


# 2. Write a function sort_strings that takes a list of strings and a
# function as arguments, and sorts the strings based on the provided
# function.
def sort_strings(func, string_list):
    return sorted(string_list, key=lambda item: func(item))


def exercise_2():
    # We can use return_third as the function to sort strings by.
    print("Exercise 2")
    words = ["finish", "calamari", "season", "cobblestone", "school"]
    print(f"Words: {words}")
    print(f"Sorted by 3rd letter: {sort_strings(return_third, words)}")
    done()


# 3. Create a function combine_lists that takes two lists, a function,
# and combines the elements of the lists using the provided function.
def combine_lists(func, list_1, list_2):
    return [func(a, b) for a, b in zip(list_1, list_2)]


def exercise_3():
    print("Exercise 3")
    list_1 = [2, 5, 3, 7, 4]
    list_2 = [5, 3, 4, 1, 2]
    print(f"Originals: {list_1} - {list_2}")
    print(f"Update: {combine_lists(add_numbers, list_1, list_2)}")
    done()


# 4. Write a function find_maximum that takes a list of numbers and a
# function, and returns the element with the highest result when
# passed through the function.
def find_maximum(func, number_list):
    result = None
    for number in number_list:
        if (result == None) or (func(number) > func(result)):
            result = number
    return result


def exercise_4():
    print("Exercise 4")
    number_list = [13, 32, 15, 91, 6, 18]
    print("Original List:", number_list)
    print("List Scores:", [number_test(number) for number in number_list])
    # 13 should be the highest.
    print("Maximum based on Score:", find_maximum(number_test, number_list))
    done()


# 5. Create a function filter_data that takes a list of dictionaries
# and a function, and filters the list based on the provided function.


# Use case: Suppose you have a list of dictionaries containing
# information about employees, with keys such as "name", "age",
# "salary", and "department". You could define a provided function
# named "filter_by_salary" that takes a dictionary as input and
# returns True if the employee's salary is greater than a certain
# amount (e.g., 50000), or False otherwise. This provided function
# could be used with the filter_data function to filter the list of
# employees based on their salaries.
def filter_data(func, data: list[dict], salary: float) -> list[dict]:
    return [item for item in data if func(item, salary)]


def filter_by_salary(data: dict, salary: float) -> dict:
    return data["salary"] >= salary


def exercise_5():
    print("Exercise 5")
    employee_records = [
        {"name": "John", "age": 41, "salary": 65720},
        {"name": "Emily", "age": 44, "salary": 50011},
        {"name": "Michael", "age": 20, "salary": 69035},
        {"name": "Jessica", "age": 34, "salary": 90879},
        {"name": "David", "age": 30, "salary": 61943},
    ]

    # We'll go for 65K

    salary = 65000

    print(
        "Employees with more that 70K:\n"
        + f"{filter_data(filter_by_salary, employee_records, salary)}"
    )
    done()


# 6. Write a function calculate_total that takes a list of prices and
# a tax calculation function, and calculates the total price with
# taxes applied.

import decimal


def calculate_total(prices: list[decimal.Decimal], func) -> decimal.Decimal:
    result = decimal.Decimal(sum(price + func(price) for price in prices))
    precision = decimal.Decimal("1").scaleb(-2)
    rounding_factor = decimal.ROUND_HALF_UP
    result = result.quantize(precision, rounding=rounding_factor)
    return result


def oh_tax(price: decimal.Decimal) -> decimal.Decimal:
    return price * decimal.Decimal(0.07)


def exercise_6():
    print("Exercise 6")
    prices_base = [36.23, 2.12, 99.13, 70.75, 5.49, 55.69, 14.32, 30.14, 95.99, 11.67]
    precision = decimal.Decimal("1").scaleb(-2)
    rounding_factor = decimal.ROUND_HALF_UP
    prices = [
        decimal.Decimal(price).quantize(precision, rounding=rounding_factor)
        for price in prices_base
    ]

    print(f"Prices are: {prices_base}")
    print(f"The base total is {sum(prices)}")
    print(f"The total, including 7% sales tax, is: {calculate_total(prices, oh_tax)}")

    done()


# 7. Create a function string_transform that takes a list of strings
# and a transformation function, and applies the transformation to
# each string in the list.


# Use case: Suppose you have a list of email addresses that all end
# with the same domain name, and you want to transform them so that
# the domain name is hidden. You could define a transformation
# function named "hide_domain" that takes a string as input and
# returns a modified string with the domain name replaced by
# "xxx". Then you could use the string_transform function to apply the
# "hide_domain" transformation to each email address in the list,
# resulting in a new list of modified email addresses with hidden
# domain names.
def string_transform(string_list: list[str], func) -> list[str]:
    return [func(item) for item in string_list]


def hide_domain(e_address: str) -> str:
    return e_address.replace("@domino.com", "")


def exercise_7():
    print("Exercise 7")
    emails = [
        "CoolCoder93@domino.com",
        "TravelBug87@domino.com",
        "Bookworm_Geek@domino.com",
        "StarrySky22@domino.com",
        "JavaJumper01@domino.com",
        "PixelPainter@domino.com",
        "NatureLover99@domino.com",
        "FitGuru_11@domino.com",
        "OceanExplorer@domino.com",
        "MysteryWriter18@domino.com",
    ]
    print("Email Address: Username")
    print(
        *(
            item_1 + ": " + item_2 + "\n"
            for (item_1, item_2) in zip(emails, string_transform(emails, hide_domain))
        )
    )
    done()


# 8. Write a function score_students that takes a list of students and
# a grading function, and assigns a score to each student using the
# grading function.
def grade_function(grade_list):
    return sum(grade_list) / len(grade_list)


def grade_students(students: list[dict], func):
    result = students.copy()
    for student in result:
        student["final"] = func(student["grades"])
    return result


def exercise_8():
    print("Exercise 8")
    students = [
        {"name": "Alice", "age": 20, "grades": [80, 75, 90, 85, 95]},
        {"name": "Bob", "age": 21, "grades": [70, 85, 75, 80, 90]},
        {"name": "Charlie", "age": 19, "grades": [90, 95, 85, 80, 75]},
        {"name": "David", "age": 18, "grades": [60, 70, 75, 80, 65]},
        {"name": "Eva", "age": 20, "grades": [95, 90, 80, 85, 90]},
    ]

    print("The final grades:")
    print(
        *(
            "\n" + student["name"] + ": " + str(student["final"])
            for student in grade_students(students, grade_function)
        )
    )
    done()


def main():
    exercise_1()
    exercise_2()
    exercise_3()
    exercise_4()
    exercise_5()
    exercise_6()
    exercise_7()
    exercise_8()


main()
