# pylint: disable = C0114, C0116

from functools import reduce  # This is part of the exercises.
from random import randint


def done():
    print("\n---\n")


# 1. Create a list of numbers and use the map() function to double
# each number in the list.
def exercise_1():
    print("Exercise 1")
    numlist = [5, 0, 2, 15, 3, 19, 2, 7, 6, 3]
    double_numlist = list(map(lambda number: number**2, numlist))

    print(f"Original Number List: {numlist}")
    print(f"Doubled Number List: {double_numlist}")
    done()


# 2. Create a list of strings and use the map() function to convert
# each string to uppercase.
def exercise_2():
    print("Exercise 2")
    stringlist = "this game is a sham".split(" ")
    upper_stringlist = list(map(lambda word: word.upper(), stringlist))
    print(f"Original String List: {stringlist}")
    print(f"Uppercase String List: {upper_stringlist}")
    done()


# 3. Create a list of tuples containing first and last names, and use
# the map() function to concatenate the first and last names with a
# space in between.
def exercise_3():
    print("Exercise 3")
    names = [
        ("George", "Washington"),
        ("Abraham", "Lincoln"),
        ("Thomas", "Jefferson"),
        ("Theodore", "Roosevelt"),
    ]
    name_list = list(map(lambda name: f"{name[0]} {name[1]}", names))
    reference_name_list = list(map(lambda name: f"{name[1]}, {name[0]}", names))
    print(f"List of name tuples: {names}")
    print(f"Name List: {name_list}")
    print(f"Reference List: {reference_name_list}")
    done()


# 4. Create a list of numbers and use the filter() function to filter
# out all the even numbers.
def exercise_4():
    print("Exercise 4")
    numberlist = [randint(0, 10) for _ in range(10)]
    evens = list(filter(lambda number: number % 2 == 0, numberlist))
    print(f"Number List: {numberlist}")
    print(f"Evens List: {evens}")
    done()


# 5. Create a list of strings and use the filter() function to filter
# out all the strings with a length greater than 5 characters.
def exercise_5():
    print("Exercise 5")
    string_list = "pangolin fee broaden elk to fine we learn".split(" ")
    filtered_strings = list(filter(lambda item: len(item) > 5, string_list))
    print(f"Strings: {string_list}")
    print(f"Filtered Strings: {filtered_strings}")
    done()


# 6. Create a list of tuples containing names and ages, and use the
# filter() function to filter out all the tuples where the age is less
# than 13.
def exercise_6():
    print("Exercise 6")
    goonies = [
        ("Mikey", 11),
        ("Brand", 15),
        ("Mouth", 12),
        ("Chunk", 11),
        ("Data", 11),
        ("Andy", 14),
        ("Steph", 13),
    ]
    older_goonies = list(filter(lambda person: person[1] >= 13, goonies))
    print(f"All the Goonies: {goonies}")
    print(f"Older Goonies: {older_goonies}")
    done()


# 7. Create a list of numbers and use the reduce() function to
# calculate the sum of all the numbers in the list.
def exercise_7():
    print("Exercise 7")
    numberlist = [randint(0, 10) for _ in range(10)]
    numberlist_sum = int(reduce(lambda total, number: total + number, numberlist))
    print(f"The sum of {numberlist} is {numberlist_sum}.")
    done()


# 8. Create a list of strings and use the reduce() function to
# concatenate all the strings in the list.
def exercise_8():
    print("Exercise 8")
    string_list = "this is a really long password".split(" ")
    run_on_string = str(reduce(lambda result, addition: result + addition, string_list))
    print(f"Original String List: {string_list}")
    print(f"Long Password: {run_on_string}")
    done()


# 9. Create a list of tuples containing product names and their
# prices, and use the reduce() function to calculate the total cost of
# all the products.
def exercise_9():
    print("Exercise 9")
    product_list = [
        ("Chocoli", 4.99),
        ("Meatna", 7.99),
        ("Struabri", 3.99),
        ("Motei", 10.99),
        ("Desri", 8.99),
    ]
    prices = [product[1] for product in product_list]
    total_sale = float(reduce(lambda total, add: total + add, prices))

    print(f"The products are {product_list}")
    print(f"The total price is ${total_sale}")
    done()


# 10. Use the map() and filter() functions together to remove odd
# numbers and double the even numbers in a list of numbers.
def exercise_10():
    print("Exercise 10")
    number_list = [randint(0, 20) for _ in range(10)]
    double_evens = list(
        map(
            lambda number: number**2,
            filter(lambda number: number % 2 == 0, number_list),
        )
    )
    print(f"Original Numbers: {number_list}")
    print(f"Doubled Evens: {double_evens}")
    done()


# 11. Use the filter() and reduce() functions together to calculate
# the product of all the odd numbers in a list of numbers.
def exercise_11():
    print("Exercise 11")
    numbers = [randint(0, 10) for _ in range(10)]
    product = int(
        reduce(
            lambda total, factor: total * factor,
            filter(lambda number: number % 2 != 0, numbers),
        )
    )
    print(f"Original Numbers: {numbers}")
    print(f"Odds' Product: {product}")
    done()


# 12. Use the map() and reduce() functions together to calculate the
# sum of the squares of all the numbers in a list of numbers.
def exercise_12():
    print("Exercise 12")
    numbers = [randint(0, 20) for _ in range(10)]
    square_sum = int(
        reduce(lambda total, add: total + add, map(lambda number: number**2, numbers))
    )
    print(f"Original Numbers: {numbers}")
    print(f"Squares' Sum: {square_sum}")
    done()


# 13. Create a list of dictionaries containing information about
# students, and use the map() function to extract the list of names of
# all the students.
def exercise_13():
    print("Exercise 13")
    students = [
        {"name": "Michael Davis", "age": 9, "score": 80},
        {"name": "Thomas Jackson", "age": 14, "score": 77},
        {"name": "Mack Mulligan", "age": 10, "score": 95},
        {"name": "Ava Nellison", "age": 16, "score": 92},
        {"name": "Siera Wright", "age": 14, "score": 86},
        {"name": "Stanley Nelson", "age": 13, "score": 65},
    ]
    student_names = list(map(lambda student: student["name"], students))

    print(f"The students are: {students}")
    print(f"Just their names: {student_names}")

    done()


# 14. Create a list of dictionaries containing information about
# students, and use the filter() function to filter out all the
# students who scored more than 80% in their exams.
def exercise_14():
    print("Exercise 14")
    students = [
        {"name": "Michael Davis", "age": 9, "score": 80},
        {"name": "Thomas Jackson", "age": 14, "score": 77},
        {"name": "Mack Mulligan", "age": 10, "score": 95},
        {"name": "Ava Nellison", "age": 16, "score": 92},
        {"name": "Siera Wright", "age": 14, "score": 86},
        {"name": "Stanley Nelson", "age": 13, "score": 65},
    ]
    high_scorers = list(filter(lambda student: student["score"] > 80, students))

    print(f"The students are: {students}")
    print(f"Just the ones with high scores: {high_scorers}")

    done()


# 15. Create a list of dictionaries containing information about
# students, and use the reduce() function to find the student with the
# highest score in their exams.
def exercise_15():
    print("Exercise 15")

    students = [
        {"name": "Michael Davis", "age": 9, "score": 80},
        {"name": "Thomas Jackson", "age": 14, "score": 77},
        {"name": "Mack Mulligan", "age": 10, "score": 95},
        {"name": "Ava Nellison", "age": 16, "score": 92},
        {"name": "Siera Wright", "age": 14, "score": 86},
        {"name": "Stanley Nelson", "age": 13, "score": 65},
    ]
    top_student = dict(
        reduce(
            lambda champion, challenger: challenger
            if challenger["score"] > champion["score"]
            else champion,
            students,
        )
    )

    print(f"The students are: {students}")
    print(f"The champion is: {top_student}")
    done()


# 16. Use the map() function to apply a custom function to a list of
# numbers that adds 5 to each even number and subtracts 3 from each
# odd number.
def exercise_16():
    print("Exercise 16")
    numbers = [randint(0, 10) for _ in range(10)]
    results = list(
        map(lambda number: number + 5 if number % 2 == 0 else number - 3, numbers)
    )
    print(f"Original Numbers: {numbers}")
    print(f"Number + 5 - 3: {results}")
    done()


# 17. Use the filter() function to filter out all the strings in a
# list that contain a specified substring.
def exercise_17():
    print("Exercise 17")

    stringlist = "peter piper picked a peck of pickled peppers".split(" ")
    substring = "pe"
    substring_filter = list(filter(lambda item: substring not in item, stringlist))

    print(f"Original String List: {stringlist}")
    print(f'Strings without "pe": {substring_filter}')

    done()


# 18. Use the reduce() function to find the longest string in a list
# of strings.
def exercise_18():
    print("Exercise 18")
    stringlist = "this is a fine situation you managed to finagle me into".split(" ")
    longest_word = str(
        reduce(
            lambda current, comer: comer if len(comer) > len(current) else current,
            stringlist,
        )
    )
    print(f"The words are: {stringlist}")
    print(f"The longest word is: {longest_word}")
    done()


# 19. Use the map() function to apply a custom function to a list of
# dictionaries that updates a specified key's value.
def age_up(person, years):
    result = person.copy()
    result["age"] = result["age"] + years
    return result


def exercise_19():
    print("Exercise 19")
    students = [
        {"name": "Michael Davis", "age": 9, "score": 80},
        {"name": "Thomas Jackson", "age": 14, "score": 77},
        {"name": "Mack Mulligan", "age": 10, "score": 95},
        {"name": "Ava Nellison", "age": 16, "score": 92},
        {"name": "Siera Wright", "age": 14, "score": 86},
        {"name": "Stanley Nelson", "age": 13, "score": 65},
    ]
    students_next_year = list(map(lambda student: age_up(student, 1), students))

    print(f"Students This Year: {students}")
    print(f"Students Next Year: {students_next_year}")
    done()


# 20. Use the filter() function to filter out all the dictionaries in
# a list that have a specified key-value pair.
def exercise_20():
    print("Exercise 20")
    students = [
        {"name": "Michael Davis", "age": 9, "score": 80},
        {"name": "Thomas Jackson", "age": 14, "score": 77},
        {"name": "Mack Mulligan", "age": 10, "score": 95},
        {"name": "Ava Nellison", "age": 16, "score": 92},
        {"name": "Siera Wright", "age": 14, "score": 86},
        {"name": "Stanley Nelson", "age": 13, "score": 65},
    ]
    no_fourteen = list(filter(lambda student: student["age"] != 14, students))

    print(f"Student List: {students}")
    print(f"Without 14-year-olds: {no_fourteen}")

    done()


def main():
    exercise_1()
    exercise_2()
    exercise_3()
    exercise_4()
    exercise_5()
    exercise_6()
    exercise_7()
    exercise_8()
    exercise_9()
    exercise_10()
    exercise_11()
    exercise_12()
    exercise_13()
    exercise_14()
    exercise_15()
    exercise_16()
    exercise_17()
    exercise_18()
    exercise_19()
    exercise_20()


main()
