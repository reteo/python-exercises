# pylint: disable=missing-module-docstring, missing-function-docstring


def done():
    print("\n---\n")


# 1. Write a closure that takes a number as input and returns a
# function that adds the input number to another number.
def make_adder(num_1):
    """Takes a number as input and returns a function that adds the
    input number to another number.

    """

    def adder(num_2):
        return num_1 + num_2

    return adder


def exercise_1():
    print("Exercise 1")
    add_2 = make_adder(2)
    print(add_2(3))  # Output 5
    done()


# 2. Write a closure that takes a number as input and returns a
# function that multiplies the input number by another number.
def make_multiplier(num_1):
    """Takes a number as input and returns a function that multiplies
    the input number to another number.

    """

    def multiplier(num_2):
        return num_1 * num_2

    return multiplier


def exercise_2():
    print("Exercise 2")
    multiply_2 = make_multiplier(2)
    print(multiply_2(3))  # Output 6
    done()


# 3. Write a closure that takes a string as input and returns a
# function that concatenates the input string with another string.
def make_concat(text_1):
    """Takes a string as input and returns a function that
    concatenates the input string with another string.

    """

    def concat(text_2):
        return text_1 + text_2

    return concat


def exercise_3():
    print("Exercise 3")
    prophecy = make_concat("In accordance with the prophecy, ")
    print(prophecy("chicken nuggets taste good."))
    # Output: "In accordance with the prophecy, chicken nuggets taste good."
    done()


# 4. Write a closure that takes a string as input and returns a
# function that returns True if the input string is a substring of
# another string.
def make_substring_test(match_string):
    """Takes a string as input and returns a function that returns
    True if the input string is a substring of another string.

    """

    def substring_test(test_string):
        return match_string.lower() in test_string.lower()

    return substring_test


def exercise_4():
    print("Exercise 4")
    match = "medium"
    sentences = [
        "This is a large problem",
        "This is a medium problem",
        "This is a small problem",
    ]
    compare = make_substring_test(match)
    print("Checking for 'medium'.")
    for sentence in sentences:
        print(sentence, f"- {'match' if compare(sentence) else 'no match'}")
    done()


# 5. Write a closure that takes a function as input and returns a
# function that applies the input function to a number and doubles the
# result.
def make_doubled_function(func):
    """Takes a function as input and returns a function that applies
    the input function to a number and doubles the result.

    """

    def doubled_function(number):
        return func(number) * 2

    return doubled_function


def exercise_5():
    print("Exercise 5")
    number = 10
    adder = make_adder(5)
    doubler = make_doubled_function(adder)
    print(doubler(number))
    done()


# 6. Write a closure that takes a function as input and returns a
# function that applies the input function to a number and squares the
# result.
def make_squared_function(func):
    """Takes a function as input and returns a function that applies
    the input function to a number and squares the result.

    """

    def doubled_function(number):
        return func(number) ** 2

    return doubled_function


def exercise_6():
    print("Exercise 6")
    number = 10
    adder = make_adder(5)
    squarer = make_squared_function(adder)
    print(squarer(number))
    done()


# 7. Write a closure that takes a function as input and returns a
# function that applies the input function to a number and negates the
# result.
def make_negative_function(func):
    """Takes a function as input and returns a function that applies
    the input function to a number and negates the result.

    """

    def negative_function(number):
        return func(number) * -1

    return negative_function


def exercise_7():
    print("Exercise 7")
    number = 10
    adder = make_adder(5)
    inverter = make_negative_function(adder)
    print(inverter(number))
    done()


# 8. Write a closure that takes a list of functions as input and
# returns a function that applies each input function to a number and
# returns a list of the results.
def make_process_list(func_list):
    """Takes a list of functions as input and returns a function that
    applies each input function to a number and returns a list of the
    results.

    """

    def procession(number):
        result = number
        for function in func_list:
            result = function(result)
        return result

    return procession


def exercise_8():
    print("Exercise 8")
    number = 10
    adder = make_adder(5)
    multiplier = make_multiplier(5)
    procession = make_process_list([adder, multiplier])
    print(procession(number))
    done()


# 9. Write a closure that takes a list of numbers as input and returns
# a function that calculates the sum of the input numbers and another
# number.
def make_sum_plus(number_list):
    """Takes a list of numbers as input and returns a function that
    calculates the sum of the input numbers and another number."""

    def sum_plus(additional_number):
        return sum(number_list) + additional_number

    return sum_plus


def exercise_9():
    print("Exercise 9")
    number_list = [10, 5, 15, 25]
    additional_number = 3
    sum_plus = make_sum_plus(number_list)
    print(sum_plus(additional_number))
    done()


# 10. Write a closure that takes a list of numbers as input and
# returns a function that calculates the product of the input numbers
# and another number.
def make_product_times(number_list):
    """Takes a list of numbers as input and returns a function that
    calculates the product of the input numbers and another number.

    """

    def product_times(additional_number):
        result = None
        for number in number_list:
            result = number if result is None else result * number
        return result * additional_number

    return product_times


def exercise_10():
    print("Exercise 10")
    number_list = [1, 3, 5]
    additional_number = 3
    product_times = make_product_times(number_list)
    print(product_times(additional_number))
    done()


# 11. Write a closure that takes an integer n as input and returns a
# function that checks whether another number is divisible by n.
def make_factor_check(factor_value):
    def factor(number):
        if number == 0:
            return False
        return number % factor_value == 0

    return factor


def exercise_11():
    print("Exercise 11")
    number_list = list(range(0, 11)[::2])
    divisible_by_5 = make_factor_check(5)

    for number in number_list:
        print(
            f"{number} {'is divisible' if divisible_by_5(number) else 'is not divisible'} by 5"
        )
    done()


# 12. Write a closure that takes a number as input and returns a
# function that calculates the power of the input number raised to
# another number.
def make_power_test(number):
    def power_test(exponent):
        return number**exponent

    return power_test


def exercise_12():
    print("Exercise 12")
    number = 3
    power_test = make_power_test(number)
    exponent = 2
    print(f"{number} with an exponent of {exponent}: {power_test(exponent)}")
    exponent = 3
    print(f"{number} with an exponent of {exponent}: {power_test(exponent)}")
    done()


# 13. Write a closure that takes a string as input and returns a
# function that counts the occurrences of the input string in another
# string.
def make_string_check(main_string):
    def count_substring(substring):
        return sum(
            1 for i in range(len(main_string)) if main_string.startswith(substring, i)
        )

    return count_substring


def exercise_13():
    print("Exercise 13")
    main_string = "Now is the time for all good men to come to the aid of the party!"
    string_search = make_string_check(main_string)
    print(f"Sentence: {main_string}")
    substring = "e"
    print(
        f'Number of instances of the substring "{substring}"'
        + f" in the sentence: {string_search(substring)}"
    )
    substring = "me"
    print(
        f'Number of instances of the substring "{substring}"'
        + f" in the sentence: {string_search(substring)}"
    )
    done()


# 14. Write a closure that takes a string as input and returns a
# function that replaces all occurrences of the input string in
# another string with a specified replacement string.


def make_string_replacer(substring: str):
    def string_replacer(main_string: str, replacement: str):
        return main_string.replace(substring, replacement)

    return string_replacer


def exercise_14():
    print("Exercise 14")
    substring = "party"
    main_string = "Now is the time for all good men to come to the aid of the party!"
    replacement = "team"

    replacing = make_string_replacer(substring)

    print(f"Original Sentence: {main_string}")
    print(f"Replacement Sentence: {replacing(main_string, replacement)}")

    done()


# 15. Write a closure that takes an integer n as input and returns a
# function that returns the first n elements of a given list.
def make_list_head(head_count):
    def list_head(main_list):
        return main_list[:head_count]

    return list_head


def exercise_15():
    print("Exercise 15")
    word_list = [
        "apple",
        "banana",
        "cherry",
        "dog",
        "elephant",
        "fish",
        "grape",
        "house",
        "ice cream",
        "jacket",
    ]
    list_head = make_list_head(3)
    print(f"Word List: {word_list}")
    print(f"First Three Words: {list_head(word_list)}")
    done()


# 16. Write a closure that takes an integer n as input and returns a
# function that returns the last n elements of a given list.
def make_list_tail(tail_count):
    def list_tail(main_list):
        return main_list[-(tail_count):]

    return list_tail


def exercise_16():
    print("Exercise 16")
    word_list = [
        "apple",
        "banana",
        "cherry",
        "dog",
        "elephant",
        "fish",
        "grape",
        "house",
        "ice cream",
        "jacket",
    ]
    list_tail = make_list_tail(3)
    print(f"Word List: {word_list}")
    print(f"Last Three Words: {list_tail(word_list)}")
    done()


# 17. Write a closure that takes an integer n as input and returns a
# function that skips the first n elements of a given list and returns
# the rest.
def make_front_skip(skip_number):
    def rest_of_list(word_list):
        return word_list[skip_number:]

    return rest_of_list


def exercise_17():
    print("Exercise 17")
    word_list = [
        "apple",
        "banana",
        "cherry",
        "dog",
        "elephant",
        "fish",
        "grape",
        "house",
        "ice cream",
        "jacket",
    ]
    skip_3 = make_front_skip(3)
    print(f"Word List: {word_list}")
    print(f"Skipping First Three Words: {skip_3(word_list)}")
    done()


# 18. Write a closure that takes an integer n as input and returns a
# function that checks whether one number is within n units of a
# second number.
def make_number_range_test(distance):
    def within_range(num_1, num_2):
        return num_2 - distance <= num_1 <= num_2 + distance

    return within_range


def exercise_18():
    print("Exercise 18")
    range_num = 5
    base_num = 11
    specific_num = 7
    range_test = make_number_range_test(range_num)
    print(
        f"Is {specific_num} within {range_num} of {base_num}? "
        + f"{'Yes' if range_test(base_num, specific_num) else 'No'}"
    )
    base_num = 13
    specific_num = 7
    print(
        f"Is {specific_num} within {range_num} of {base_num}? "
        + f"{'Yes' if range_test(base_num, specific_num) else 'No'}"
    )
    base_num = 12
    specific_num = 7
    print(
        f"Is {specific_num} within {range_num} of {base_num}? "
        + f"{'Yes' if range_test(base_num, specific_num) else 'No'}"
    )
    done()


# 19. Write a closure that takes a number as input and returns a
# function that rounds another number to the input number of decimal
# places.
def make_precision_rounding(decimals):
    def round_number(number):
        return round(float(number), decimals)

    return round_number


def exercise_19():
    print("Exercise 19")
    precision = 2  # Currency Precision
    round_currency = make_precision_rounding(precision)
    total = 1.321039218842
    print(f"Original number: {total}\nRounded number: {round_currency(total)}\n")
    precision = 3  # Government Tax Precision
    round_currency = make_precision_rounding(precision)
    total = 1.321039218842
    print(f"Original number: {total}\nRounded number: {round_currency(total)}")
    done()


# 20. Write a closure that takes a dictionary as input and returns a
# function that updates the input dictionary with another dictionary.
def make_dictionary_concatenate(dict_addendum: dict):
    def dict_concat(dictionary: dict):
        new_dict = dictionary.copy()
        for key, value in dict_addendum.items():
            new_dict[key] = value
        return new_dict

    return dict_concat


def exercise_20():
    print("Exercise 20")
    users = {"Name": "John", "Age": 20}
    users_add = {"Email": "john@somewhere.else"}
    dict_addendum = make_dictionary_concatenate(users_add)
    print(f"New Record: {dict_addendum(users)}")
    done()


# 21. Write a closure that takes a set as input and returns a function
# that updates the input set with another set.
def make_set_combine(update_set: set):
    def set_combine(origin_set: set):
        return origin_set.union(update_set)

    return set_combine


def exercise_21():
    print("Exercise 21")
    set_1 = {"apple", "orange", "banana"}
    set_2 = {"pear", "peach", "cherry"}
    set_combine = make_set_combine(set_2)
    print(f"{set_1} + {set_2}")
    print(f"Becomes: {set_combine(set_1)}")
    done()


# 22. Write a closure that takes a list as input and returns a
# function that updates the input list with another list.
def make_list_combine(update_list: list):
    def list_combine(origin_list: list):
        temp_list = origin_list.copy()
        temp_list.extend(update_list)
        return temp_list

    return list_combine


def exercise_22():
    print("Exercise 22")
    list_1 = ["GM", "Ford", "Dodge"]
    list_2 = ["Toyota", "Hyundai", "Nissan"]
    list_combine = make_list_combine(list_2)
    print(f"{list_1} + {list_2}")
    print(f"Becomes: {list_combine(list_1)}")
    done()


def main():
    exercise_1()
    exercise_2()
    exercise_3()
    exercise_4()
    exercise_5()
    exercise_6()
    exercise_7()
    exercise_8()
    exercise_9()
    exercise_10()
    exercise_11()
    exercise_12()
    exercise_13()
    exercise_14()
    exercise_15()
    exercise_16()
    exercise_17()
    exercise_18()
    exercise_19()
    exercise_20()
    exercise_21()
    exercise_22()


main()
