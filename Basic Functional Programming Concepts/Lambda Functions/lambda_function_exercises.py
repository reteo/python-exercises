# pylint: disable = C0114, C0116, C3001, W0108
from functools import reduce


def done():
    print("\n---\n")


# 1. Create a lambda function that takes two numbers as input and
# returns their sum.
def exercise_1():
    print("Exercise 1")
    lsum = lambda num_1, num_2: num_1 + num_2

    print(f"4 + 5 = {lsum(4,5)}")
    done()


# 2. Write a lambda function that takes a number as input and returns
# its square.
def exercise_2():
    print("Exercise 2")
    lsquare = lambda num_1: num_1**2

    print(f"3 squared = {lsquare(3)}")
    done()


# 3. Create a lambda function that takes a string as input and returns
# its length.
def exercise_3():
    print("Exercise 3")
    llen = lambda str_1: len(str_1)

    print(f"The size of \"beneath\" is {llen('beneath')}")
    done()


# 4. Write a lambda function that takes a list of numbers and returns
# the maximum value in the list.


# I think using max() would be cheating.  Using reduce() instead.
def exercise_4():
    print("Exercise 4")
    lcomp = lambda num1, num2: num1 if num1 > num2 else num2
    lmax = lambda numlist: reduce(lambda result, comp: lcomp(result, comp), numlist)

    print(f"The maximum value in [1, 6, 3, 4] is {lmax([1,6,3,4])}")
    done()


# 5. Create a lambda function that takes a list of numbers and returns
# the average value of the list.
def exercise_5():
    print("Exercise 5")
    lavg = lambda numlist: sum(numlist) / len(numlist)

    print(f"The avertage value of [1, 6, 3, 4] is {lavg([1, 6, 3, 4])}")
    done()


# 6. Write a lambda function that takes a string and returns the
# string in reverse order.
def exercise_6():
    print("Exercise 6")
    lreverse = lambda data: data[::-1]

    print(f"When you reverse \"mirror,\" you get \"{lreverse('mirror')}.\"")
    done()


# 7. Create a lambda function that takes two strings as input and
# concatenates them.
def exercise_7():
    print("Exercise 7")
    lconcat = lambda data_1, data_2: data_1 + data_2

    print(
        f"When you combine \"speed\" and \"way\", you get \"{lconcat('speed', 'way')}.\""
    )
    done()


# 8. Write a lambda function that takes a list of strings and returns
# a list of the lengths of those strings.
def exercise_8():
    print("Exercise 8")
    lsize_strings = lambda stringlist: [len(item) for item in stringlist]

    string_list = "something wicked this way comes".split(" ")
    print(f"The sizes for the words {string_list} are {lsize_strings(string_list)}")
    done()


# 9. Create a lambda function that takes a list of strings and returns
# a list of the strings sorted alphabetically.
def exercise_9():
    print("Exercise 9")
    lalpha_sort = lambda stringlist: sorted(stringlist)

    string_list = "something wicked this way comes".split(" ")
    print(f'The words "{string_list}" can be sorted as "{lalpha_sort(string_list)}."')
    done()


# 10. Write a lambda function that takes a list of numbers and returns
# a list of the even numbers in the list.
def exercise_10():
    print("Exercise 10")
    leven = lambda numlist: [num for num in numlist if num % 2 == 0]

    num_list = [5, 2, 6, 8, 7, 3]
    print(f'the even numbers in "{num_list}" are "{leven(num_list)}."')
    done()


# 11. Create a lambda function that takes a list of numbers and
# returns a list of the odd numbers in the list.
def exercise_11():
    print("Exercise 11")
    lodd = lambda numlist: [num for num in numlist if num % 2 != 0]

    num_list = [5, 2, 6, 8, 7, 3]
    print(f'the odd numbers in "{num_list}" are "{lodd(num_list)}."')
    done()


# 12. Write a lambda function that takes a list of strings and returns
# a list of the strings sorted by length.
def exercise_12():
    print("Exercise 12")
    lsort = lambda stringlist: sorted(stringlist, key=lambda item: len(item))

    string_list = (
        "now is the time for all good people to come to the aid of the party".split(" ")
    )
    print(
        "The list to sort by length is:\n"
        + f"{string_list}\n"
        + "And the sorted list is:\n"
        + f"{lsort(string_list)}"
    )
    done()


# 13. Create a lambda function that takes a list of dictionaries and
# returns a list of the dictionaries sorted by a specified key.
def exercise_13():
    print("Exercise 13")
    lsort = lambda data: sorted(data, key=lambda datum: datum["age"])
    people = [
        {"name": "Alice", "age": 25},
        {"name": "Bob", "age": 30},
        {"name": "Charlie", "age": 20},
        {"name": "David", "age": 28},
        {"name": "Eva", "age": 22},
        {"name": "Frank", "age": 35},
        {"name": "Grace", "age": 29},
        {"name": "Henry", "age": 27},
        {"name": "Ivy", "age": 24},
        {"name": "Jack", "age": 31},
    ]

    print(*(f"\n{person['name']}: age {person['age']}" for person in lsort(people)))

    done()


# 14. Write a lambda function that takes a list of tuples and returns
# a list of the tuples sorted by a specified index.
def exercise_14():
    print("Exercise 14")
    lsort = lambda data: sorted(data, key=lambda datum: datum[1])
    coordinates = [(1, 5), (3, 2), (-2, 4), (0, 0), (6, 9), (-5, -3)]

    print("Coordinates sorted by vertical value:")
    print(*(f"\n{datum}" for datum in lsort(coordinates)))
    done()


# 15. Create a lambda function that takes two lists of numbers and
# returns a new list containing the element-wise sum of the input
# lists.
def exercise_15():
    print("Exercise 15")
    lesum = lambda list1, list2: [num1 + num2 for num1, num2 in zip(list1, list2)]
    list_1 = [27, 5, 49, 7, 23]
    list_2 = [54, 63, 98, 50, 49]

    print(f"First Number List: {list_1}")
    print(f"Second Number List: {list_2}")
    print(f"Lists Added Together: {lesum(list_1, list_2)}")
    done()


# 16. Write a lambda function that takes a list of numbers and a
# number N, and returns a list of the N largest numbers in the input
# list.
def exercise_16():
    print("Exercise 16")
    llargest = lambda numlist, qty: sorted(numlist, reverse=True)[0:qty]

    numbers = [84, 81, 1, 10, 97, 50, 84, 94, 23, 70, 100, 65, 27, 28, 65]
    print(f"Numbers: {numbers}\nLargest 5: {llargest(numbers, 5)}\n")
    done()


# 17. Create a lambda function that takes a list of numbers and a
# number N, and returns a list of the N smallest numbers in the input
# list.
def exercise_17():
    print("Exercise 17")
    lsmallest = lambda numlist, qty: sorted(numlist)[0:qty]

    numbers = [84, 81, 1, 10, 97, 50, 84, 94, 23, 70, 100, 65, 27, 28, 65]
    print(f"Numbers: {numbers}\nSmallest 5: {lsmallest(numbers, 5)}\n")
    done()


# 18. Write a lambda function that takes a list of numbers and a
# number N, and returns a list of the numbers in the input list raised
# to the power of N.
def exercise_18():
    print("Exercise 18")
    llistpow = lambda numlist, exp: list(map(lambda number: number**exp, numlist))

    num_list = [89, 10, 95, 83, 78]
    power = 2
    print(f"The squares of {num_list} are {llistpow(num_list, power)}")
    done()


# 19. Create a lambda function that takes a list of strings and a
# string S, and returns a list of the strings in the input list that
# start with the string S.
def exercise_19():
    print("Exercise 19")
    lstartswith = lambda stringlist, string_s: [
        item for item in stringlist if item.startswith(string_s)
    ]

    string_list = "simple simon met a pieman going to the fair".split(" ")
    starting = "sim"
    print(f"The string list is {string_list}.")
    print(
        f"The words starting with '{starting}' are {lstartswith(string_list, starting)}"
    )
    done()


# 20. Write a lambda function that takes a list of strings and a
# string S, and returns a list of the strings in the input list that
# end with the string S.
def exercise_20():
    print("Exercise 20")
    lendswith = lambda stringlist, string_s: [
        item for item in stringlist if item.endswith(string_s)
    ]

    string_list = "peter piper picked a peck of pickled peppers".split(" ")
    ending = "er"
    print(f"The string list is {string_list}.")
    print(f"The words ending with '{ending}' are {lendswith(string_list, ending)}")

    done()


# 21. Create a lambda function that takes a list of strings and
# returns a list of the strings in the input list that are
# palindromes.
def exercise_21():
    print("Exercise 21")
    lpalindrome = lambda stringlist: list(
        filter(lambda word: word == word[::-1], stringlist)
    )
    word_list = [
        "level",
        "radar",
        "python",
        "madam",
        "kayak",
        "java",
        "racecar",
        "civic",
        "perl",
        "rotor",
    ]
    print(f"The words are {word_list}")
    print(f"The palindromes are {lpalindrome(word_list)}")
    done()


# 22. Write a lambda function that takes a list of strings and a
# character C, and returns a list of the strings in the input list
# that contain the character C.
def exercise_22():
    print("Exercise 22")
    lstringcontains = lambda stringlist, char_c: list(
        filter(lambda datum: char_c in datum, stringlist)
    )
    stringlist = "she sells sea shells by the seashore".split(" ")
    char_c = "s"

    print(f"String List: {stringlist}")
    print(f"Strings with 's': '{lstringcontains(stringlist, char_c)}'")
    done()


# 23. Create a lambda function that takes a list of strings and a
# number N, and returns a list of the strings in the input list that
# have a length greater than or equal to N.
def exercise_23():
    print("Exercise 23")
    llengthhead = lambda wordlist, size: list(
        filter(lambda word: len(word) >= size, wordlist)
    )

    stringlist = "five feet high the door and three may walk abreast".split(" ")
    print(f"Original list: {stringlist}")
    print(f"Words size 4 or greater: {llengthhead(stringlist, 4)}")
    print(f"Words size 5 or greater: {llengthhead(stringlist, 5)}")
    done()


# 24. Write a lambda function that takes a list of numbers and returns
# a list of the numbers in the input list that are prime.
def exercise_24():
    print("Exercise 24")
    is_prime = lambda value: [
        i for i in range(2, value) if value % i == 0
    ] == [] and not 0 <= value < 2

    prime_list = lambda numlist: list(filter(is_prime, numlist))

    numlist = list(range(0, 21))
    print(f"Number List: {numlist}")
    print(f"Prime List: {prime_list(numlist)}")

    done()


def main():
    exercise_1()
    exercise_2()
    exercise_3()
    exercise_4()
    exercise_5()
    exercise_6()
    exercise_7()
    exercise_8()
    exercise_9()
    exercise_10()
    exercise_11()
    exercise_12()
    exercise_13()
    exercise_14()
    exercise_15()
    exercise_16()
    exercise_17()
    exercise_18()
    exercise_19()
    exercise_20()
    exercise_21()
    exercise_22()
    exercise_23()
    exercise_24()


main()
