from typing import Union, Callable
import string
from math import sqrt


def done():
    print("\n---\n")


def number_place(num: int) -> str:
    if str(num)[-1] == "1":
        return "st"
    elif str(num)[-1] == "2":
        return "nd"
    elif str(num)[-1] == "3":
        return "rd"
    else:
        return "th"


# When working with numbers, the calculation may or may not include
# floating point numbers.  Best not take chances.
number = Union[int, float]


# 1. Define a pure function add() that takes two numbers as arguments
# and returns their sum.
def add(num_1: number, num_2: number) -> number:
    """Adds two numbers together."""
    return num_1 + num_2


def exercise_1():
    print("Exercise 1")
    print(f"1 + 3 = {add(1, 3)}")
    done()


# 2. Define a pure function subtract() that takes two numbers as
# arguments and returns the result of subtracting the second number
# from the first.
def subtract(num_1: number, num_2: number) -> number:
    """Subtracts the second number from the first."""
    return num_1 - num_2


def exercise_2():
    print("Exercise 2")
    print(f"4 - 3 = {subtract(4, 3)}")
    done()


# 3. Define a pure function multiply() that takes two numbers as
# arguments and returns their product.
def multiply(num_1: number, num_2: number) -> number:
    """Multiplies two numbers."""
    return num_1 * num_2


def exercise_3():
    print("Exercise 3")
    print(f"5 × 3 = {multiply(5, 3)}")
    done()


# 4. Define a pure function divide() that takes two numbers as
# arguments and returns the result of dividing the first number by the
# second. Handle the case where the second number is 0.
def divide(num_1: number, num_2: number) -> number:
    """Divides the first number by the second."""
    return num_1 / num_2


def exercise_4():
    print("Exercise 4")
    print(f"9 ÷ 3 = {divide(9, 3)}")
    print(f"5 ÷ 2 = {divide(5, 2)}")
    done()


# 5. Define a pure function is_even() that takes a number as an
# argument and returns True if the number is even and False otherwise.
def is_even(num: number) -> bool:
    """Returns True if the number is even, False otherwise."""
    return num % 2 == 0


def exercise_5():
    print("Exercise 5")
    for x in (4, 5):
        print(f"{x} is {'even' if is_even(x) else 'odd'}")
    done()


# 6. Define a pure function is_odd() that takes a number as an
# argument and returns True if the number is odd and False otherwise.
def is_odd(num: number) -> bool:
    """Returns true if the number is odd, False otherwise."""
    return num % 2 != 0


def exercise_6():
    print("Exercise 6")
    for x in (4, 5):
        print(f"{x} is {'odd' if is_odd(x) else 'even'}")
    done()


# 7. Define a pure function square() that takes a number as an
# argument and returns its square.
def square(num: number) -> number:
    """Returns the square of a number."""
    return num**2


def exercise_7():
    print("Exercise 7")
    for x in (4, 5):
        print(f"{x} squared is {square(x)}")
    done()


# 8. Define a pure function cube() that takes a number as an argument
# and returns its cube.
def cube(num: number) -> number:
    """Returns the cube of a number."""
    return num**3


def exercise_8():
    print("Exercise 8")
    for x in (2, 3):
        print(f"{x} cubed is {cube(x)}")
    done()


# 9. Define a pure function power() that takes a number and an
# exponent as arguments and returns the number raised to the exponent.
def power(num: number, exp: number) -> number:
    """Returns the power of a number using a specified exponent."""
    return num**exp


def exercise_9():
    print("Exercise 9")
    for x, y in [(1, 1), (4, 2), (5, 3), (3, 4)]:
        print(f"{x} to the {y}{number_place(y)} power is {power(x, y)}")
    done()


# 10. Define a pure function factorial() that takes a number as an
# argument and returns its factorial.
def factorial(num: number) -> number:
    """Returns a factorial of the specified number."""
    if num == 0:
        return 1
    return num * factorial(num - 1)


def exercise_10():
    print("Exercise 10")
    print(f"The factorial for 5 is {factorial(5)}")
    done()


# 11. Define a pure function is_prime() that takes a number as an
# argument and returns True if the number is prime and False
# otherwise.
def is_prime(num: int) -> bool:
    """Returns True if a number is a prime, False otherwise."""
    if num == 0 or num == 1:
        return False
    for prime in range(2, num - 1):
        factor = num / prime
        if factor == int(factor):
            return False
    return True


def exercise_11():
    print("Exercise 11")
    for i in range(0, 19):
        print(f"{i}: {'Prime' if is_prime(i) else 'Not prime'}")
    done()


# 12. Define a pure function gcd() that takes two numbers as arguments
# and returns their greatest common divisor.
def gcd(num_1: int, num_2: int) -> int:
    """Returns the greatest common denominator between two numbers."""
    if num_1 == 0:
        return num_2
    if num_2 == 0:
        return num_1
    return gcd(num_2, num_1 % num_2)


def exercise_12():
    print("Exercise 12")
    for x, y in [(12, 40), (15, 25), (180, 150)]:
        print(f"The greatest common denominator of {x} and {y} is {gcd(x, y)}")
    done()


# 13. Define a pure function lcm() that takes two numbers as arguments
# and returns their least common multiple.
def lcm(num_1: int, num_2: int) -> int:
    """Returns the least common multiple between two numbers."""
    return (num_1 * num_2) // gcd(num_1, num_2)


def exercise_13():
    print("Exercise 13")
    for x, y in [(12, 40), (15, 25), (180, 150)]:
        print(f"The least common multiple of {x} and {y} is {lcm(x, y)}")
    done()


# 14. Define a pure function sum_list() that takes a list of numbers
# as an argument and returns the sum of all elements in the list.
def sum_list(number_list: list[number]) -> number:
    """Returns the sum of a list of numbers."""
    list_sum = 0
    for num in number_list:
        list_sum += num
    return list_sum


def exercise_14():
    print("Exercise 14")
    number_list = [15, 10, 5, 25]
    print(f"The number list of {number_list} totals {sum_list(number_list)}")
    done()


# 15. Define a pure function find_max() that takes a list of numbers
# as an argument and returns the maximum value in the list.
def find_max(number_list: list[number]) -> number:
    """Returns the highest number in a list."""
    current_max = None
    for num in number_list:
        if current_max == None:
            current_max = num
        if num > current_max:
            current_max = num
    if current_max == None:
        current_max = 0
    return current_max


def exercise_15():
    print("Exercise 15")
    numlist = [10, 2, -5, 12, 32, 1]
    print(f"The highest number in {numlist} is {find_max(numlist)}")
    done()


# 16. Define a pure function find_min() that takes a list of numbers
# as an argument and returns the minimum value in the list.
def find_min(number_list: list[number]) -> number:
    """Returns the lowest number in a list."""
    current_min = None
    for num in number_list:
        if current_min == None:
            current_min = num
        if num < current_min:
            current_min = num
    if current_min == None:
        current_min = 0
    return current_min


def exercise_16():
    print("Exercise 16")
    numlist = [10, 2, -5, 12, 32, 1]
    print(f"The lowest number in {numlist} is {find_min(numlist)}")
    done()


# 17. Define a pure function average() that takes a list of numbers as
# an argument and returns the average of all elements in the list.
def average(number_list: list[number]) -> number:
    """Returns the average from a list of numbers."""
    return sum_list(number_list) / len(number_list)


def exercise_17():
    print("Exercise 17")
    numlist = [10, 2, -5, 12, 32, 1]
    print(f"The average of {numlist} is {average(numlist)}")
    done()


# 18. Define a pure function reverse_string() that takes a string as
# an argument and returns the string reversed.
def reverse_string(string_data: str) -> str:
    """Return the string in reverse."""
    return string_data[::-1]


def exercise_18():
    print("Exercise 18")
    print(f"'Mirror' written in reverse is {reverse_string('mirror')}")
    done()


# 19. Define a pure function count_vowels() that takes a string as an
# argument and returns the number of vowels in the string.
def count_vowels(string_data: str) -> int:
    """Counts the number of vowels in a string."""
    vowel_count = 0
    for letter in string_data:
        if letter in "aeiou":
            vowel_count += 1
    return vowel_count


def exercise_19():
    print("Exercise 19")
    sentence = "This is the way we wash our socks."
    print(f"'{sentence}': {count_vowels(sentence)} vowels")
    done()


# 20. Define a pure function is_palindrome() that takes a string as an
# argument and returns True if the string is a palindrome and False
# otherwise.
def is_palindrome(string_data: str) -> int:
    """Returns True if a string is a palindrome, False otherwise."""
    data = string_data.lower()
    for i in range(0, len(data)):
        if data[i] != data[-(i + 1)]:
            return False
    return True


def exercise_20():
    print("Exercise 20")
    words = ["bob", "time", "level", "racecar", "toyota"]
    for word in words:
        print(
            f"{word} {'is a palindrome' if is_palindrome(word) else 'is not a palindrome'}"
        )
    done()


# 21. Define a pure function fibonacci() that takes a positive integer
# n as an argument and returns the nth Fibonacci number.
def fibonacci(num: int) -> int:
    """Calculate the fibonacci sequence to a specific level."""
    if num == 0:
        return 0
    if num == 1:
        return 1
    return fibonacci(num - 1) + fibonacci(num - 2)


def exercise_21():
    print("Exercise 21")
    print(f"The 8th element in the Fibonacci Sequence is {fibonacci(8)}")
    done()


# 22. Define a pure function is_anagram() that takes two strings as
# arguments and returns True if the strings are anagrams of each other
# and False otherwise.
def is_anagram(data_1: str, data_2: str) -> bool:
    """Returns True if the two strings are anagrams of one another; False otherwise."""
    letters_1 = sorted(data_1.lower())
    letters_2 = sorted(data_2.lower())

    if letters_1 == letters_2:
        return True
    else:
        return False


def exercise_22():
    print("Exercise 22")
    words = [
        ("listen", "silent"),
        ("medal", "metal"),
        ("race", "care"),
        ("angel", "angle"),
        ("sea", "see"),
    ]
    for x, y in words:
        print(
            f"{x} and {y} {'are anagrams' if is_anagram(x, y) else 'are not anagrams'}"
        )
    done()


# 23. Define a pure function count_words() that takes a string as an
# argument and returns a dictionary with the count of each word in the
# string.
def count_words(data: str) -> int:
    """Returns the number of words in a string."""
    return len(data.split(" "))


def exercise_23():
    print("Exercise 23")
    sentence = "This is the time for all good men to come to the aid of the party."
    print(f"{sentence}: {count_words(sentence)} words.")
    done()


# 24. Define a pure function map_function() that takes a function and
# a list as arguments and returns a new list containing the result of
# applying the function to each element in the list.
def map_function(fun: Callable, data: list) -> list:
    """Applies a specified function to each element of a list."""
    return [fun(item) for item in data]


def exercise_24():
    print("Exercise 24")
    data = [
        "serendipity",
        "luminescence",
        "ponderous",
        "mellifluous",
        "quandary",
        "esoteric",
    ]
    print(f"Original list: {data}")
    print(f"Reversed list: {map_function(reverse_string, data)}")
    done()


# 25. Define a pure function filter_function() that takes a function
# and a list as arguments and returns a new list containing the
# elements of the original list for which the function returns True.
def filter_function(fun: Callable, data: list) -> list:
    """Filters a list based on the results of a specifed filter function."""
    result = []
    for item in data:
        if not fun(item):
            continue
        result.append(item)
    return result


def exercise_25():
    print("Exercise 25")
    numbers = [i for i in range(0, 20)]
    print(f"Primes from 0 to 20: {filter_function(is_prime, numbers)}")


def main():
    exercise_1()

    exercise_2()

    exercise_3()

    exercise_4()

    exercise_5()

    exercise_6()

    exercise_7()

    exercise_8()

    exercise_9()

    exercise_10()

    exercise_11()

    exercise_12()

    exercise_13()

    exercise_14()

    exercise_15()

    exercise_16()

    exercise_17()

    exercise_18()

    exercise_19()

    exercise_20()

    exercise_21()

    exercise_22()

    exercise_23()

    exercise_24()

    exercise_25()


main()
