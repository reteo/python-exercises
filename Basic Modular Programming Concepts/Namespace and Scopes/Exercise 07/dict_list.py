"""dict_list.py:

   Create a module with a function called my_func that takes a
   dictionary as an argument and returns the values of the dictionary
   as a list. Import the module into a new script and use the my_func
   function to convert a dictionary to a list.

"""

def dict_list(data: dict) -> list:
    return [ v for  v in data.values()]
