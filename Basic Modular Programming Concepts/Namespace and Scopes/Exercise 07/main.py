"""main.py:

   Create a module with a function called my_func that takes a
   dictionary as an argument and returns the values of the dictionary
   as a list. Import the module into a new script and use the my_func
   function to convert a dictionary to a list.

"""

import dict_list

def main():
    person = {
        "name": "Jack",
        "age": 14,
        "color": "blue"
    }

    print(dict_list.dict_list(person))

main()
