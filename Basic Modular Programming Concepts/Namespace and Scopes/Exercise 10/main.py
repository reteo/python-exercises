"""main.py:

   Create a module with a function called my_func that takes a string
   as an argument and returns the number of vowels in the
   string. Import the module into a new script and use the my_func
   function to find the number of vowels in a string.

"""

import vowel_count

def main():
    sentence = "This is a a particularly long sentence with a very large number of words."
    print(vowel_count.my_func(sentence))

main()
