"""vowel_count.py:

   Create a module with a function called my_func that takes a string
   as an argument and returns the number of vowels in the
   string. Import the module into a new script and use the my_func
   function to find the number of vowels in a string.

"""

def my_func(data: str) -> int:
    return sum(1 for letter in data if letter in "aeiou")
