"""main.py:

   Create a module with a function called my_func that takes a string
   as an argument and returns the string in reverse.  Import the
   module into a new script and use the my_func function to reverse a
   string.

"""

def my_func(data: str) -> str:
    return data[::-1]
