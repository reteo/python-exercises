"""getter_module.py:

   Create a module with a variable called my_var. Create a function in
   the same module that prints the value of my_var.  Import the module
   into a new script and call the function.

"""

my_var = "Output of my_var."

def my_getter():
    print(my_var)
