"""greetings.py:

   Create a module with a function called my_func that prints "Hello,
   World!" to the console. Import the module into a new script and
   call the my_func function.

"""

def greeting():
    return "Hello, World!"
