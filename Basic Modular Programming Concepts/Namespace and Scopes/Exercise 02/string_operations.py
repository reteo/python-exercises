"""string_operations.py:

   Create a function in a module that takes a string as an argument
   and returns the length of the string. Import the module into a new
   script and use the function to find the length of a string.

"""

def string_length(data: str) -> int:
    """Returns the length of a string."""
    return len(data)
