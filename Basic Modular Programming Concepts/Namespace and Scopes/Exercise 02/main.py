"""main.py:

   Create a function in a module that takes a string as an argument
   and returns the length of the string. Import the module into a new
   script and use the function to find the length of a string.

"""

import string_operations

def main():
    sentence = "This is a string."
    print(string_operations.string_length(sentence))

main()
