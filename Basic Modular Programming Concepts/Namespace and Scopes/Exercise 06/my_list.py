"""my_list.py:

   Create a module with a function called my_func that takes a list as
   an argument and returns the sum of the list. Import the module into
   a new script and use the my_func function to find the sum of a
   list.

"""

def my_func(data: list[int]) -> int:
    return sum(data)
