"""main.py:

   Create a module with a function called my_func that takes a list as
   an argument and returns the sum of the list. Import the module into
   a new script and use the my_func function to find the sum of a
   list.

"""

import random
import my_list

def main():
    numlist = [random.randint(0, 9) for _ in range(0, random.randint(4, 10))]
    print(my_list.my_func(numlist))

main()
