"""main.py:

   Create a new module with a variable called my_var. Import the
   module into a new script and print the value of my_var.

"""

import variable_value

def main():
    print(variable_value.my_var)

main()
