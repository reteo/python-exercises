"""variable_value.py:

   Create a new module with a variable called my_var. Import the
   module into a new script and print the value of my_var.

"""

my_var = "my example variable"
