"""main.py:

   Create a module with a function called my_func that takes two
   numbers as arguments and returns the product of the two numbers.
   Import the module into a new script and use the my_func function to
   find the product of two numbers.

"""

import product_operation

def main():
    print(product_operation.my_func(10, 5))

main()
