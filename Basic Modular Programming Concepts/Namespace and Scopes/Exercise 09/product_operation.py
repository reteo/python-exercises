"""product_operation.py:

   Create a module with a function called my_func that takes two
   numbers as arguments and returns the product of the two numbers.
   Import the module into a new script and use the my_func function to
   find the product of two numbers.

"""

def my_func(num_a: int, num_b: int) -> int:
    """Return the product of two numbers."""
    return num_a * num_b
