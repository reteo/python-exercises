"""main.py:

   Create a module with a class called Person that has an __init__
   method which takes a name and age as arguments and sets them as
   instance variables.  Import the module into a new script and create
   an instance of the Person class.

"""

import person_operations

def main():
    person = person_operations.Person("Jack", 30)

    print(person.name, person.age)

main()
