"""person_operations.py:

   Create a module with a class called Person that has an __init__
   method which takes a name and age as arguments and sets them as
   instance variables.  Import the module into a new script and create
   an instance of the Person class.

"""

class Person:
    def __init__(self, name: str, age: int):
        self.name = name
        self.age = age
