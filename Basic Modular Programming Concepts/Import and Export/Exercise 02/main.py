"""main.py: main loop for calculator module code"""

import calculator

def main():
    x = 6
    y = 3

    print(calculator.addition(x, y))
    print(calculator.subtraction(x, y))
    print(calculator.multiplication(x, y))
    print(calculator.division(x, y))
    print(calculator.modulus(x, y))
    print(calculator.exponent(x, y))

main()
