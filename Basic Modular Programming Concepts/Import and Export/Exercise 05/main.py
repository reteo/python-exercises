"""main.py:

Create a module named "string_operations" that defines functions for
manipulating strings like "reverse_string" and
"count_occurrences". Import this module in another Python script and
use the functions to manipulate strings.

"""

import string_operations

def main():
    sentence = "Peter Piper picked a peck of pickled peppers"
    print(string_operations.reverse_string(sentence))
    print(string_operations.count_occurrences(sentence, "pi"))

main()
