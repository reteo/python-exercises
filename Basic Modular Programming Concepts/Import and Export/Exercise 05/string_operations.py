"""string_operations.py:

Create a module named "string_operations" that defines functions for
manipulating strings like "reverse_string" and
"count_occurrences". Import this module in another Python script and
use the functions to manipulate strings.

"""

def reverse_string(in_string: str) -> str:
    return in_string[::-1]

def count_occurrences(in_string: str, to_match: str) -> int:
    work_string = in_string.lower()
    return sum(1 for i in range(len(work_string))
               if work_string.startswith(to_match, i))
