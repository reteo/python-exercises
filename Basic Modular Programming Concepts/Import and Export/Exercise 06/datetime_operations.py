""""datetime_operations.py:

Create a module named "email_operations" that defines functions for
sending emails using the built-in "smtplib" module. Import this module
in another Python script and use the functions to send an email.

"""

import datetime

def make_datetime(year: int, month: int, day: int) -> datetime.datetime:
    return datetime.datetime(year, month, day)

def get_current_date():
    return datetime.datetime.now()

def calculate_time_difference(first_time: datetime.datetime, second_time: datetime.datetime) -> datetime.timedelta:
    return second_time - first_time if second_time > first_time else first_time - second_time
