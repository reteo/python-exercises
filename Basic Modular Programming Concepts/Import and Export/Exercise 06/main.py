""""main.py:

Create a module named "email_operations" that defines functions for
sending emails using the built-in "smtplib" module. Import this module
in another Python script and use the functions to send an email.

"""

import random

import datetime_operations

def main():
    first_date = datetime_operations.make_datetime(random.randint(2018, 2023),
                                                   random.randint(1, 12),
                                                   random.randint(0, 28))
    second_date = datetime_operations.make_datetime(random.randint(2018, 2023),
                                                   random.randint(1, 12),
                                                   random.randint(0, 28))
    
    print(datetime_operations.get_current_date())
    print(first_date, second_date)
    print(datetime_operations.calculate_time_difference(first_date, second_date))

main()
