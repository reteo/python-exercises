"""main.py:

Create a module named "random_generator" that defines a function for
generating random numbers. Import this module in another Python script
and use the function to generate a random number.

"""

import random_generator

def main():
    print(random_generator.random_number(10))

main()
