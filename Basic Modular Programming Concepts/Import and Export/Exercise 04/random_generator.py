"""random_generator.py:

Create a module named "random_generator" that defines a function for
generating random numbers. Import this module in another Python script
and use the function to generate a random number.

"""

import random

def random_number(limit):
    """Generates an integer between 0 and the specified limit."""
    return random.randint(0, limit)
