"""encryption_operations.py:

   Create a module named "encryption_operations" that defines
   functions for encrypting and decrypting data using the built-in
   "hashlib" module. Import this module in another Python script and
   use the functions to encrypt and decrypt data.

"""

import hashlib

def encrypt_data(data, key):
    pass

def decrypt_data(data, key):
    pass
