'''main.py:

Create a module named "my_module" that defines a function named
"greet" that takes a name parameter and returns a greeting
string. Import this module in another Python script and use the
"greet" function to greet yourself.

'''

import my_module

def main():
    name = input("Enter your name: ")
    print(my_module.greet(name))

main()
