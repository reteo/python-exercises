"""my_module.py:

Create a module named "my_module" that defines a function named
"greet" that takes a name parameter and returns a greeting
string. Import this module in another Python script and use the
"greet" function to greet yourself.

"""

def greet(name: str) -> str:
    return f"Hello, {name}!"
