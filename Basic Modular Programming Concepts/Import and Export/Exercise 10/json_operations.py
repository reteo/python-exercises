"""json_operations.py:

   Create a module named "json_operations" that defines functions for
   working with JSON data like "parse_json" and
   "serialize_json". Import this module in another Python script and
   use the functions to work with JSON data.

"""

import json

def serialize_json(data):
    pass

def parse_json(data):
    pass
