"""url_operations.py:

  Create a module named "url_operations" that defines functions for
  working with URLs like "parse_url" and "build_url". Import this
  module in another Python script and use the functions to work with
  URLs.

"""

import urllib
import bs4 as beautifulsoup

def build_url():
    pass

def parse_url():
    pass
