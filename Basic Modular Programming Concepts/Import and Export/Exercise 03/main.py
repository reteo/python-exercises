"""main.py:

Create a module named "file_operations" that defines functions for
reading and writing files. Import this module in another Python script
and use the functions to read and write data to a file.

"""

import file_operations

FILENAME = "file.txt"
SENTENCE = "Peter Piper picked a peck of pickled peppers."

def main():
    with open(FILENAME, "w") as fopen:
        file_operations.write_file(fopen, SENTENCE)

    with open(FILENAME, "r") as fopen:
        print(file_operations.read_file(fopen))

main()
