"""file_operations.py: defines functions for reading and writing files."""

def read_file(file_object):
    """Read data from the specified file object."""
    return file_object.read()

def write_file(file_object, data):
    """Write the data to the specified file object."""
    file_object.write(data)
