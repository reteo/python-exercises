"""email_operations.py:

Create a module named "email_operations" that defines functions for
sending emails using the built-in "smtplib" module. Import this module
in another Python script and use the functions to send an email.

"""

# Ignored for now; did not study smtplib yet.

import smtplib

def send_email(title: str, message: str):
    pass
