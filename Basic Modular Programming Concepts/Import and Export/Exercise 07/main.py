"""main.py:

Create a module named "email_operations" that defines functions for
sending emails using the built-in "smtplib" module. Import this module
in another Python script and use the functions to send an email.

"""

import email_operations

# Ignored for now; did not study smtplib yet.

def main():
    pass

main()
