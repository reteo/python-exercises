"""main.py: Exercise 07

   Write a program that uses the 'math' module to calculate the area
   and circumference of a circle with a given radius, and then prints
   out the results.

"""

import math

def enter_radius():
    while True:
        try:
            radius = float(input("Enter Radius: "))
        except ValueError:
            print("Please enter a number.")
        else:
            return radius

def main():
    radius = enter_radius()

    print("Area:", math.pi * (radius ** 2))
    print("Circumference:", math.pi * radius * 2)

main()
