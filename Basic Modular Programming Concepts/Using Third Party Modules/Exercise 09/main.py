"""main:py -

   Write a program that uses the 'collections' module to count the
   number of occurrences of each word in a text file, and then prints
   out the top 10 most common words and their frequencies.

"""
import string
import collections

def strip_punctuation(word: str) -> str:
    """Remove the punctuation from the specified word."""
    # We'll use the filter function to return anything not punctuation or digits.
    return "".join(list(filter(lambda letter: letter not in string.punctuation + string.digits, word)))

def make_lowercase(word: str) -> str:
    return word.lower()

def process_wordlist(func: callable, wordlist: list[str]):
    return list(map(lambda word: func(word), wordlist))

def main():
    # Read the document.
    with open("dolphins.txt") as fopen:
        text = fopen.read()

    # Split the words into a list.
    words = text.split(" ")

    # Make all words lowercase and remove punctuation.
    words = process_wordlist(make_lowercase, process_wordlist(strip_punctuation, words))
    
    # Count each word
    counts = collections.Counter(words)

    # Print the top 10 results.
    print(counts.most_common(10))
    
main()
