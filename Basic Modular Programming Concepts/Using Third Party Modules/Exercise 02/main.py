"""main.py:

   Write a program that uses the 'datetime' module to print the
   current date and time in a readable format.

"""

import datetime

def main():
    print(datetime.datetime.now().strftime("%A, %B %d, %Y at %H:%m %p"))

main()
