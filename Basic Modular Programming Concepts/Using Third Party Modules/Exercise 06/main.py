"""main:py - Exercise 06

   Write a program that uses the 're' module to search a text file for
   all instances of a particular word or phrase, and prints out the
   number of times it occurs.

"""

import re

def main():
    dolphin_count = 0

    with open("dolphins.txt", "r") as fopen:
        for line in fopen.readlines():
            dolphin_matches = re.findall("[Dd]olphin", line)
            dolphin_count += len(dolphin_matches)

    print(f"In the file \"dolphins.txt,\" The word \"dolphin\" appears {dolphin_count} times.")
            
main()
