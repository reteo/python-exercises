"""main.py:

   Write a program that uses the 'time' module to calculate how many
   seconds are in a day, and prints out the result.

"""

# The 'time' module does not really support calculating the number of
# a seconds in a day.  Nor is it needed.

SECONDS_PER_MINUTE = (60)
MINUTES_PER_HOUR = (60)
HOURS_PER_DAY = (24)

def main():
    print(f"There are {SECONDS_PER_MINUTE * MINUTES_PER_HOUR * HOURS_PER_DAY} seconds in a day.")

main()
