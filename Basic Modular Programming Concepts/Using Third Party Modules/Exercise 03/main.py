"""main.py:

   Write a program that uses the 'os' module to list all the files in
   a directory, and then prints out the file names and sizes in a
   human-readable format.

"""

import os

def get_size(item: str) -> int:
    return os.path.getsize(item)
    
def main():
    path = "."
    directory = os.listdir (path)

    print("Debugging:", directory)

    for item in directory:
        name = item
        size = get_size(item)

        print(name, size)

main()
