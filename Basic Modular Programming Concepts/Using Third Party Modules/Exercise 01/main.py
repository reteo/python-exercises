"""main.py:

   Write a program that uses the 'random' module to generate a random
   number between 1 and 10, and then asks the user to guess the
   number. The program should tell the user if their guess was too
   high or too low, and continue asking for guesses until the correct
   number is guessed.

"""

import random

def get_guess(guesses: int) -> int:
    """Allows the player to make their guess and makes sure it's a number."""
    while True:
        try:
            guess = int(input(f"Choose a number from 1 to 10.  You have {guesses} {tries(guesses)} left: "))
        except ValueError:
            print("Please enter a number from 1 to 10.")
        else:
            return guess

def tries(count: int) -> str:
    """Pick the correct inflection of the word 'try' based on the number remaining."""
    if count == 1:
        return "try"
    else:
        return "tries"

def main():
    """Main function."""
    number = random.randint(1, 10)
    guesses = 3
    
    while guesses > 0:
        guess = get_guess(guesses)
        guesses -= 1
        if guess == number:
            print("You got it!")
            break

        if guesses == 0:
            print(f"Sorry, the number was {number}.")
            break
        
        if guess < number:
            print(f"Higher.  You have {guesses} {tries(guesses)} left.")
        elif guess > number:
            print(f"Lower.  You have {guesses} {tries(guesses)} left.")
main()
