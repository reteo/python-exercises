"""main.py - Exercise 08

   Write a program that uses the 'json' module to read a JSON file
   containing a list of people and their ages (in years), and then
   calculates and prints out the average age.

"""

import json

def main():
    with open("people.json", "r") as fopen:
        PEOPLE_STRING = fopen.read()
        people = json.loads(PEOPLE_STRING)

    ages_list = [person["age"] for person in people]

    print(f"The average age is {sum(ages_list)/len(ages_list)}.")
    

main()
