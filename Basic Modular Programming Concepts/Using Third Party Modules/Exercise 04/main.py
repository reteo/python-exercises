"""main.py:

   Write a program that uses the 'csv' module to read a CSV file
   containing student grades (with columns for student name and
   grade), and then calculates the average grade and prints it out.

"""

import csv

def main():
    filename = "grades.csv"

    student_grades = []
    
    with open("grades.csv", "r", newline = '') as fopen:
        reader = csv.reader(fopen)
        heading = False
        for row in reader:
            if heading == False: # Skip the headers
                heading = True
                continue
            name = row[0]
            grade = row[1]

            student_grades.append((name, grade))

    grades = list(map(lambda x: int(x[1]), student_grades))

    print("Debugging:", grades)
    print(sum(grades)/len(grades))
            
main()
