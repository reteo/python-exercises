"""init.py: initialize the package"""

import my_package

def main():
    # testing the add() function in math_utils
    num1 = 2
    num2 = 7
    print(my_package.math_utils.add(num1, num2))

    ## testing the reverse() function in string_utils
    sentence = "this is a collection of words"
    print(my_package.string_utils.reverse(sentence))

    # testing the
    print(my_package.date_utils.get_current_date())

main()
