"""string_utils.py: manage strings"""

def reverse(data: str) -> str:
    return data[::-1]
