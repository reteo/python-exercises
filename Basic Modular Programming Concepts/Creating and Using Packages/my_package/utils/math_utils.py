"""math_utils.py: Mathematical utilities"""

import typing

number = typing.Union[int, float]

def add(num1: number, num2: number) -> number:
    solution = num1 + num2
    if solution == int(solution):
        return int(solution)
    else:
        return solution

def multiply(num1: number, num2: number) -> number:
    solution = num1 * num2
    if solution == int(solution):
        return int(solution)
    else:
        return solution
