"""date_utils.py: utilities covering dates"""

import datetime

def get_current_date():
    return datetime.datetime.now()
