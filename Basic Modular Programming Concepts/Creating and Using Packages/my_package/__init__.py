"""__init__.py: configure the package"""

__name__ = "my_package"

from .utils import math_utils
from .utils import string_utils
from .utils import date_utils
