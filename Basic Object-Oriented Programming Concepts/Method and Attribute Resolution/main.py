"""Main Loop for Exercises in Method and Attribute Resolution"""

from letters import F, H, I, K, M, N, P, FTwo


def done():
    print("\n---\n")


def main():

    print(
        "Instantiate class F and call the display() method to demonstrate method resolution order in multiple inheritance.\n"
    )

    current = F()

    current.display()

    done()

    print(
        "Modify class F to inherit from classes E and D in this order, and call the display() method again to observe the change in method resolution order.\n"
    )

    current = FTwo()

    current.display()

    done()

    print(
        "Instantiate class H, call the set_color() method to set a color, and then call the get_color() method to demonstrate attribute resolution.\n"
    )

    color = H()

    print(color.get_color())
    color.set_color("Green")
    print(color.get_color())

    done()

    print(
        "Instantiate multiple instances of class I and call the increment_count() method on each instance to observe the behavior of class-level attributes.\n"
    )

    counter_1 = I()
    counter_2 = I()
    counter_3 = I()

    for i in range(4, 51):
        if i % 5 == 0:
            counter_1.increment_count()
        if i % 7 == 0:
            counter_2.increment_count()
        if i % 11 == 0:
            counter_3.increment_count()

    print("Counter 1:", counter_1.count)
    print("Counter 2:", counter_2.count)
    print("Counter 3:", counter_3.count)

    done()

    print(
        "Modify the increment_count() method in class I to be a class method and observe the change in behavior\n"
    )

    counter_1 = I()
    counter_2 = I()
    counter_3 = I()

    for i in range(4, 51):
        if i % 5 == 0:
            I.increment_count(counter_1)
        if i % 7 == 0:
            I.increment_count(counter_2)
        if i % 11 == 0:
            I.increment_count(counter_3)

    print("Counter 1:", counter_1.count)
    print("Counter 2:", counter_2.count)
    print("Counter 3:", counter_3.count)

    done()

    print(
        "Instantiate class K, call the set_size() method to set a size, and then call the get_size() method to demonstrate attribute resolution.\n"
    )

    item = K()

    item.set_size(15)

    print(item.get_size())

    done()

    print("Instantiate class M and call the sum() and product() methods to demonstrate method resolution for static methods.\n")

    num1 = 10
    num2 = 5

    print(M.sum(num1, num2))
    print(M.product(num1, num2))

    done()

    print("Instantiate class N, use the data property to set and get the value of the _data attribute, and demonstrate attribute resolution for properties.\n")

    data_object = N()

    print(data_object.data)

    data_object.data = 512
    
    print(data_object.data)
    
    done()

    print("Instantiate class P and call the call_helper() method to observe method resolution order with private methods.\n")

    helper = P()

    helper.call_helper()

    done()


main()
