""" Letter Classes for Exercises"""

# --- Part 1 --- #


class A:
    def display(self):
        print("lass A")


class B(A):
    def display(self):
        print("Class B")


class C(B):
    pass


class D(A):
    def display(self):
        print("Class D")


class E(A):
    def display(self):
        print("Class E")


class F(D, E):
    pass


class FTwo(E, D):
    pass


# --- Part 2 --- #


class G:
    color = "Blue"

    def get_color(self):
        return self.color


class H(G):
    def set_color(self, color):
        self.color = color


# --- Part 3 --- #


class I:
    def __init__(self):
        self.count = 0

    def increment_count(self):
        self.count += 1


class ITwo:
    count = 0

    @classmethod
    def increment_count(cls):
        cls.count += 1


class J:
    size = 0

    def get_size(self):
        return self.size


class K(J):
    def set_size(self, size):
        self.size = size


class L:
    @staticmethod
    def sum(num1, num2):
        return num1 + num2

    
class M(L):
    @staticmethod
    def product(num1, num2):
        return num1 * num2


class N:
    _data = 1024

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, value):
        self._data = value


class O:
    def _helper(self):
        print("Helper method")

    def call_helper(self):
        self._helper()


class P(O):
    def _helper(self):
        print("Overridden helper method")
