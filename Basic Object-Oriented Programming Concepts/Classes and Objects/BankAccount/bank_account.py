"""bank_account.py: Bank Account

   1. Create a class called BankAccount with attributes
   account_number, balance, and owner. Instantiate this class with
   your own details.

   2. Add a method called deposit() to the BankAccount class that
   takes an amount as an argument and updates the balance.

   3. Add a method called withdraw() to the BankAccount class that
   takes an amount as an argument and updates the balance, but first
   checks if the withdrawal amount is less than or equal to the
   current balance.

"""

# When working with finance, it is essential to take every step to
# make sure that the numbers are accurate and precise.  The decimal
# module is designed with this goal in mind.

# Note: we want the interface to use floating-point numbers, but any
# internal calculations should be performed using decimal objects.

import decimal

from main import two_decimals

# Get the precision settings set.
decimal.getcontext().prec = 10
decimal.getcontext().rounding = decimal.ROUND_HALF_UP

class BankAccount:
    def __init__(self, account_number: str, balance: float, owner: str):
        self.account_number = account_number
        self.owner = owner
        self.balance = decimal.Decimal(balance) # Make the balance a
                                                # decimal object.
                                                
    def round_amount(self, amount):
        precision = decimal.Decimal('1').scaleb(-2) # -2 = 2 decimal places
        rounding_factor = decimal.ROUND_HALF_UP
        return (decimal.Decimal(amount).quantize(precision, rounding = rounding_factor))

    def get_balance(self):
        #return float(self.balance)
        return self.round_amount(self.balance)

    def get_owner(self):
        return self.owner

    def deposit(self, amount):
        self.balance += decimal.Decimal(amount)

    def withdraw(self, amount):
        withdrawal = decimal.Decimal(amount)
        if withdrawal > self.balance:
            return False
        else:
            self.balance -= withdrawal
            return True
