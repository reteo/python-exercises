"""main.py: Main loop

   1. Create a class called BankAccount with attributes
   account_number, balance, and owner. Instantiate this class with
   your own details.

   2. Add a method called deposit() to the BankAccount class that
   takes an amount as an argument and updates the balance.

   3. Add a method called withdraw() to the BankAccount class that
   takes an amount as an argument and updates the balance, but first
   checks if the withdrawal amount is less than or equal to the
   current balance.

"""

import decimal
import bank_account

def round_amount(amount):
    precision = decimal.Decimal('1').scaleb(-2) # -2 = 2 decimal places
    rounding_factor = decimal.ROUND_HALF_UP
    return (decimal.Decimal(amount).quantize(precision, rounding = rounding_factor))

def withdraw(account, amount):
    if account.withdraw(amount) == True:
        return round_amount(decimal.Decimal(amount))
    else:
        return 0

def deposit(account, amount):
    account.deposit(amount)
    return amount

def main():
    account = bank_account.BankAccount("UA-111", 0, "Lampros Liontos")
    on_hand = decimal.Decimal(1500)

    on_hand -= deposit(account, 1500)
    print(account.get_balance())
    on_hand += withdraw(account, 300)    
    print(on_hand, account.get_balance())
    on_hand += withdraw(account, 250.55)
    print(on_hand, account.get_balance())
    on_hand += withdraw(account, 1500)
    print(on_hand, account.get_balance())

main()
