"""main.py: main loop

   1. Create a class called Rectangle with attributes length and width. Instantiate this class with the dimensions of your choice.

   2. Add a method to the Rectangle class to calculate the area of the rectangle using the formula: area = length * width.

   3. Add a method to the Rectangle class to calculate the perimeter of the rectangle using the formula: perimeter = 2 * (length + width).

"""

import rectangle

def main():
    a = rectangle.Rectangle(5, 10)

    print(a)
    print("Perimeter:", a.perimeter())
    print("Area:", a.area())

main()
