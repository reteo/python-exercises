"""rectangle.py: Rectangle Class

   1. Create a class called Rectangle with attributes length and
   width. Instantiate this class with the dimensions of your choice.

   2. Add a method to the Rectangle class to calculate the area of the
   rectangle using the formula: area = length * width.

   3. Add a method to the Rectangle class to calculate the perimeter
   of the rectangle using the formula: perimeter = 2 * (length +
   width).

"""

class Rectangle:
    def __init__(self, length, width):
        self.length = length
        self.width = width

    def area(self):
        return self.length * self.width

    def perimeter(self):
        return 2 * (self.length + self.width)
