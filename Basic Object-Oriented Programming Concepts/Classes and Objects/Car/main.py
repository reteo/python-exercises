"""main.py: main loop

1. Create a class called Car with attributes make, model, year, and
mileage. Instantiate this class with the details of your choice.

2. Add a method called drive() to the Car class that takes a distance
as an argument and updates the mileage.

"""

import car

def main():
    vehicle = car.Car("Toyota", "Camry", 2008, 150000)

    print(vehicle.get_year(), vehicle.get_make(), vehicle.get_model())
    print("Miles Before Trip:", vehicle.get_mileage())
    vehicle.drive(1500)
    print("Miles After Trip:", vehicle.get_mileage())

main()

