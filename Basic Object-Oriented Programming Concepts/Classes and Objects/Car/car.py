"""car.py: Car class

1. Create a class called Car with attributes make, model, year, and
mileage. Instantiate this class with the details of your choice.

2. Add a method called drive() to the Car class that takes a distance
as an argument and updates the mileage.

"""

class Car:
    def __init__(self, make: str, model: str, year: int, mileage: float):
        self.make = make
        self.model = model
        self.year = year
        self.mileage = mileage

    def get_make(self):
        return self.make

    def get_model(self):
        return self.model

    def get_year(self):
        return self.year

    def get_mileage(self):
        return self.mileage

    def drive(self, distance):
        self.mileage += distance
