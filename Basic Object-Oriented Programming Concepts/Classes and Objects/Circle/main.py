"""Circle.py: Circle Class

   1. Implement a class called Circle with a single attribute called
      radius.

   2. Add a method to the Circle class to calculate the area of the
      circle using the formula: area = π * (radius ^ 2).

   3. Add a method to the Circle class to calculate the circumference
      of the circle using the formula: circumference = 2 * π * radius.

"""

import circle

a = circle.Circle(1) # A unit circle

print(a)
print("Area:", a.area())
print("Circumference:", a.circumference())
