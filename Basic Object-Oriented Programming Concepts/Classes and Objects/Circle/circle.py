"""Circle.py: Circle Class

   1. Implement a class called Circle with a single attribute called
      radius.

   2. Add a method to the Circle class to calculate the area of the
      circle using the formula: area = π * (radius ^ 2).

   3. Add a method to the Circle class to calculate the circumference
      of the circle using the formula: circumference = 2 * π * radius.

"""

import math

class Circle:
    def __init__(self, radius):
        self.radius = radius

    def area(self):
        return math.pi * (self.radius ** 2)

    def circumference(self):
        return math.pi * self.radius * 2
