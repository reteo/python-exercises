"""person.py: Person class

   1. Create a simple class called Person with attributes name, age, and gender. Instantiate this class with your own details.

   2. Add a method called introduce_yourself() to the Person class that prints the name, age, and gender of the person.

"""

class Person:
    def __init__(self, name: str, age: int, gender: str):
        self.name = name
        self.age = age
        self.gender = gender

    # Getters

    def get_name(self) -> str:
        return self.name

    def get_age(self) -> int:
        return self.age

    def get_gender(self) -> str:
        return self.gender

    # Setters
    
    def set_name(self, name: str):
        self.name = name

    def set_age(self, age: int):
        self.age = age

    def set_gender(self, gender: str):
        self.gender = gender

    # Additional public functions

    def introduce_yourself(self):
        return f"Hello, my name is {self.name}!  I'm {str(self.age)} years old, and I am {self.gender}"
