""" teacher.py: Teacher Class

    5. Create a class called Teacher that inherits from the Person class and add a subject attribute.

    6. Add a method called teach() to the Teacher class that prints "I am teaching [subject]."

"""

import person

class Teacher(person.Person):
    def __init__(self, name, age, gender, subject):
        super().__init__(name, age, gender)
        self.subject = subject

    def teach(self):
        return f"I am teaching {self.subject}"
