"""student.py - Student Class

   3. Create a class called Student that inherits from the Person class and add a student_id attribute.

   4. Add a method called study() to the Student class that prints "I am studying."

"""

import person

class Student(person.Person):
    def __init__(self, name, age, gender, student_id):
        super().__init__(name, age, gender)
        self.student_id = student_id

    def study(self):
        return "I am studying."
