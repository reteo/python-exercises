"""main.py: Main function

   1. Create a simple class called Person with attributes name, age, and gender. Instantiate this class with your own details.

   2. Add a method called introduce_yourself() to the Person class that prints the name, age, and gender of the person.

   3. Create a class called Student that inherits from the Person class and add a student_id attribute.

   4. Add a method called study() to the Student class that prints "I am studying.""

   5. Create a class called Teacher that inherits from the Person class and add a subject attribute.

   6. Add a method called teach() to the Teacher class that prints "I am teaching [subject]."

   7. Use the Student and Teacher classes to create instances of a student and a teacher, then call their respective methods.

"""

import person
import student
import teacher

me = person.Person("Lampros Liontos", 48, "male")

print(me.introduce_yourself())

you = student.Student("Mike Thomas", 47, "male", "stu-1029")

print(you.study())

him = teacher.Teacher("Jason Frantz", 48, "male", "biology")

print(him.teach())
