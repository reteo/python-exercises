"""Library Class

1. Create a class called Library with an attribute books initialized
as an empty list.

2. Add a method called add_book() to the Library class that takes a
book title as an argument and adds it to the books list.

3. Add a method called remove_book() to the Library class that takes a
book title as an argument and removes it from the books list.

"""

class Library:
    """Library class; contains a list of books available at the library."""
    def __init__(self):
        self.books = []

    def get_books(self) -> list[str]:
        """Get list of books"""
        return self.books

    def add_book(self, title: str):
        """Adds a book title to the library's book list."""
        self.books.append(title)

    def remove_book(self, title: str):
        """Removes a book title from the library's book list."""
        self.books.remove(title)
