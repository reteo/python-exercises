"""Main Loop

1. Create a class called Library with an attribute books initialized
as an empty list.

2. Add a method called add_book() to the Library class that takes a
book title as an argument and adds it to the books list.

3. Add a method called remove_book() to the Library class that takes a
book title as an argument and removes it from the books list.

"""

import library

def main():
    my_library = library.Library()

    # Provides a list of books to add to the library.
    books_to_add = [
        "The Virtue of Losing",
        "Five Days to Whine",
        "Another Long Day",
        "The Adventures of Bob Jones"
    ]

    for book in books_to_add:
        my_library.add_book(book)

    print(my_library.get_books())

    my_library.remove_book("Another Long Day")

    print(my_library.get_books())

main()
