class LightSwitch:

    # Create a class LightSwitch with an attribute is_on. Implement
    # the __init__() method to initialize the is_on attribute with a
    # default value of False.

    def __init__(self):
        self.is_on = False

    # Add a toggle() method to the LightSwitch class that changes the
    # state of the is_on attribute.

    def toggle(self):
        if self.is_on == False:
            self.turn_on()
        else:
            self.turn_off()

    # Add a turn_on() method to the LightSwitch class that sets the
    # is_on attribute to True.

    def turn_on(self):
        self.is_on = True

    # Add a turn_off() method to the LightSwitch class that sets the
    # is_on attribute to False.

    def turn_off(self):
        self.is_on = False
