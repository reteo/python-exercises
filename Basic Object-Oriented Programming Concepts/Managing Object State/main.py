from random import randint


def done():
    print("\n---\n")


from bank_account import BankAccount


def bank_account():
    account = BankAccount("001-A", 50)
    account.deposit(20)
    print(account.balance)
    print(account.withdraw(60))
    print(account.balance)
    print(account.withdraw(30))
    print(account.balance)


from rectangle import Rectangle


def rectangle():
    garden = Rectangle(30, 25)
    print(garden.get_area())
    garden.set_dimensions(25, 25)
    print(garden.get_area())


from counter import Counter


def counter():
    clicker = Counter()
    print(clicker.count)
    clicker.increment()
    print(clicker.count)
    clicker.increment()
    print(clicker.count)
    clicker.increment()
    print(clicker.count)
    clicker.reset()
    print(clicker.count)
    clicker.increment()
    print(clicker.count)


from car import Car


def car():
    car = Car("Toyota", "Camry")
    print(car.speed)
    car.accelerate(50)
    print(car.speed)
    car.brake(25)
    print(car.speed)
    car.accelerate(10)
    print(car.speed)
    car.accelerate(30)
    print(car.speed)
    car.brake(65)
    print(car.speed)
    car.accelerate(70)
    print(car.speed)
    car.brake(85)
    print(car.speed)


from stopwatch import Stopwatch


def stopwatch():
    stopwatch = Stopwatch()

    print(stopwatch.elapsed_time)

    stopwatch.start()
    for _ in range(0, randint(0, 100000000)):
        pass
    stopwatch.stop()

    print(stopwatch.elapsed_time)

    stopwatch.reset()

    print(stopwatch.elapsed_time)


from temperature import Temperature


def temperature():
    therm = Temperature(30)

    print("---")
    print(therm.celsius)
    print(therm.fahrenheit)
    therm.set_fahrenheit(98.6)
    print("---")
    print(therm.celsius)
    print(therm.fahrenheit)
    therm.set_celsius(0)
    print("---")
    print(therm.celsius)
    print(therm.fahrenheit)
    therm.set_fahrenheit(0)
    print("---")
    print(therm.celsius)
    print(therm.fahrenheit)
    therm.set_celsius(30)
    print("---")
    print(therm.celsius)
    print(therm.fahrenheit)

from light_switch import LightSwitch

def light_switch():
    light = LightSwitch()

    print(light.is_on)
    light.turn_on()
    print(light.is_on)
    light.turn_on()
    print(light.is_on)
    light.turn_off()
    print(light.is_on)
    light.turn_off()
    print(light.is_on)
    light.turn_on()
    print(light.is_on)
    light.toggle()
    print(light.is_on)
    light.toggle()
    print(light.is_on)
    light.toggle()
    print(light.is_on)


### --------------------------------- ###


def main():
    bank_account()

    done()

    rectangle()

    done()

    counter()

    done()

    car()

    done()

    stopwatch()

    done()

    temperature()

    done()

    light_switch()

    done()


main()
