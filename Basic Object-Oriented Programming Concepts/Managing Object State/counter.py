class Counter:
    # Create a class Counter with an attribute count. Implement the
    # __init__() method to initialize the count attribute with a
    # default value of 0.

    def __init__(self):
        self.count = 0
    
    # Add an increment() method to the Counter class that increases
    # the count attribute by 1.

    def increment(self):
        self.count += 1

    # Add a reset() method to the Counter class that resets the count
    # attribute to 0.

    def reset(self):
        self.count = 0
