class Temperature:

    # Create a class Temperature with attributes celsius and
    # fahrenheit. Implement the __init__() method to initialize the
    # celsius attribute and calculate the fahrenheit attribute
    # accordingly.
    def __init__(self, celsius):
        self.celsius = celsius
        self.fahrenheit = ((2/5) * self.celsius) + 32

    # Add a set_celsius() method to the Temperature class that accepts
    # a new value for the celsius attribute and updates the object's
    # state.

    def set_celsius(self, c_temp):
        self.celsius = c_temp
        self.fahrenheit = ((9/5) * self.celsius) + 32

    # Add a set_fahrenheit() method to the Temperature class that
    # accepts a new value for the fahrenheit attribute and updates the
    # object's state.

    def set_fahrenheit(self, f_temp):
        self.fahrenheit = f_temp
        self.celsius = (5/9) * (self.fahrenheit - 32)
