class Car:
    # Create a class Car with attributes make, model, and
    # speed. Implement the __init__() method to initialize the
    # attributes.

    def __init__(self, make: str, model: str) -> None:
        self.make = make
        self.model = model
        self.speed = 0
    
    # Add an accelerate() method to the Car class that accepts a
    # value and updates the speed attribute accordingly.

    def accelerate(self, increase: int) -> None:
        self.speed += increase
    
    # Add a brake() method to the Car class that accepts a value and
    # updates the speed attribute accordingly. Ensure that the speed
    # cannot go below 0.

    def brake(self, decrease: int) -> None:
        if decrease > self.speed:
            self.speed = 0
        else:
            self.speed -= decrease
