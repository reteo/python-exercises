class BankAccount:
    
    # Create a class BankAccount with attributes balance and
    # account_number. Implement the __init__() method to initialize
    # the attributes.
    
    def __init__(self, account_number: str, opening_balance: float) -> None:
        self.account_number = account_number
        self.balance = opening_balance

    # Add a deposit() method to the BankAccount class that accepts an
    # amount and updates the account balance accordingly.
        
    def deposit(self, amount: float) -> None:
        self.balance += amount

    # Add a withdraw() method to the BankAccount class that accepts an
    # amount and updates the account balance accordingly. Ensure that
    # the balance cannot go below 0.
        
    def withdraw(self, amount: float) -> float:
        if self.balance < amount:
            return 0
        self.balance -= amount
        return amount
