from datetime import datetime

class Stopwatch:

    # Create a class Stopwatch with an attribute
    # elapsed_time. Implement the __init__() method to initialize the
    # elapsed_time attribute with a default value of 0.

    def __init__(self):
        self.elapsed_time = 0

    # Add a start() method to the Stopwatch class that records the
    # current time as the start time.

    def start(self):
        self.start_time = datetime.now()

    # Add a stop() method to the Stopwatch class that records the
    # current time as the end time and updates the elapsed_time
    # attribute.

    def stop(self):
        if self.start_time != None:
            end_time = datetime.now()
            self.elapsed_time = end_time - self.start_time

    # Add a reset() method to the Stopwatch class that resets the
    # elapsed_time attribute to 0.

    def reset(self):
        self.elapsed_time = 0
        self.start_time = None
