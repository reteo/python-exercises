class Rectangle:

    # Create a class Rectangle with attributes width and
    # height. Implement the __init__() method to initialize the
    # attributes.
    
    def __init__(self, width: float, height: float) -> None:
        self.width = width
        self.height = height

    # Add a set_dimensions() method to the Rectangle class that
    # accepts new values for width and height and updates the object's
    # state.

    def set_dimensions(self, width: float, height: float) -> None:
        self.width = width
        self.height = height

    # Add a get_area() method to the Rectangle class that calculates
    # and returns the area based on the object's state.

    def get_area(self) -> float:
        return self.width * self.height
