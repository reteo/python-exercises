"""Stack Class Exercises

Create a class Stack with a private attribute _data that holds a
list. Implement the __init__() method to initialize the _data
attribute as an empty list.

"""

class MyStack:
    def __init__(self):
        """Initializes the stack values"""
        self._data = []
        self._index = 0

    def __str__(self):
        """Returns a string representation of the stack"""
        return " ".join(self._data)

    def __len__(self):
        """Returns the length of the stack"""
        return len(self._data)

    def __getitem__(self, index):
        """Gets the specified item of the stack"""
        return self._data[index]

    def __setitem__(self, index, data):
        """Modifies the specified index of the stack"""
        self._data[index] = data

    def __delitem__(self, index):
        """Allows the del keyword to remove an item from the stack"""
        del self._data[index]

    def copy(self):
        result = MyStack()

        for datum in self._data:
            result.push(datum)

        return result

    def _reverse(self):
        result = MyStack()

        for datum in self._data[::-1]:
            result.push(datum)

        return result

    def push(self, value):
        """Adds a new item to the top (end) of the stack."""
        self._data.append(value)

    def pop(self):
        """Removes and returns the item from the top (end) of the
        stack.

        """
        return self._data.pop(len(self._data)-1)

    def peek(self):
        """Returns the item from the top (end) of the stack without
        removing it.

        """
        return self._data[len(self._data)-1]

    def __iter__(self):
        """Returns a new iterator instance that maintains its own
        state.  This allows concurrent use of the object in multiple
        iterators, if needed.

        """
        return self.copy()

    def __reversed__(self):
        """Returns a new iterator instance that maintains its own
        state.  This allows concurrent use of the object in multiple
        iterators, if needed.  This instance will use a reversed
        version of the data.

        """
        return self._reverse()
        
    def __next__(self):
        """Returns the current iteration's data and increases the
        index.  Stops the iteration if the index has reached the end

        """
        if self._index >= len(self._data):
            raise StopIteration

        value = self._data[self._index]
        self._index += 1
        return value

    def __contains__(self, datum):
        """Checks to see if datum is in data"""
        if datum in self._data:
            return True
        return False

    def __enter__(self):
        """Within a context manager, we will set it up so that
        anything done to the stack will be reverted at the end of the
        context

        """
        self._data_backup = self._data.copy()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        """Restores the backed-up data at the end of a context"""
        self._data = self._data_backup.copy()
