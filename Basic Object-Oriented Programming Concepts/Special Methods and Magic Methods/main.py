from circle import Circle
from stack import MyStack


def main():

    # Stack Class
    stack = MyStack()

    stack.push("Something")
    stack.push("Wicked")
    stack.push("This")
    stack.push("Way")
    stack.push("Arrives")
    stack.push("and")
    stack.push("Quickly")

    print(stack)

    del stack[5]

    print(stack)

    print(stack[2])

    stack[4] = "Comes"

    print(stack)

    print(len(stack))

    print(stack.peek())

    print(stack.pop())

    print(stack)

    for item in stack:
        print(item)

    for item in reversed(stack):
        print(item)

    if "Wicked" in stack:
        print("Wicked is here.")

    if "Wicket" in stack:
        print("Wicket is here.")

    with stack as temp_stack:
        temp_stack.push("For Now")
        print(temp_stack)

    print(stack)

    print("\n---\n")

    # Circle Class
    circle = Circle(10.0)
    
    print(circle)

    circle.radius = 5
    
    print(circle)

    circle.diameter = 20
    
    print(circle)

    circle.area = 78.53981633974483
    
    print(circle)

    circle.circumference = 62.83185307179586
    
    print(circle)
    
    print(repr(circle))

    circle1 = Circle(5)
    circle2 = Circle(5)
    circle3 = Circle(10)

    print (circle1 == circle2)
    print (circle2 == circle3)

    print(circle2 < circle3)
    print(circle2 <= circle3)
    print(circle2 > circle3)
    print(circle2 >= circle3)
    print(circle1 >= circle2)
    print(circle1 <= circle2)
    print(circle1 > circle2)
    print(circle2 < circle3)
    print(circle2 + circle3)
    print(circle2 * 5)
    print(5 * circle2)
    print(circle3())

main()
