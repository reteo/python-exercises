from math import pi, sqrt


class Circle:
    def __init__(self, radius: float):
        self._radius = float(radius)

    @property
    def radius(self):
        """Return radius of circle."""
        return self._radius

    @radius.setter
    def radius(self, value: float):
        """Set radius of circle."""
        if value < 0:
            raise ValueError("The radius needs to be a positive number!")
        self._radius = float(value)

    @property
    def diameter(self):
        """Return diameter of circle."""
        return 2 * self._radius

    @diameter.setter
    def diameter(self, value: float):
        """Set diameter of circle."""
        if value < 0:
            raise ValueError("The diameter needs to be a positive number!")
        self._radius = value / 2

    @property
    def area(self):
        """Return area of circle."""
        return float(pi * (self._radius**2))

    @area.setter
    def area(self, value):
        """Set area of circle."""
        self._radius = sqrt(value / pi)

    @property
    def circumference(self):
        """Return circumference of circle."""
        return float(2 * pi * self._radius)

    @circumference.setter
    def circumference(self, value):
        """Set circumference of circle."""
        self._radius = value / (2 * pi)

    def __str__(self):
        """Return circle details."""
        result = (
            f"Radius: {self.radius}\n"
            + f"Diameter: {self.diameter}\n"
            + f"Circumference: {self.circumference}\n"
            + f"Area: {self.area}\n"
            )

        return result

    def __repr__(self) -> str:
        """Recreate an object from a string representation"""
        return f"Circle({self._radius})"

    def __eq__(self, other):
        """Compare circles for equality."""
        if self._radius == other._radius:
            return True
        return False

    def __lt__(self, other):
        """Compare to see if one circle is smaller than another."""
        if self._radius < other._radius:
            return True
        return False

    def __le__(self, other):
        """Compare to see if one circle is smaller or equal in size to another."""
        if self._radius <= other._radius:
            return True
        return False

    def __gt__(self, other):
        """Compare to see if one circle is larger than another."""
        if self._radius > other._radius:
            return True
        return False

    def __ge__(self, other):
        """Compare to see if one circle is larger or equal in size to another."""
        if self._radius >= other._radius:
            return True
        return False

    def __add__(self, other):
        """Add two circles radii together to make a larger circle."""
        return Circle(self._radius + other._radius)

    def __mul__(self, scalar):
        """Multiplies the radius by a scalar value and returns a new circle object."""
        return Circle(self._radius * float(scalar))

    # Makes certain the above works in both directions.
    __rmul__ = __mul__ 

    def __call__(self):
        """Makes it so that calling the object will return the object's area."""
        return self.area
