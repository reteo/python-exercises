"""Create a class Finalizer with a __del__() method that performs
cleanup tasks (e.g., closing a file or releasing resources) when the
object is garbage collected.

"""

class Finalizer:
    def __init__(self, filename):
        # print("Initiating Finalizer")
        self.fopen = open(filename, "w")
        print(f"The file is {'Open' if not self.fopen.closed else 'Closed'}")

    def __del__(self):
        print("Closing Finalizer")
        print(f"The file is {'Open' if not self.fopen.closed else 'Closed'}")
        self.fopen.close()
        print(f"The file is {'Open' if not self.fopen.closed else 'Closed'}")

    def write(self, string):
        self.fopen.write(string)
        
