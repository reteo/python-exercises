class Person:
    def __init__(self, name, age):
        print(f"Starting: Name = {name}, Age = {age}")
        self.name = name
        self.age = age

    def __del__(self):
        print(f"Exiting: Name = {self.name}, Age = {self.age}")

    def __str__(self):
        return self.name
