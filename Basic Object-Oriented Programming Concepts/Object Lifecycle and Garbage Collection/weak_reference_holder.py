import weakref
import data_holder

class WeakReferenceHolder:
    def __init__(self):
        self.data = data_holder.DataHolder(42)
        self.holder_ref = weakref.ref(self.data)

    def get_weak_ref(self):
        return self.holder_ref()

    def clear_object(self):
        self.data = None
        
