from my_class import MyClass
from person import Person
from my_resource import MyResource
from my_resource_error import MyResourceError
from data_holder import DataHolder
from weak_reference_holder import WeakReferenceHolder
from finalizer import Finalizer
import os, psutil, time, gc


def done():
    print("\n---\n")


def exercise_1_1():
    # Instantiate a MyClass object and manually call its __del__()
    # method to observe its behavior.

    my_object = MyClass()

    print("mark")
    my_object.__del__()
    print("mark")


def exercise_1_2():
    # Create two instances of MyClass and assign them to each other's
    # attributes, creating a reference cycle. Observe the behavior
    # when the script ends.
    print("Creating Objects")
    my_object_1 = MyClass()
    my_object_2 = MyClass()

    print("Assigning object 1 to object 2")
    my_object_1.my_object = my_object_2
    print("Assigning object 2 to object 1")
    my_object_2.my_object = my_object_1
    print("All Done.")

    ## The "Object Destroyed" message for these objects will only
    ## execute at the end of the script.  This is because there is a
    ## cross-reference, in which each object is keeping the other
    ## object alive, since they refer to each other.


def exercise_1_3():
    # Import the gc (garbage collector) module and use the
    # gc.collect() function to perform manual garbage collection after
    # creating the reference cycle in the previous exercise.

    gc.collect()


def exercise_2_1():
    # Instantiate multiple Person objects, store them in a list, and
    # then remove them from the list. Observe the behavior of the
    # __del__() method.

    people = [Person("Jack", 15), Person("Tom", 10), Person("Jason", 17)]

    print([person.name for person in people])

    # It would seem that the items are destructed in the opposite
    # order as they were constructed.


def exercise_3_1():
    # 1. Create a class MyResource with an attribute file and methods
    # open() and close() to manage file resources

    # 2. Add the __enter__() and __exit__() methods to the MyResource
    # class to make it usable as a context manager.

    # 3. Use the MyResource class in a with statement to automatically
    # manage the object's lifecycle.

    filename = "exercise_3_1.txt"

    try:
        with MyResource(filename, "w") as fopen:
            fopen.write("Something different this way schploinks.")
    except MyResourceError as e:
        print(f"Error: {e}")

    try:
        with MyResource(filename, "r") as fopen:
            print(fopen.readline())
    except MyResourceError as e:
        print(f"Error: {e}")


def exercise_4_1():
    # 1. Create a custom exception MyResourceError that inherits from
    # the built-in Exception class.

    # 2. Modify the MyResource class to raise a MyResourceError if the
    # close() method is called before the open() method.

    # 3. Use the try and except statements to handle the
    # MyResourceError in a script that uses the MyResource class.

    filename = "exercise_4_1.txt"

    fopen = MyResource(filename, "w")

    try:
        fopen.close()
    except MyResourceError:
        print("Resource Error Tripped!")


def observe_memory_usage():
    process = psutil.Process(os.getpid())
    mem_info = process.memory_info()
    return mem_info.rss


def exercise_5_1():
    # Create a class DataHolder with an attribute data that holds a
    # large amount of data (e.g., a large list). Then, instantiate
    # multiple DataHolder objects and observe the memory usage of your
    # Python script.
    data_size = 10**6
    large_data = list(range(data_size))

    initial_memory_usage = observe_memory_usage()
    print(f"Initial memory usage: {initial_memory_usage / (1024 * 1024)} MB")

    print("First, without garbage collection.")
    
    data_holders = []
    for i in range(5):
        data_holders.append(DataHolder(large_data.copy()))
        time.sleep(1)
        current_memory_usage = observe_memory_usage()
        print(f"Memory usage after creating DataHolder: {current_memory_usage / (1024 * 1024)} MB")
    
    del data_holders
    gc.collect()

    print("Next, with garbage collection.")
    
    data_holders = []
    
    for i in range(5):
        data_holders.append(DataHolder(large_data.copy()))
        time.sleep(1)
        current_memory_usage = observe_memory_usage()
        print(f"Memory usage after creating DataHolder: {current_memory_usage / (1024 * 1024)} MB")
        del data_holders[0]
        gc.collect()

    del data_holders
    gc.collect()

    print("Now, with garbage collection debugging.")
    
    data_holders = []

    gc.set_debug(gc.DEBUG_STATS | gc.DEBUG_UNCOLLECTABLE)
    
    for i in range(5):
        data_holders.append(DataHolder(large_data.copy()))
        time.sleep(1)
        current_memory_usage = observe_memory_usage()
        print(f"Memory usage after creating DataHolder: {current_memory_usage / (1024 * 1024)} MB")
        del data_holders[0]
        gc.collect()

    gc.set_debug(0)

    print("Now, with object inspection.")
    
    data_holders = []
    
    for i in range(5):
        data_holders.append(DataHolder(large_data.copy()))
        time.sleep(1)
        current_memory_usage = observe_memory_usage()
        print(f"Memory usage after creating DataHolder: {current_memory_usage / (1024 * 1024)} MB")
        del data_holders[0]
        print(f"Number of objects tracked before collection: {len(gc.get_objects())}")
        gc.collect()
        print(f"Number of objects tracked after collection: {len(gc.get_objects())}")

def exercise_6_1():
    # Observe the behavior of the weak reference when the DataHolder
    # object is garbage collected.
    test_object = WeakReferenceHolder()
    print(test_object.get_weak_ref())
    test_object.clear_object()
    print(test_object.get_weak_ref())

def exercise_7_1():
    """Use the gc.get_referrers() function to inspect the objects that
    reference a specific object.

    """
    test_object = DataHolder(192)
    second_test_object = DataHolder(24)
    test_object.reference = second_test_object
    second_test_object.reference = test_object
    
    referrers = gc.get_referrers(test_object)
    for referrer in referrers:
        print(referrer)

def exercise_8_1():
    # Use the gc.get_threshold() and gc.set_threshold() functions to
    # inspect and modify the garbage collector's threshold values.

    print(gc.get_threshold())
    gc.set_threshold(800, 15, 15)
    print(gc.get_threshold())

def exercise_9_1():
    # Create a class Finalizer with a __del__() method that performs
    # cleanup tasks (e.g., closing a file or releasing resources) when
    # the object is garbage collected.

    # Instantiate a Finalizer object, and observe the behavior of the
    # __del__() method when the object is garbage collected.

    text_file = Finalizer("exercise_9_1.txt")
    text_file.write("This is a test string.")
    text_file = None
    gc.collect()
    

def main():
    exercise_1_1()

    done()

    exercise_1_2()

    done()

    exercise_1_3()

    done()

    exercise_2_1()

    done()

    exercise_3_1()

    done()

    exercise_4_1()

    done()

    exercise_5_1()

    done()

    exercise_6_1()

    done()

    exercise_7_1()

    done()

    exercise_8_1()

    done()

    exercise_9_1()

    done()


main()
