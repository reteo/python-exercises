from my_resource_error import MyResourceError

class MyResource:
    def __init__(self, filename, mode):
        self._filename = filename
        self._mode = mode
        self.fopen = None

    def open(self):
        self.fopen = open(self._filename, self._mode)
        print("Debugging: After Opening:", self.fopen)

    def close(self):
        print("Debugging: Before Closing:", self.fopen)
        if self.fopen is None:
            raise MyResourceError("File not opened.")
        self.fopen.close()

    def __enter__(self):
        self.open()
        return self.fopen

    def __exit__(self, exc_type, exc_mess, exc_trace):
        self.close()
