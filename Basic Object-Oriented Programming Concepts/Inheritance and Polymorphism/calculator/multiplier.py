"""Implement a class called Multiplier that inherits from the
Calculator class and overrides the operate() method to multiply the
two numbers."""

from .calculator import Calculator


class Multiplier(Calculator):
    def operate(self, num1, num2):
        return num1 * num2
