"""Implement a class called Adder that inherits from the Calculator
class and overrides the operate() method to add the two numbers."""

from .calculator import Calculator

class Adder(Calculator):
    def operate(self, num1, num2):
        return num1 + num2
