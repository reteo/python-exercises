"""Create a class called Calculator with an abstract method called
operate() that takes two numbers as arguments and performs an
operation on them."""

from abc import ABC, abstractmethod


class Calculator(ABC):
    @abstractmethod
    def operate(self, num1, num2):
        pass
