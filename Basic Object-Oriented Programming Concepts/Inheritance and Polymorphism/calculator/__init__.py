__name__ = "calculator"

from .calculator import Calculator
from .adder import Adder
from .multiplier import Multiplier
