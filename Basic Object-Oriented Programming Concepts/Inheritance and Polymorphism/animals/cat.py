"""Implement a class called Cat that inherits from the Animal class
and overrides the make_sound() method to print 'Meow!'

"""

from .animal import Animal

class Cat(Animal):
    def make_sound(self):
        return "Meow!"
