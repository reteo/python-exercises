"""Implement a class called Dog that inherits from the Animal class
and overrides the make_sound() method to print 'Woof!'
"""

from .animal import Animal

class Dog(Animal):
    def make_sound(self):
        return "Woof!"
