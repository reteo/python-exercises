"""Create a class called Animal with an abstract method called
make_sound() that prints the sound the animal makes.

"""

from abc import ABC, abstractmethod


class Animal(ABC):
    @abstractmethod
    def make_sound(self):
        pass
