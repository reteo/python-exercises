"""Animals Package"""

__name__ = "animals"

from .animal import Animal
from .dog import Dog
from .cat import Cat
