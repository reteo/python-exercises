"""2. Implement a class called Square that inherits from the Shape class
and overrides the area() method to calculate and return the area of
the square.

"""

from .shape import Shape

class Square(Shape):
    def __init__(self, side):
        self.side = side

    def area(self):
        return self.side * self.side

    def set_side(self, side):
        self.side = side

    def get_side(self):
        return self.side
