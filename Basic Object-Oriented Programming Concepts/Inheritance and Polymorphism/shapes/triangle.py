"""3. Implement a class called Triangle that inherits from the Shape
class and overrides the area() method to calculate and return the area
of the triangle.

"""

from .shape import Shape
import math

class Triangle(Shape):
    def __init__(self, side_1, side_2):
        """Only two sides are needed; the third is calculated."""
        self.side_1 = side_1
        self.side_2 = side_2
        # final side is calculated using the Pythagorean Theorem.
        self.side_3 = math.sqrt((side_1 ** 2) + (side_2 ** 2))

    def area(self):
        return (self.side_1 * self.side_2)/2
