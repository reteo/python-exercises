"""Shapes Package"""

__name__ = "shapes"

from .shape import Shape
from .square import Square
from .triangle import Triangle
