"""

1. Create a class called Shape with an abstract method called area()
that returns the area of the shape.

"""

import abc # module for abstract classes


class Shape(abc.ABC):
    @abc.abstractmethod
    def area(self):
        pass
