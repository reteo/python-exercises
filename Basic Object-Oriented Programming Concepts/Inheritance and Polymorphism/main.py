"""1. Instantiate a square and a triangle, then call their respective
area() methods to demonstrate polymorphism.
"""

from shapes import Shape, Square, Triangle
from animals import Animal, Cat, Dog
from calculator import Calculator, Adder, Multiplier
from transport import Transport, Car, Bicycle


# From "Shape Class," Exercise 5
def calculate_area(shape: Shape):
    """Create a function called calculate_area() that takes a Shape
    object as an argument and calls the area() method on it."""
    return shape.area()


# From "Animal Class," Exercise 5
def play_sound(animal: Animal):

    """Create a function called play_sound() that takes an Animal
    object as an argument and calls the make_sound() method on it.

    """
    return animal.make_sound()


# From "Calculator Class," Exercise 5
def perform_operation(calculator: Calculator, num1: int, num2: int):
    """Create a function called perform_operation() that takes a
    Calculator object and two numbers as arguments and calls the
    operate() method on the calculator object with the given numbers.

    """
    return calculator.operate(num1, num2)

# From "Transport Class," Exercise 5
def start_travel(transport: Transport):
    """Create a function called start_travel() that takes a Transport
    object as an argument and calls the travel() method on it."""
    return transport.travel()

def done():
    print("\n---\n")


def main():

    print(
        "Instantiate a square and a triangle, then call their\n"
        + "respective area() methods to demonstrate polymorphism.\n"
    )

    shapes = [Square(5), Triangle(6, 3)]

    for shape in shapes:
        print(shape.area())

    done()

    # ---- #

    print(
        "Use the calculate_area() function to find the area of a\n"
        "square and a triangle, demonstrating polymorphism through\n"
        "the function."
    )

    for shape in shapes:
        print(calculate_area(shape))

    done()

    # --- #

    print(
        "Instantiate a dog and a cat, then call their respective\n"
        + "make_sound() methods to demonstrate polymorphism.\n"
    )

    animals = [Dog(), Cat()]

    for animal in animals:
        print(animal.make_sound())

    done()

    # ---- #

    print(
        "Use the play_sound() function to play the sound of a dog and\n"
        "a cat, demonstrating polymorphism through the function.\n"
    )

    for animal in animals:
        print(play_sound(animal))

    done()

    # --- #

    print(
        "Instantiate an adder and a multiplier, then call their\n"
        "respective operate() methods with two numbers to demonstrate\n"
        "polymorphism."
    )

    adder = Adder()
    multiplier = Multiplier()

    numbers = [4, 4]

    for operation in [adder, multiplier]:
        print(operation.operate(numbers[0], numbers[1]))

    done()

    # --- #

    print(
        "Use the perform_operation() function to perform addition\n"
        + "and multiplication, demonstrating polymorphism through\n"
        + "the function.\n"
    )


    for operation in [adder, multiplier]:
        print(perform_operation(operation, numbers[0], numbers[1]))

    done()

    # --- #

    print(
        "Instantiate a car and a bicycle, then call their\n"
        + "respective travel() methods to demonstrate polymorphism.\n"
    )

    car = Car()
    bike = Bicycle()

    for transport in [car, bike]:
        print(transport.travel())

    done()

    # --- #

    print(
        "Use the start_travel() function to start traveling by car\n" +
        "and bicycle, demonstrating polymorphism through the\n" +
        "function.\n"
    )

    for transport in [car, bike]:
        print(start_travel(transport))

    

    done()

    # --- #

main()
