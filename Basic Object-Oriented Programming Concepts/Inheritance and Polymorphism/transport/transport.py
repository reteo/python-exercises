"""Create an abstract class called Transport with an abstract method
called travel() that prints the mode of transportation."""

from abc import ABC, abstractmethod


class Transport(ABC):
    @abstractmethod
    def travel(self):
        pass
