"""Implement a class called Bicycle that inherits from the Transport
class and overrides the travel() method to print 'Riding a bicycle.'"""

from .transport import Transport


class Bicycle(Transport):
    def travel(self):
        return "Riding a bicycle."
