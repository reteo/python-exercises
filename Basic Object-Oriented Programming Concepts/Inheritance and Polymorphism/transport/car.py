"""Implement a class called Car that inherits from the Transport class
and overrides the travel() method to print 'Driving a car.'

"""

from .transport import Transport


class Car(Transport):
    def travel(self):
        return "Driving a Car."
