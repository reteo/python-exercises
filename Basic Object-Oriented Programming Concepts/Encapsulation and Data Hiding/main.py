"""Main module"""

from decimal import Decimal
from car import Car
from employee import Employee
from person import Person
from bank_account import BankAccount
from temperature import Temperature


def done():
    print("\n --- \n")


def main():
    print(
        "Instantiate the Person class and use the getter and\n"
        + "setter methods to access and modify the private\n"
        + "attributes.\n"
    )

    me = Person("Lamprs", 47, "female")

    print(
        f"Name: {me.get_name()}\n"
        + f"Age: {me.get_age()}\n"
        + f"Gender: {me.get_gender()}\n"
    )

    me.set_name("Lampros")
    me.set_age(48)
    me.set_gender("male")

    print(
        f"Name: {me.get_name()}\n"
        + f"Age: {me.get_age()}\n"
        + f"Gender: {me.get_gender()}\n"
    )

    done()

    # --- #

    print(
        "Instantiate the BankAccount class and use the public\n"
        + "methods to deposit and withdraw money, demonstrating\n"
        + "encapsulation.\n"
    )

    account = BankAccount("ba-001", Decimal(100.00), "Lampros Liontos")

    print(account.check_balance())
    print("Add 500:", account.deposit(Decimal(500)))
    print(account.check_balance())
    print("Withdraw 400:", account.withdraw(Decimal(400)))
    print(account.check_balance())
    print("Withdraw 500:", account.withdraw(Decimal(500)))
    print(account.check_balance())
    print("Add -100:", account.deposit(Decimal(-100)))
    print(account.check_balance())
    print("Withdraw -100:", account.withdraw(Decimal(500)))
    print(account.check_balance())

    done()

    # --- #

    print(
        "Instantiate the Car class and use the getter, setter,\n"
        + "and drive() methods to access and modify the private\n"
        + "attributes, demonstrating encapsulation.\n"
    )

    car = Car("Toyota", "Camry", "2008", Decimal(150000))

    print(f"Make: {car.get_make()}")
    print(f"Model: {car.get_model()}")
    print(f"Year: {car.get_year()}")
    print(f"Mileage: {car.get_mileage()}")

    car.drive(Decimal(152.22))

    print(f"Mileage: {car.get_mileage()}")

    done()

    # --- #

    print(
        "Instantiate the Temperature class and use the getter\n"
        + "and setter methods to convert between Celsius and\n"
        + "Fahrenheit, demonstrating encapsulation.\n"
    )

    print("\nChanged to 25°C\n")
    thermometer = Temperature(25)

    print(f"Celcius: {thermometer.get_celsius()}")
    print(f"Fahrenheit: {thermometer.get_fahrenheit()}")

    print("\nChanged to 15°C\n")
    thermometer.set_celsius(15)

    print(f"Celcius: {thermometer.get_celsius()}")
    print(f"Fahrenheit: {thermometer.get_fahrenheit()}")

    print("\nChanged to 77°F\n")
    thermometer.set_fahrenheit(77)

    print(f"Celcius: {thermometer.get_celsius()}")
    print(f"Fahrenheit: {thermometer.get_fahrenheit()}")

    done()

    # --- #

    print(
        "Instantiate the Employee class and use the getter methods\n"
        + "and annual_salary() method to access the private attributes\n"
        + "and calculate the annual salary, demonstrating encapsulation\n"
        + "and data hiding.\n"
    )

    employee = Employee("emp-001", "Lampros Liontos", 40000.00)

    print("ID:", employee.get_id())
    print("Name:", employee.get_name())
    print("Salary:", employee.get_salary())
    print("Salary + Bonus:", employee.annual_salary())

    done()

    # --- #


main()
