"""Create a class called Person with private attributes _name, _age,
and _gender. Initialize these attributes using a constructor method."""


class Person:
    def __init__(self, name: str, age: int, gender: str):
        self._name = name
        self._age = age
        self._gender = gender

    # Getters

    def get_name(self):
        return self._name

    def get_age(self):
        return self._age

    def get_gender(self):
        return self._gender

    # Setters

    def set_name(self, name: str):
        self._name = name

    def set_age(self, age: int):
        self._age = age

    def set_gender(self, gender: str):
        self._gender = gender

    # String Result

    def __str__(self):
        return f"Name: {self._name}\n" + f"Age: {self._age}\n" + f"Gender: {self._gender}\n"
