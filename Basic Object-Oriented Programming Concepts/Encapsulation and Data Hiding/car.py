"""Create a class called Car with private attributes _make, _model,
_year, and _mileage. Initialize these attributes using a constructor
method.

"""

from decimal import Decimal


class Car:
    def __init__(self, make, model, year, mileage):
        self._make = make
        self._model = model
        self._year = year
        self._mileage = mileage

    def get_make(self):
        return self._make

    def get_model(self):
        return self._model

    def get_year(self):
        return self._year

    def get_mileage(self):
        return self._mileage.quantize(Decimal('0.01'))

    def drive(self, mileage: Decimal):
        self._mileage += mileage
