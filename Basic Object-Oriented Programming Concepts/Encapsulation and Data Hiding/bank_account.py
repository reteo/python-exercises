"""Create a class called BankAccount with private attributes
_account_number, _balance, and _owner. Initialize these attributes
using a constructor method.

"""

from decimal import Decimal

class BankAccount:
    def __init__(self, account: str, balance: Decimal, owner: str):
        self._account_number = account
        self._balance = balance
        self._owner = owner

    def _can_withdraw(self, amount):
        if (self._balance - amount) < 0:
            return False
        return True

    def _test_positive(self, amount):
        if amount < Decimal(0):
            return False
        return True

    def deposit(self, amount: Decimal):
        if self._test_positive(amount):
            self._balance += amount
            return True
        return False

    def withdraw(self, amount: Decimal):
        if self._test_positive(amount) and self._can_withdraw(amount):
            self._balance -= amount
            return True
        return False

    def check_balance(self):
        return self._balance
