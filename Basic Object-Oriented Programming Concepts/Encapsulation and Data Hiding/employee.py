"""Create a class called Employee with private attributes _id, _name,
and _salary. Initialize these attributes using a constructor method.

"""

class Employee:
    def __init__(self, identification: str, name: str, salary: float):
        self._id = identification
        self._name = name
        self._salary = salary

    def get_name(self):
        return self._name

    def get_id(self):
        return self._id

    def get_salary(self):
        return self._salary

    def _calculate_bonus(self):
        return self._salary * 0.1

    def annual_salary(self):
        return self._salary + self._calculate_bonus()
