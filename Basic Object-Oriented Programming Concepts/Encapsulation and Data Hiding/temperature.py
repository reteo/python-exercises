"""Create a class called Temperature with a private attribute
_celsius. Initialize this attribute using a constructor method.

"""

class Temperature:
    def __init__(self, temp_celcius):
        self._celcius = temp_celcius

    def get_celsius(self):
        return self._celcius

    def get_fahrenheit(self):
        return (((9/5) * self._celcius) + 32)

    def set_celsius(self, temp_celsius):
        self._celcius = temp_celsius

    def set_fahrenheit(self, temp_fahrenheit):
        self._celcius = (((5/9) * (temp_fahrenheit - 32)))
