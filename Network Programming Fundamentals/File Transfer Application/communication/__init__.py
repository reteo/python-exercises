__name__ = "communication"

from communicate import receive, recv_file, send, send_file
from connection import Connection
from secure import SConnection
