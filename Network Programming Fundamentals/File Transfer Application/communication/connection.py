import socket


class Connection:
    """
    A class that abstracts a TCP connection, either as a server or
    a client.
    """

    def close(self):
        """Close the connection."""
        if self.conn:
            self.conn.close()
        if self._socket:
            self._socket.close()

    def __init__(self, host: tuple[str, int], server=False):
        """
        Create a new Connection.

        :param host: A tuple of (hostname, port number).
        :param server: True if this is a server connection, False if client.
        """

        self._server = server
        self._socket = None
        self.conn = None
        self._addr = None

        try:
            if server:
                self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self._socket.bind(host)
                self._socket.listen()

                self.conn, self._addr = self._socket.accept()
            else:
                self.conn = socket.create_connection(host)
                self._addr = host[0]

        except Exception as e:
            print(f"Error creating connection: {e}")
            self.close()
            raise

    def __del__(self):
        """Destructor, ensures resources are cleaned up."""
        self.close()

    def __enter__(self):
        """Allows object to be used with context managers."""
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        """Allows object to be used with context managers.  Closes
        connection when context ends."""
        self.close()

    def get_address(self):
        """Returns the address the connection is intended for."""
        return self._addr
