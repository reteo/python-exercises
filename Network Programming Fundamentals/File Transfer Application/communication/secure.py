import socket
import ssl


class SConnection:
    """
    A class that secures an existing socket, and acts like the
    socket itself.
    """

    def close(self):
        """
        Closes the Secure Socket.
        """
        if self.conn and self.conn is not None:
            self.conn.close()

    def __init__(self, sock, server=False, host=""):
        """Initiates a secure wrapper around an existing socket.  Use
        connection in this object in place of the one in the original
        socket.

        """
        # First, create context.
        if server == True:
            self._context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
        else:
            self._context = ssl.create_default_context()

        # Next, wrap the socket.
        if server == True:
            self.conn = self._context.wrap_socket(sock, server_side=True)
        else:
            self.conn = self._context.wrap_socket(sock, server_hostname=host)

    def __enter__(self):
        """Allows this object to be used with a context manager."""
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        """Allows this object to be used with a context manager;
        closes the wrapper when finished.

        """
        self.close()

    def __del__(self):
        """Cleans up if the object gets destroyed."""
        self.close()
