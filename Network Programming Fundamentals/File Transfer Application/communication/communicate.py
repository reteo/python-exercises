def receive(sock):
    """Receives messages from remote node."""
    result = b""
    try:
        while True:
            data = sock.conn.recv(1024)
            if not data:
                break
            result += data
    except Exception as e:
        print(f"Error occurred during receive:\n{str(e)}")
        raise
    finally:
        return result.decode()


def send(sock, message):
    """Sends messages to remote node."""
    try:
        sock.conn.sendall(message.encode())
    except Exception as e:
        print(f"Error occurred during send:\n{str(e)}")
        raise
    else:
        return True


def send_file(sock, filename):
    """Sends file data to remote node."""
    try:
        with open(filename, "rb") as fopen:
            while True:
                data = fopen.read(1024)
                if not data:
                    break
                sock.conn.sendall(data)
    except FileNotFoundError:
        print(f"The file {filename} does not exist.")
        raise
    except Exception as e:
        print(f"Error occurred during file transmission:\n{str(e)}")
        raise
    else:
        return True


def recv_file(sock, filename):
    """Recives file data from remote node."""
    try:
        with open(filename, "wb") as fopen:
            while True:
                data = sock.conn.recv(1024)
                if not data:
                    break
                fopen.write(data)
    except Exception as e:
        print(f"Error occurred during receive:\n{str(e)}")
        raise
    else:
        return True
